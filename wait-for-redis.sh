#!/bin/sh

: ${REDIS_HOST:=localhost}
: ${REDIS_PORT:=6379}

cmd="$@"

until nc -z $REDIS_HOST $REDIS_PORT
do
    echo "Waiting for Redis ($REDIS_HOST:$REDIS_PORT) to start..."
    sleep 0.5
done

>&2 echo "Redis is up - executing command"
exec $cmd
