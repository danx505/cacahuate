#!/usr/bin/env python3
from os import path

from setuptools import setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst')) as f:
    long_description = f.read()

with open(path.join(here, 'cacahuate', 'version.txt')) as f:
    version = f.read().strip()

setup(
    name='cacahuate',
    description='The process virtual machine',
    long_description=long_description,
    url='https://github.com/tracsa/cacahuate',

    version=version,

    author='Abraham Toriz Cruz',
    author_email='categulario@gmail.com',
    license='MIT',

    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries :: Python Modules',

        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],

    keywords='process',

    packages=[
        'cacahuate',
        'cacahuate.models',
        'cacahuate.http',
        'cacahuate.http.views',
        'cacahuate.http.views.admin',
        'cacahuate.http.views.api',
        'cacahuate.http.views.workflow',
        'cacahuate.auth',
        'cacahuate.auth.backends',
        'cacahuate.auth.hierarchy',
        'cacahuate.xml',
        'cacahuate.database',
        'cacahuate.migrations',
        'cacahuate.migrations.versions',
    ],

    package_data={
        'cacahuate': [
            'grammars/*.g',
            'xml/*.rng',
            'version.txt',
            'templates/*.html',
            'alembic.ini',
        ],
    },

    entry_points={
        'console_scripts': [
            'cacahuated = cacahuate.main:main',
            'xml_validate = cacahuate.main:xml_validate',
            'rng_path = cacahuate.main:rng_path',
            'apply-migrations = cacahuate.migrations.apply:main',
        ],
    },

    install_requires=[
        'flask >= 2.0',
        'Flask-Cors',
        'Flask_PyMongo < 2.0',
        'SQLAlchemy >= 2.0',
        'pyjwt >= 2.0',
        'psycopg2-binary',
        'pymongo < 4.0',
        'case_conversion',
        'itacate',
        'lark-parser >= 0.6',
        'ldap3',
        'pika >= 1.0',
        'simplejson',
        'requests',
        'passlib',
        'jsonpath-ng',
        'jinja2',
        'MarkupSafe < 2.1',
        'celery',
        'python-dateutil',
        'alembic',
        (
            'graphene-sqlalchemy @ '
            'git+https://github.com/graphql-python/graphene-sqlalchemy@'
            'd0668cc82dfd349aa418dd6fc16d54e80162960a'
            '#egg=graphene-sqlalchemy'
        ),
        'graphql-server',
        'pyyaml',
    ],

    setup_requires=[
        'pytest-runner',
    ],

    tests_require=[
        'pytest',
        'pytest-mock',
    ],
)
