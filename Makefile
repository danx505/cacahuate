.PHONY: help
## help
##  - show available targets
help: Makefile
	@sed -n 's/^##//p' $<

.PHONY: build
## build
build:
	./setup.py sdist && ./setup.py bdist_wheel

.PHONY: test
## test
test: pytest lint xmllint xml_validate

.PHONY: xml
## xml
xml: xmllint xml_validate

.PHONY: publish
## publish
publish:
	twine upload dist/* && git push && git push --tags

.PHONY: clean
## clean
clean:
	rm -rf dist/

.PHONY: pytest
## pytest
pytest:
	pytest -xvv

.PHONY: lint
## lint
lint:
	flake8

.PHONY: xmllint
## xmllint
xmllint:
	xmllint --noout --relaxng cacahuate/xml/process-spec.rng xml/*.xml

.PHONY: xml_validate
## xml_validate
xml_validate:
	xml_validate xml/*.xml

.PHONY: clear-objects
## clear-objects
clear-objects:
	python -c "from coralillo import Engine; eng=Engine(); eng.lua.drop(args=['*'])"
	mongo cacahuate --eval "db.pointer.drop()"
	mongo cacahuate --eval "db.execution.drop()"
	sudo rabbitmqctl purge_queue cacahuate_process
