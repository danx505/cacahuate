#!/bin/sh

: ${MONGO_HOST:=localhost}
: ${MONGO_PORT:=27017}

cmd="$@"

until nc -z $MONGO_HOST $MONGO_PORT
do
    echo "Waiting for Mongo ($MONGO_HOST:$MONGO_PORT) to start..."
    sleep 0.5
done

>&2 echo "Mongo is up - executing command"
exec $cmd
