"""Set not null fields

Revision ID: 57c580f2a1ec
Revises: 956b9a3e04c4
Create Date: 2023-06-28 11:16:51.738835

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '57c580f2a1ec'
down_revision = '956b9a3e04c4'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('field', 'fieldset_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('fieldsetdefinition', 'workflow_id',
               existing_type=sa.INTEGER(),
               nullable=False)
    op.alter_column('taskdefinition', 'name',
               existing_type=sa.VARCHAR(length=100),
               nullable=False)
    op.alter_column('workflowdefinition', 'updated_at',
               existing_type=postgresql.TIMESTAMP(timezone=True),
               nullable=False)
    op.alter_column('workflowdefinition', 'name',
               existing_type=sa.VARCHAR(length=100),
               nullable=False)
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('workflowdefinition', 'name',
               existing_type=sa.VARCHAR(length=100),
               nullable=True)
    op.alter_column('workflowdefinition', 'updated_at',
               existing_type=postgresql.TIMESTAMP(timezone=True),
               nullable=True)
    op.alter_column('taskdefinition', 'name',
               existing_type=sa.VARCHAR(length=100),
               nullable=True)
    op.alter_column('fieldsetdefinition', 'workflow_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    op.alter_column('field', 'fieldset_id',
               existing_type=sa.INTEGER(),
               nullable=True)
    # ### end Alembic commands ###
