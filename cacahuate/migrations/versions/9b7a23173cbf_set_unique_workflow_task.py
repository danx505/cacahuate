"""Set unique workflow task

Revision ID: 9b7a23173cbf
Revises: abdb063ee657
Create Date: 2023-06-26 14:21:04.303152

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = '9b7a23173cbf'
down_revision = 'abdb063ee657'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index('ix_taskdefinition_identifier', table_name='taskdefinition')
    op.create_index(op.f('ix_taskdefinition_identifier'), 'taskdefinition', ['identifier'], unique=False)
    op.create_unique_constraint(None, 'taskdefinition', ['identifier', 'workflow_id'])
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'taskdefinition', type_='unique')
    op.drop_index(op.f('ix_taskdefinition_identifier'), table_name='taskdefinition')
    op.create_index('ix_taskdefinition_identifier', 'taskdefinition', ['identifier'], unique=False)
    # ### end Alembic commands ###
