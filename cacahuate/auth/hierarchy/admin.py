import logging
import traceback

import cacahuate.auth.base
import cacahuate.models

LOGGER = logging.getLogger(__name__)


def get_clean_array_from_string(value):
    if isinstance(value, str) is False:
        return []

    return list({
        x.strip() for x in value.split(',') if x.strip()
    })


class AdminHierarchyProvider(cacahuate.auth.base.BaseHierarchyProvider):

    def find_users(self, **params):
        try:
            return cacahuate.database.query.filter_users(**params)
        except Exception:
            LOGGER.error(traceback.format_exc())
            return []
