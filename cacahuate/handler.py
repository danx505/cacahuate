import datetime
import importlib
import logging

import cacahuate.rabbit
from cacahuate.cascade import cascade_invalidate
from cacahuate.cascade import track_next_node
from cacahuate.database import db_session
from cacahuate.errors import EndOfProcess
from cacahuate.errors import InconsistentState
from cacahuate.errors import MisconfiguredProvider
from cacahuate.jsontypes import Map
from cacahuate.models import (
    Execution,
    Pointer,
    User,
    unpack_execution_values,
)
from cacahuate.mongo import make_context
from cacahuate.mongo import pointer_entry
from cacahuate.templates import render_or
from cacahuate.xml import Xml
from cacahuate.xml.node import UserAttachedNode

import pymongo

import simplejson as json

import sqlalchemy.orm.exc

LOGGER = logging.getLogger(__name__)


class Handler:
    ''' The actual process machine, it is in charge of moving the pointers
    through the graph of nodes '''

    def __init__(self, config):
        self.config = config
        self.mongo = None

    def __call__(self, body: bytes):
        ''' the main callback of cacahuate, gets called when a new message
        arrives from rabbitmq. '''
        message = json.loads(body)

        if message['command'] == 'cancel':
            self.cancel_execution(message)
        elif message['command'] == 'step':
            self.step(message)
        elif message['command'] == 'patch':
            self.patch(message)
        else:
            LOGGER.warning(
                'Unrecognized command {}'.format(message['command']),
            )

    def step(self, message: dict):
        ''' Handles deleting a pointer from the current node and creating a new
        one on the next '''
        pointer, user, forms = self.recover_step(message)
        execution = pointer.execution

        process_name = execution.process_name
        xml = Xml.load(
            self.config,
            process_name,
            direct=True,
        )
        node = xml.get_node(pointer.node_id)

        # node's lifetime ends here
        self.teardown(node, pointer, user, forms)

        # compute the next node in the sequence
        try:
            next_node, state = self.next(xml, node, execution)
        except EndOfProcess:
            # finish the execution
            return self.finish_execution(execution)

        self.wakeup_and_notify(next_node, execution, state)

    def wakeup_and_notify(self, node, execution, state):
        ''' Calls wakeup on the given node and notifies if it is a sync node
        '''
        # node's begining of life
        qdata = self.wakeup(node, execution, state)

        # Sync nodes are queued immediatly
        if qdata:
            new_pointer, new_input = qdata

            from .tasks import handle
            handle.delay(json.dumps({
                'command': 'step',
                'pointer_id': new_pointer.id,
                'user_identifier': '__system__',
                'input': new_input,
            }))

    def next(  # noqa
        self,
        xml,
        node,
        execution,
    ):
        ''' Given a position in the script, return the next position '''
        # Return next node by simple adjacency, works for actions and accepted
        # validations

        state = next(self.execution_collection().find({'id': execution.id}))

        try:
            while True:
                node = node.next(
                    xml,
                    state,
                    self.get_mongo(),
                    self.config,
                )

                # refresh state because previous call might have changed it
                state = next(
                    self.execution_collection().find({'id': execution.id}),
                )

                if node.id in state['state']['items']:
                    if state['state']['items'][node.id]['state'] == 'valid':
                        continue

                return node, state
        except StopIteration:
            # End of process
            raise EndOfProcess

    def wakeup(self, node, execution, state):
        ''' Waking up a node often means to notify someone or something about
        the execution, this is the first step in a node's lifecycle '''

        # get currect execution context
        exc_doc = next(self.execution_collection().find({'id': execution.id}))
        context = make_context(exc_doc, self.config)

        # create a pointer in this node
        pointer = self._create_pointer(
            node.id,
            type(node).__name__.lower(),
            node.get_name(context),
            node.get_description(context),
            execution,
        )
        LOGGER.debug('Created pointer p:{} n:{} e:{}'.format(
            pointer.id,
            node.id,
            execution.id,
        ))

        # mark this node as ongoing
        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': {
                f'state.items.{node.id}.state': pointer.status,
                f'state.items.{node.id}.name': pointer.name,
                f'state.items.{node.id}.description': pointer.description,
            },
        })

        # update registry about this pointer
        self.pointer_collection().insert_one(pointer_entry(
            node,
            pointer,
        ))

        # notify someone (can raise an exception
        if isinstance(node, UserAttachedNode):
            notified_users = self.notify_users(node, pointer, state)
        else:
            notified_users = []

        # do some work (can raise an exception)
        if not node.is_async():
            inpt = node.work(self.config, state, self.get_mongo())
        else:
            inpt = []

        # set actors to this pointer (means everything succeeded)
        self.pointer_collection().update_one({
            'id': pointer.id,
        }, {
            '$set': {
                'notified_users': notified_users,
            },
        })

        # nodes with forms are not queued
        if not node.is_async():
            return pointer, inpt

    def teardown(self, node, pointer, user, forms):
        ''' finishes the node's lifecycle '''
        current_time = datetime.datetime.now()

        execution = pointer.execution

        if user not in pointer.actors:
            pointer.actors.append(user)

        if user not in execution.actors:
            execution.actors.append(user)

        actor_json = {
            '_type': 'actor',
            'state': 'valid',
            'user': user.as_json(include=[
                '_type',
                'fullname',
                'identifier',
                'email',
            ]),
            'forms': forms,
        }

        values = self.compact_values(forms)

        # update state
        self.execution_collection().update_one(
            {
                'id': execution.id,
                '$or': [
                    {
                        'actor_list.node': node.id,
                        'actor_list.actor.identifier': {
                            '$ne': user.identifier,
                        },
                    },
                    {
                        'actor_list.node': {
                            '$ne': node.id,
                        },
                    },
                ],
            },
            {
                '$push': {
                    'actor_list': {
                        'node': node.id,
                        'actor': user.as_json(include=[
                            '_type',
                            'fullname',
                            'identifier',
                            'email',
                        ]),
                    },
                },
            },
        )

        mongo_exe = self.execution_collection().find_one_and_update(
            {'id': execution.id},
            {
                '$set': {**{
                    'state.items.{node}.state'.format(node=node.id): 'valid',
                    'state.items.{node}.actors.items.{identifier}'.format(
                        node=node.id,
                        identifier=user.identifier,
                    ): actor_json,
                }, **values},
            },
            return_document=pymongo.collection.ReturnDocument.AFTER,
        )

        context = make_context(mongo_exe, self.config)

        # update execution's name and description
        execution.name = render_or(
            execution.name_template,
            execution.name,
            context,
        )
        execution.description = render_or(
            execution.description_template,
            execution.description,
            context,
        )
        for value in execution.values:
            db_session.delete(value)
        execution.values = unpack_execution_values(execution, context)

        self.execution_collection().update_one(
            {'id': execution.id},
            {'$set': {
                'name': execution.name,
                'description': execution.description,
                'values._execution.0.name': execution.name,
                'values._execution.0.description': execution.description,
            }},
        )

        self.pointer_collection().update_many(
            {'execution.id': execution.id},
            {'$set': {
                'execution': execution.as_json(),
            }},
        )

        # update pointer
        self.pointer_collection().update_one({
            'id': pointer.id,
        }, {
            '$set': {
                'state': 'finished',
                'finished_at': current_time,
                'actors': Map(
                    [actor_json],
                    key=lambda a: a['user']['identifier'],
                ).to_json(),
                'actor_list': [
                    {
                        'form': form['ref'],
                        'actor': user.as_json(include=[
                            '_type',
                            'fullname',
                            'identifier',
                            'email',
                        ]),
                    } for form in forms
                ],
            },
        })

        execution.updated_at = current_time
        pointer.status = 'finished'
        pointer.updated_at = current_time
        pointer.finished_at = current_time
        db_session.commit()

        LOGGER.debug('Deleted pointer p:{} n:{} e:{}'.format(
            pointer.id,
            pointer.node_id,
            execution.id,
        ))

    def finish_execution(self, execution):
        """ shuts down this execution and every related object """
        current_time = datetime.datetime.now()

        execution.status = 'finished'
        execution.updated_at = current_time
        execution.finished_at = current_time

        for pointer in execution.pointers.filter(
            Pointer.status == Pointer.Status.ONGOING,
        ):
            pointer.status = Pointer.Status.CANCELLED
            pointer.updated_at = current_time
            pointer.finished_at = current_time

        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': {
                'status': execution.status,
                'finished_at': execution.finished_at,
            },
        })

        self.pointer_collection().update_many({
            'execution.id': execution.id,
        }, {
            '$set': {
                'execution': execution.as_json(),
            },
        })

        db_session.commit()

        LOGGER.debug('Finished e:{}'.format(execution.id))

    def compact_values(self, collected_forms):
        ''' Given an imput from a node create a representation that will be
        used to store the data in the 'values' key of the execution collection
        in mongodb. '''
        compact = {}

        for form in collected_forms:
            key = 'values.{}'.format(form['ref'])

            if key in compact:
                compact[key].append({
                    k: v['value'] for k, v in form['inputs']['items'].items()
                })
            else:
                compact[key] = [{
                    k: v['value'] for k, v in form['inputs']['items'].items()
                }]

        return compact

    def get_invalid_users(self, node_state):
        users = [
            identifier
            for identifier, actor in node_state['actors']['items'].items()
            if actor['state'] == 'invalid'
        ]

        return db_session.query(User).filter(
            User.identifier.in_(users),
        ).all()

    def notify_users(self, node, pointer, state):
        node_state = state['state']['items'][node.id]

        if node_state['state'] == 'invalid':
            users = self.get_invalid_users(node_state)
        else:
            users = node.get_actors(self.config, state)

        if type(users) is not list:
            raise MisconfiguredProvider('Provider returned non list')

        if len(users) == 0 and self.config['USERLESS_TASK_RAISES_ERROR']:
            raise InconsistentState(
                'No user assigned, dead execution {execution_id}'.format(
                    execution_id=pointer.execution.id,
                ),
            )

        notified_users = []

        for user in users:
            notified_users.append(user.as_json())

            if user not in pointer.candidates:
                pointer.candidates.append(user)

            mediums = self.get_contact_channels(
                user,
                self.config['NOTIFICATION_ASSIGNED_TASK_SUBJECT'],
                self.config['NOTIFICATION_ASSIGNED_TASK_TEMPLATE'],
            )

            for medium, params in mediums:
                body = json.dumps({
                    **{
                        'data': {
                            'pointer': {
                                **pointer.as_json(),
                                'execution': pointer.execution.as_json(),
                            },
                            'cacahuate_url': self.config['GUI_URL'],
                        },
                    },
                    **params,
                })

                cacahuate.rabbit.send_notify(
                    hostname=self.config['RABBIT_HOST'],
                    port=self.config['RABBIT_PORT'],
                    username=self.config['RABBIT_USER'],
                    password=self.config['RABBIT_PASS'],
                    heartbeat=self.config['RABBIT_HEARTBEAT'],
                    exchange=self.config['RABBIT_NOTIFY_EXCHANGE'],
                    medium=medium,
                    body=body,
                )

        db_session.commit()

        LOGGER.debug('Waking up n:{} found users: {} e:{}'.format(
            node.id,
            ', '.join(u.identifier for u in users),
            pointer.execution.id,
        ))

        return notified_users

    def get_mongo(self):
        if self.mongo is None:
            uri = self.config['MONGO_URI']

            with pymongo.MongoClient(uri) as client:
                database_name = pymongo.uri_parser.parse_uri(uri)['database']
                self.mongo = client[database_name]

        return self.mongo

    def execution_collection(self):
        return self.get_mongo()[self.config['EXECUTION_COLLECTION']]

    def pointer_collection(self):
        return self.get_mongo()[self.config['POINTER_COLLECTION']]

    def get_contact_channels(
        self,
        user: User,
        subject,
        template,
    ):
        return [('email', {
            'recipient': user.email,
            'subject': subject,
            'template': template,
        })]

    def _create_pointer(
        self,
        node_id: str,
        node_type: str,
        name: str,
        description: str,
        execution: Execution,
    ):
        ''' Given a node, its process, and a specific execution of the former
        create a persistent pointer to the current execution state '''
        pointer = Pointer(
            node_id=node_id,
            node_type=node_type,
            name=name,
            description=description,
            execution=execution,
            status=Pointer.Status.ONGOING,
            id=self.generate_pointer_id(),
        )

        db_session.add(pointer)
        db_session.commit()

        return pointer

    def recover_step(self, message: dict):
        ''' given an execution id and a pointer from the persistent storage,
        return the asociated process node to continue its execution '''
        try:
            pointer = db_session.query(Pointer).filter(
                Pointer.id == message['pointer_id'],
                Pointer.status == Pointer.Status.ONGOING,
            ).one()
        except sqlalchemy.orm.exc.NoResultFound:
            raise InconsistentState(
                'Queued dead pointer: {pointer_id}'.format(
                    pointer_id=message.get('pointer_id'),
                ),
            )

        user = db_session.query(User).filter(
            User.identifier == message.get('user_identifier'),
        ).first()

        if user is None:
            if message.get('user_identifier') == '__system__':
                user = cacahuate.models.get_or_create_user(
                    '__system__',
                    {
                        'identifier': '__system__',
                        'fullname': 'System',
                        'email': '',
                    },
                )
            else:
                raise InconsistentState(
                    'sent identifier of unexisten user: {user_id}'.format(
                        user_id=message.get('user_identifier'),
                    ),
                )

        return (
            pointer,
            user,
            message['input'],
        )

    def patch(self, message):
        current_time = datetime.datetime.now()

        execution = db_session.query(Execution).filter(
            Execution.id == message['execution_id'],
            Execution.status == Execution.Status.ONGOING,
        ).one()

        process_name = execution.process_name
        xml = Xml.load(
            self.config,
            process_name,
            direct=True,
        )

        # set nodes with pointers as unfilled, delete pointers
        updates = {}

        user = db_session.query(User).filter(
            User.identifier == message.get('user_identifier'),
        ).first()

        if user is None:
            if message.get('user_identifier') == '__system__':
                user = cacahuate.models.get_or_create_user(
                    '__system__',
                    {
                        'identifier': '__system__',
                        'fullname': 'System',
                        'email': '',
                    },
                )
            else:
                raise InconsistentState(
                    'sent identifier of unexisten user: {user_id}'.format(
                        user_id=message.get('user_identifier'),
                    ),
                )

        execution.updated_at = current_time
        for pointer in execution.pointers.filter(
            Pointer.status == Pointer.Status.ONGOING,
        ).all():
            pointer.status = Pointer.Status.CANCELLED
            pointer.updated_at = current_time
            pointer.finished_at = current_time

            if user not in pointer.actors:
                pointer.actors.append(user)

            updates['state.items.{node}.state'.format(
                node=pointer.node_id,
            )] = 'unfilled'

            self.pointer_collection().update_one({
                'id': pointer.id,
            }, {
                '$set': {
                    'state': 'cancelled',
                    'finished_at': current_time,
                    'patch': {
                        'comment': message['comment'],
                        'inputs': message['inputs'],
                        'actor': user.as_json(include=[
                            '_type',
                            'fullname',
                            'identifier',
                        ]),
                    },
                },
            })
        db_session.commit()

        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': updates,
        })

        # retrieve updated state
        state = next(self.execution_collection().find({'id': execution.id}))

        state_updates = cascade_invalidate(
            xml,
            state,
            message['inputs'],
            message['comment'],
        )

        # update state
        self.execution_collection().update_one({
            'id': state['id'],
        }, {
            '$set': state_updates,
        })

        # retrieve updated state
        state = next(self.execution_collection().find({'id': execution.id}))

        first_invalid_node = track_next_node(
            xml,
            state,
            self.get_mongo(),
            self.config,
        )

        # wakeup and start execution from the found invalid node
        self.wakeup_and_notify(first_invalid_node, execution, state)

    def cancel_execution(self, message):
        current_time = datetime.datetime.now()

        execution_id = message['execution_id']

        execution = db_session.query(Execution).filter(
            Execution.id == execution_id,
            Execution.status == Execution.Status.ONGOING,
        ).one()

        user = db_session.query(User).filter(
            User.identifier == message.get('user_identifier'),
        ).first()

        if user is None:
            if message.get('user_identifier') == '__system__':
                user = cacahuate.models.get_or_create_user(
                    '__system__',
                    {
                        'identifier': '__system__',
                        'fullname': 'System',
                        'email': '',
                    },
                )
            else:
                raise InconsistentState(
                    'sent identifier of unexisten user: {user_id}'.format(
                        user_id=message.get('user_identifier'),
                    ),
                )

        execution.status = 'cancelled'
        execution.finished_at = current_time

        if user not in execution.actors:
            execution.actors.append(user)

        for pointer in execution.pointers.filter(
            Pointer.status == Pointer.Status.ONGOING,
        ):
            pointer.status = Pointer.Status.CANCELLED
            pointer.updated_at = current_time
            pointer.finished_at = current_time

            if user not in pointer.actors:
                pointer.actors.append(user)

        self.execution_collection().update_one({
            'id': execution.id,
        }, {
            '$set': {
                'status': execution.status,
                'finished_at': execution.finished_at,
            },
        })

        self.pointer_collection().update_many({
            'execution.id': execution.id,
            'state': Pointer.Status.ONGOING,
        }, {
            '$set': {
                'state': 'cancelled',
                'finished_at': execution.finished_at,
            },
        })

        self.pointer_collection().update_many({
            'execution.id': execution.id,
        }, {
            '$set': {
                'execution': execution.as_json(),
            },
        })

        users = execution.actors.all()

        for user in users:
            mediums = self.get_contact_channels(
                user,
                self.config['NOTIFICATION_CANCELLED_ACTIVITY_SUBJECT'],
                self.config['NOTIFICATION_CANCELLED_ACTIVITY_TEMPLATE'],
            )

            for medium, params in mediums:
                body = json.dumps({
                    **{
                        'data': {
                            'execution': {
                                **execution.as_json(),
                            },
                            'cacahuate_url': self.config['GUI_URL'],
                        },
                    },
                    **params,
                })

                cacahuate.rabbit.send_notify(
                    hostname=self.config['RABBIT_HOST'],
                    port=self.config['RABBIT_PORT'],
                    username=self.config['RABBIT_USER'],
                    password=self.config['RABBIT_PASS'],
                    heartbeat=self.config['RABBIT_HEARTBEAT'],
                    exchange=self.config['RABBIT_NOTIFY_EXCHANGE'],
                    medium=medium,
                    body=body,
                )

        db_session.commit()

    def _generate_id(self):
        func = getattr(
            importlib.import_module(
                self.config['DB_ID_FUNCTION'].rsplit('.', 1)[0],
            ),
            self.config['DB_ID_FUNCTION'].rsplit('.', 1)[1],
        )

        return func()

    def generate_execution_id(self):
        new_id = self._generate_id()

        while db_session.query(Execution).filter(
            Execution.id == new_id,
        ).count() != 0:
            new_id = self._generate_id()

        return new_id

    def generate_pointer_id(self):
        new_id = self._generate_id()

        while db_session.query(Pointer).filter(
            Pointer.id == new_id,
        ).count() != 0:
            new_id = self._generate_id()

        return new_id
