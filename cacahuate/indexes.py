import pymongo


def create_indexes(config):
    # Create index
    uri = config['MONGO_URI']

    with pymongo.MongoClient(uri) as client:
        database_name = pymongo.uri_parser.parse_uri(uri)['database']
        db = client[database_name]

        db.execution.create_index("id", unique=True)
        db.execution.create_index("status")
        db.execution.create_index("started_at")
        db.execution.create_index("finished_at")
        db.execution.create_index("process_name")

        db.execution.create_index([
            ('name', pymongo.TEXT),
            ('description', pymongo.TEXT),
        ])

        db.pointer.create_index("id", unique=True)
        db.pointer.create_index("state")
        db.pointer.create_index("status")
        db.pointer.create_index("execution.id")
        db.pointer.create_index("execution.status")
        db.pointer.create_index("execution.process_name")
        db.pointer.create_index("started_at")
        db.pointer.create_index("finished_at")

        db.pointer.create_index([
            ('node.name', pymongo.TEXT),
            ('node.description', pymongo.TEXT),
            ('execution.name', pymongo.TEXT),
            ('execution.description', pymongo.TEXT),
        ])
