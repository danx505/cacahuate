import os

from cacahuate.errors import (
    ProcessNotFound,
)
from cacahuate.http.errors import (
    NotFound,
)
from cacahuate.http.middleware import (
    pagination,
    requires_auth,
)
from cacahuate.http.mongo import mongo
from cacahuate.mongo import json_prepare
from cacahuate.xml import (
    Xml,
    form_to_dict,
)

import flask
from flask import g
from flask import jsonify
from flask import request


bp = flask.Blueprint('process', __name__)


@bp.route('/process', methods=['GET'])
@requires_auth
def list_process():
    def add_form(xml):
        json_xml = xml.to_json()
        forms = []
        xmliter = iter(xml)
        first_node = next(xmliter)
        xmliter.parser.expandNode(first_node)

        for form in first_node.getElementsByTagName('form'):
            forms.append(form_to_dict(form))

        json_xml['form_array'] = forms

        return json_xml

    return jsonify({
        'data': list(filter(
            lambda x: x,
            map(
                add_form,
                Xml.processes(flask.current_app.config),
            ),
        )),
    })


@bp.route('/process/<name>', methods=['GET'])
@requires_auth
def find_process(name):
    def add_form(xml):
        json_xml = xml.to_json()
        forms = []
        xmliter = iter(xml)
        first_node = next(xmliter)
        xmliter.parser.expandNode(first_node)

        for form in first_node.getElementsByTagName('form'):
            forms.append(form_to_dict(form))

        json_xml['form_array'] = forms

        return json_xml

    version = request.args.get('version', '')

    if version:
        version = ".{}".format(version)

    process_name = "{}{}".format(name, version)

    try:
        xml = Xml.load(flask.current_app.config, process_name)
    except ProcessNotFound:
        raise NotFound([{
            'detail': '{} process does not exist'
                      .format(process_name),
            'where': 'request.body.process_name',
        }])

    return jsonify({
        'data': add_form(xml),
    })


@bp.route('/process/<name>.xml', methods=['GET'])
@requires_auth
def xml_process(name):
    version = request.args.get('version', '')

    if version:
        version = ".{}".format(version)

    process_name = "{}{}".format(name, version)

    try:
        xml = Xml.load(flask.current_app.config, process_name)
    except ProcessNotFound:
        raise NotFound([{
            'detail': '{} process does not exist'
                      .format(process_name),
            'where': 'request.body.process_name',
        }])
    ruta = os.path.join(flask.current_app.config['XML_PATH'], xml.filename)
    return open(ruta).read(), {'Content-Type': 'text/xml; charset=utf-8'}


@bp.route('/process/<pk>/statistics', methods=['GET'])
@requires_auth
def node_statistics(pk):
    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]
    query = [
        {"$match": {"process_id": pk}},
        {"$project": {
            "process_id": "$process_id",
            "node": "$node.id",
            "difference_time": {
                "$subtract": ["$finished_at", "$started_at"],
            },
        }},
        {"$group": {
            "_id": {"process_id": "$process_id", "node": "$node"},
            "process_id": {"$first": "$process_id"},
            "node": {"$first": "$node"},
            "max": {
                "$max": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "min": {
                "$min": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "average": {
                "$avg": {
                    "$divide": ["$difference_time", 1000],
                },
            },
        }},
        {"$sort": {"execution": 1, "node": 1}},
    ]
    return jsonify({
        "data": list(map(
            json_prepare,
            collection.aggregate(query),
        )),
    })


@bp.route('/process/statistics', methods=['GET'])
@requires_auth
@pagination
def process_statistics():
    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    query = [
        {"$match": {"status": "finished"}},
        {"$skip": g.offset},
        {"$limit": g.limit},
        {"$project": {
            "difference_time": {
                "$subtract": ["$finished_at", "$started_at"],
            },
            "process": {"id": "$process.id"},
        }},

        {"$group": {
            "_id": "$process.id",
            "process": {"$first": "$process.id"},
            "max": {
                "$max": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "min": {
                "$min": {
                    "$divide": ["$difference_time", 1000],
                },
            },
            "average": {
                "$avg": {
                    "$divide": ["$difference_time", 1000],
                },
            },

        }},
        {"$sort": {"process": 1}},
    ]

    return jsonify({
        "data": list(map(
            json_prepare,
            collection.aggregate(query),
        )),
    })
