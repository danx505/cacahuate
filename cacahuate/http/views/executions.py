import http

from cacahuate.database import db_session
from cacahuate.database.query import query_executions
from cacahuate.http.middleware import (
    pagination,
    permission_required,
    requires_auth,
)
from cacahuate.http.mongo import mongo
from cacahuate.models import Execution
from cacahuate.mongo import json_prepare

from dateutil.parser import parse

import flask


bp = flask.Blueprint('cacahuate_executions', __name__)


@bp.route('/executions', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_execution',
])
@pagination
def view_executions():
    dict_args = flask.request.args.to_dict()

    payload = {}

    payload['offset'] = flask.g.offset
    payload['limit'] = flask.g.limit

    mapper = {
        'search_query': str,
        'process_name__contains': str,
        'process_name__eq': str,
        'process_name__in': lambda x: str(x).split(','),
        'process_name__startswith': str,
        'started_at__gt': parse,
        'started_at__lt': parse,
        'started_at__date__ge': parse,
        'started_at__date__gt': parse,
        'started_at__date__le': parse,
        'started_at__date__lt': parse,
        'finished_at__gt': parse,
        'finished_at__lt': parse,
        'status__eq': str,
        'status__in': lambda x: str(x).split(','),
        'tasks__started_at__gt': parse,
        'tasks__started_at__lt': parse,
        'tasks__started_at__date__ge': parse,
        'tasks__started_at__date__gt': parse,
        'tasks__started_at__date__le': parse,
        'tasks__started_at__date__lt': parse,
        'tasks__finished_at__gt': parse,
        'tasks__finished_at__lt': parse,
        'tasks__status__eq': str,
        'tasks__status__in': lambda x: str(x).split(','),
        'tasks__actors__identifier__eq': str,
        'tasks__actors__identifier__in': lambda x: str(x).split(','),
        'tasks__actors__groups__codename__eq': str,
        'tasks__actors__groups__codename__in': lambda x: str(x).split(','),
        'tasks__candidates__identifier__eq': str,
        'tasks__candidates__identifier__in': lambda x: str(x).split(','),
        'tasks__candidates__groups__codename__eq': str,
        'tasks__candidates__groups__codename__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if dict_args.get(key) is not None:
            payload[key] = fn(dict_args[key])

    query = query_executions(**payload)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,
    )


@bp.route('/executions/<pk>', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_execution',
])
def view_execution(pk):
    execution = db_session.query(Execution).filter(
        Execution.id == pk,
    ).one()

    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]

    doc = collection.find_one({'id': pk})

    return flask.make_response(
        flask.jsonify({
            **execution.as_json(),
            'data': json_prepare(doc) if doc else None,
        }),
        http.HTTPStatus.OK,
    )


@bp.route('/executions/<pk>/tasks', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_execution',
    'auth.view_task',
])
@pagination
def view_execution_tasks(pk):
    execution = db_session.query(Execution).get({
        'id': pk,
    })

    query = execution.pointers

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,
    )
