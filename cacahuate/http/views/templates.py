import importlib
from os import path

from cacahuate.http.mongo import mongo
from cacahuate.mongo import make_context
from cacahuate.templates import datetimeformat

import flask

from jinja2 import (
    Environment,
    FileSystemLoader,
    select_autoescape,
)

import sqlalchemy.orm.exc

bp = flask.Blueprint('template', __name__)


@bp.route('/execution/<pk>/summary', methods=['GET'])
def execution_template(pk):
    # load values
    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]

    try:
        execution = next(collection.find({'id': pk}))
    except StopIteration:
        raise sqlalchemy.orm.exc.NoResultFound(
            'Specified execution never existed, and never will',
        )

    if 'process_name' not in execution:
        return 'Not supported for old processes', 409

    context = make_context(execution, flask.current_app.config)

    # load template
    process_name = execution['process_name']
    name, version, _ = process_name.split('.')

    # Loaders will be inserted in inverse order and then reversed. The fallback
    # is the default template at ``templates/summary.html``
    paths = [
        path.join(path.dirname(path.realpath(__file__)), '../../templates'),
    ]

    app_template_path = flask.current_app.config['TEMPLATE_PATH']
    if app_template_path is not None and path.isdir(app_template_path):
        paths.append(app_template_path)

        process_dir = path.join(app_template_path, name)
        if path.isdir(process_dir):
            paths.append(process_dir)

        process_version_dir = path.join(
            process_dir,
            version,
        )
        if path.isdir(process_version_dir):
            paths.append(process_version_dir)

    env = Environment(
        loader=FileSystemLoader(reversed(paths)),
        autoescape=select_autoescape(['html', 'xml']),
    )

    env.filters['datetimeformat'] = datetimeformat

    for name, function in flask.current_app.config['JINJA_FILTERS'].items():
        if callable(function):
            env.filters[name] = function

        else:
            env.filters[name] = getattr(
                importlib.import_module(
                    function.rsplit('.', 1)[0],
                ),
                function.rsplit('.', 1)[1],
            )

    return flask.make_response(
        env.get_template('summary.html').render(**context),
        200,
    )
