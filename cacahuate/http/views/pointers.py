import collections
import http
import os
import xml

from cacahuate.database import db_session
from cacahuate.database.query import query_pointers
from cacahuate.http.middleware import (
    pagination,
    permission_required,
    requires_auth,
)
from cacahuate.models import Pointer
from cacahuate.xml.parser import (
    parse_xml,
    translate_parsed_process,
)

from dateutil.parser import parse

import flask


bp = flask.Blueprint('cacahuate_pointers', __name__)


@bp.route('/pointers', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
@pagination
def get_pointers():
    dict_args = flask.request.args.to_dict()

    payload = {}

    payload['offset'] = flask.g.offset
    payload['limit'] = flask.g.limit

    mapper = {
        'search_query': str,
        'node_id__contains': str,
        'node_id__eq': str,
        'node_id__in': lambda x: str(x).split(','),
        'node_id__startswith': str,
        'started_at__gt': parse,
        'started_at__lt': parse,
        'started_at__date__ge': parse,
        'started_at__date__gt': parse,
        'started_at__date__le': parse,
        'started_at__date__lt': parse,
        'finished_at__gt': parse,
        'finished_at__lt': parse,
        'status__eq': str,
        'status__in': lambda x: str(x).split(','),
        'execution__id__eq': str,
        'execution__id__in': lambda x: str(x).split(','),
        'execution__process_name__contains': str,
        'execution__process_name__eq': str,
        'execution__process_name__in': lambda x: str(x).split(','),
        'execution__process_name__startswith': str,
        'actors__identifier__eq': str,
        'actors__identifier__in': lambda x: str(x).split(','),
        'actors__groups__codename__eq': str,
        'actors__groups__codename__in': lambda x: str(x).split(','),
        'candidates__identifier__eq': str,
        'candidates__identifier__in': lambda x: str(x).split(','),
        'candidates__groups__codename__eq': str,
        'candidates__groups__codename__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if dict_args.get(key) is not None:
            payload[key] = fn(dict_args[key])

    query = query_pointers(**payload)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


@bp.route('/pointers/<pk>/forms', methods=['GET'])
@requires_auth
@permission_required([
    'auth.view_pointer',
])
def get_pointer_forms(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
    ).one()

    xml_path = os.path.join(
        flask.current_app.config['XML_PATH'],
        pointer.execution.process_name,
    )
    with open(xml_path) as f:
        parsed = parse_xml(xml.dom.pulldom.parse(f))
    process_schema = translate_parsed_process(parsed)

    forms_schema = process_schema['fieldsets']
    task_fields = process_schema['tasks'][pointer.node_id]['fields']

    forms = collections.OrderedDict()
    for fields in task_fields:
        form_id, field_id = fields.split('.')

        forms.setdefault(
            form_id,
            {
                'id': forms_schema[form_id]['id'],
                'min_amount': forms_schema[form_id]['min_amount'],
                'max_amount': forms_schema[form_id]['max_amount'],
                'fields': [],
                'values': [],
            },
        )['fields'].append(
            process_schema['fieldsets'][form_id]['fields'][field_id].copy(),
        )

    related_values = {}
    for value in sorted(
        pointer.execution.values,
        key=lambda item: item.fieldset_index,
        reverse=True,
    ):
        reference = f'{value.fieldset_ref}.{value.field_ref}'
        if reference not in task_fields:
            continue

        related_values.setdefault(
            value.fieldset_ref,
            [{}] * (value.fieldset_index + 1),
        )[value.fieldset_index][value.field_ref] = value.value_entry

    for fieldset_ref, values in related_values.items():
        forms[fieldset_ref]['values'] = values

    return flask.make_response(
        flask.jsonify(
            list(forms.values()),
        ),
        http.HTTPStatus.OK,
    )
