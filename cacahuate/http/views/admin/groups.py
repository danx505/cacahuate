import http

import cacahuate.errors
import cacahuate.http.middleware
import cacahuate.http.validation
from cacahuate.database import db_session
from cacahuate.database.query import query_groups
from cacahuate.models import (
    Group,
    Permission,
    User,
)

import flask


bp = flask.Blueprint('cacahuate_groups', __name__)


# TODO: Delete legacy url
@bp.route(
    '/group',
    methods=['GET'],
)
@bp.route(
    '/groups',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_group',
])
@cacahuate.http.middleware.pagination
def view_groups():
    dict_args = flask.request.args.to_dict()

    payload = {}

    payload['offset'] = flask.g.offset
    payload['limit'] = flask.g.limit

    mapper = {
        'search_query': str,
        'codename__eq': str,
        'codename__in': lambda x: str(x).split(','),
        'users__identifier__eq': str,
        'users__identifier__in': lambda x: str(x).split(','),
        'users__email__eq': str,
        'users__email__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if dict_args.get(key) is not None:
            payload[key] = fn(dict_args[key])

    query = query_groups(**payload)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/group',
    methods=['POST'],
)
@bp.route(
    '/groups',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.add_group',
])
def add_group():
    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name'],
    )

    group = Group(
        name=flask.request.json['name'],
        codename=flask.request.json['codename'],
    )

    db_session.add(group)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': group.codename,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>',
    methods=['PUT'],
)
@bp.route(
    '/groups/<codename>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def set_group(codename):
    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename', 'name'],
    )

    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).first()

    if group is None:
        group = Group(**{
            **flask.request.json,
            'codename': codename,
        })
        db_session.add(group)
    else:
        for k, v in {
            **flask.request.json,
            'codename': codename,
        }.items():
            setattr(group, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': group.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route('/group/<codename>', methods=['GET'])
@bp.route('/groups/<codename>', methods=['GET'])
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_group',
])
def view_group(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'codename': group.codename,
            'name': group.name,
        }),
        http.HTTPStatus.OK,  # 200
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>',
    methods=['DELETE'],
)
@bp.route(
    '/groups/<codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.delete_group',
])
def delete_group(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    db_session.delete(group)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': group.codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/users',
    methods=['GET'],
)
@bp.route(
    '/groups/<codename>/users',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_group',
])
@cacahuate.http.middleware.pagination
def view_group_users(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    query = group.users

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/users',
    methods=['POST'],
)
@bp.route(
    '/groups/<codename>/users',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def add_group_user(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['identifier'],
    )

    user = db_session.query(User).filter(
        User.identifier == flask.request.json['identifier'],
    ).one()

    group.users.append(user)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/users',
    methods=['PUT'],
)
@bp.route(
    '/groups/<codename>/users',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def set_group_user(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    mapped = {
      x['identifier']: x
      for x in flask.request.json
    }

    items = []
    for item in group.users:
        if item.identifier not in mapped:
            continue

        for k, v in mapped[item.identifier].items():
            setattr(item, k, v)

        items.append(item)
        mapped.pop(item.identifier)

    for item in mapped.values():
        items.append(
            db_session.query(User).filter(
                User.identifier == item['identifier'],
            ).one(),
        )

    group.users = items

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/users/<identifier>',
    methods=['DELETE'],
)
@bp.route(
    '/groups/<codename>/users/<identifier>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def remove_group_user(identifier, codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    group.users.remove(user)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/permissions',
    methods=['GET'],
)
@bp.route(
    '/groups/<codename>/permissions',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_group',
])
@cacahuate.http.middleware.pagination
def view_group_permissions(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    query = group.permissions

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/permissions',
    methods=['POST'],
)
@bp.route(
    '/groups/<codename>/permissions',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def add_group_permission(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename'],
    )

    permission = db_session.query(Permission).filter(
        Permission.codename == flask.request.json['codename'],
    ).one()

    group.permissions.append(permission)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<codename>/permissions',
    methods=['PUT'],
)
@bp.route(
    '/groups/<codename>/permissions',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def set_group_permission(codename):
    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    mapped = {
      x['codename']: x
      for x in flask.request.json
    }

    items = []
    for item in group.permissions:
        if item.codename not in mapped:
            continue

        for k, v in mapped[item.codename].items():
            setattr(item, k, v)

        items.append(item)
        mapped.pop(item.codename)

    for item in mapped.values():
        items.append(
            db_session.query(Permission).filter(
                Permission.codename == item['codename'],
            ).one(),
        )

    group.permissions = items

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/group/<group_codename>/permissions/<permission_codename>',
    methods=['DELETE'],
)
@bp.route(
    '/groups/<group_codename>/permissions/<permission_codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.change_group',
])
def remove_group_permission(group_codename, permission_codename):
    group = db_session.query(Group).filter(
        Group.codename == group_codename,
    ).one()

    permission = db_session.query(Permission).filter(
        Permission.codename == permission_codename,
    ).one()

    group.permissions.remove(permission)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'codename': group_codename,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )
