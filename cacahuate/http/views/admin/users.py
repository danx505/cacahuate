import http

import cacahuate.errors
import cacahuate.http.middleware
import cacahuate.http.validation
from cacahuate.database import db_session
from cacahuate.database.query import query_users
from cacahuate.models import (
    Group,
    Permission,
    Pointer,
    Quirk,
    User,
)

import flask


bp = flask.Blueprint('cacahuate_users', __name__)


# TODO: Delete legacy url
@bp.route(
    '/user',
    methods=['GET'],
)
@bp.route(
    '/users',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
@cacahuate.http.middleware.pagination
def view_users():
    dict_args = flask.request.args.to_dict()

    payload = {}

    payload['offset'] = flask.g.offset
    payload['limit'] = flask.g.limit

    mapper = {
        'search_query': str,
        'identifier__eq': str,
        'identifier__in': lambda x: str(x).split(','),
        'email__eq': str,
        'email__in': lambda x: str(x).split(','),
        'groups__codename__eq': str,
        'groups__codename__in': lambda x: str(x).split(','),
    }

    for key, fn in mapper.items():
        if dict_args.get(key) is not None:
            payload[key] = fn(dict_args[key])

    query = query_users(**payload)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user',
    methods=['POST'],
)
@bp.route(
    '/users',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.add_user',
])
def add_user():
    cacahuate.http.validation.validate_json(
        flask.request.json,
        User.get_fields(ignore=['id']),
    )

    user = User(
        fullname=flask.request.json['fullname'],
        identifier=flask.request.json['identifier'],
        email=flask.request.json['email'],
        is_superuser=flask.request.json['is_superuser'],
        is_admin=flask.request.json['is_admin'],
        is_staff=flask.request.json['is_staff'],
        is_active=flask.request.json['is_active'],
    )

    db_session.add(user)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': user.identifier,
            'fullname': user.fullname,
            'email': user.email,
        }),
        http.HTTPStatus.CREATED,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>',
    methods=['PUT'],
)
@bp.route(
    '/users/<identifier>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def set_user(identifier):
    cacahuate.http.validation.validate_json(
        flask.request.json,
        User.get_fields(ignore=['id', 'identifier']),
    )

    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).first()

    if user is None:
        user = User(**{
            **flask.request.json,
            'identifier': identifier,
        })
        db_session.add(user)
    else:
        for k, v in {
            **flask.request.json,
            'identifier': identifier,
        }.items():
            setattr(user, k, v)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': user.identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route('/user/<identifier>', methods=['GET'])
@bp.route('/users/<identifier>', methods=['GET'])
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
def view_user(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    return flask.make_response(
        flask.jsonify({
            'identifier': user.identifier,
            'fullname': user.fullname,
            'email': user.email,
            'is_superuser': user.is_superuser,
            'is_admin': user.is_admin,
            'is_staff': user.is_staff,
            'is_active': user.is_active,
        }),
        http.HTTPStatus.OK,  # 200
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>',
    methods=['DELETE'],
)
@bp.route(
    '/users/<identifier>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.delete_user',
])
def delete_user(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    db_session.delete(user)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': user.identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/groups',
    methods=['GET'],
)
@bp.route(
    '/users/<identifier>/groups',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
@cacahuate.http.middleware.pagination
def view_user_groups(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    query = user.groups

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/groups',
    methods=['POST'],
)
@bp.route(
    '/users/<identifier>/groups',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def add_user_group(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename'],
    )

    group = db_session.query(Group).filter(
        Group.codename == flask.request.json['codename'],
    ).one()

    user.groups.append(group)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/groups',
    methods=['PUT'],
)
@bp.route(
    '/users/<identifier>/groups',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def set_user_group(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    mapped = {
      x['codename']: x
      for x in flask.request.json
    }

    items = []
    for item in user.groups:
        if item.codename not in mapped:
            continue

        for k, v in mapped[item.codename].items():
            setattr(item, k, v)

        items.append(item)
        mapped.pop(item.codename)

    for item in mapped.values():
        items.append(
            db_session.query(Group).filter(
                Group.codename == item['codename'],
            ).one(),
        )

    user.groups = items

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/groups/<codename>',
    methods=['DELETE'],
)
@bp.route(
    '/users/<identifier>/groups/<codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def remove_user_group(identifier, codename):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    group = db_session.query(Group).filter(
        Group.codename == codename,
    ).one()

    user.groups.remove(group)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/permissions',
    methods=['GET'],
)
@bp.route(
    '/users/<identifier>/permissions',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
@cacahuate.http.middleware.pagination
def view_user_permissions(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    query = user.permissions

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/permissions',
    methods=['POST'],
)
@bp.route(
    '/users/<identifier>/permissions',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def add_user_permission(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename'],
    )

    permission = db_session.query(Permission).filter(
        Permission.codename == flask.request.json['codename'],
    ).one()

    user.permissions.append(permission)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/permissions',
    methods=['PUT'],
)
@bp.route(
    '/users/<identifier>/permissions',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def set_user_permission(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    mapped = {
      x['codename']: x
      for x in flask.request.json
    }

    items = []
    for item in user.permissions:
        if item.codename not in mapped:
            continue

        for k, v in mapped[item.codename].items():
            setattr(item, k, v)

        items.append(item)
        mapped.pop(item.codename)

    for item in mapped.values():
        items.append(
            db_session.query(Permission).filter(
                Permission.codename == item['codename'],
            ).one(),
        )

    user.permissions = items

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/permissions/<codename>',
    methods=['DELETE'],
)
@bp.route(
    '/users/<identifier>/permissions/<codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def remove_user_permission(identifier, codename):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    permission = db_session.query(Permission).filter(
        Permission.codename == codename,
    ).one()

    user.permissions.remove(permission)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/assigned-tasks',
    methods=['GET'],
)
@bp.route(
    '/users/<identifier>/assigned-tasks',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
@cacahuate.http.middleware.pagination
def view_user_assigned_tasks(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    dict_args = flask.request.args.to_dict()

    query = user.assigned_tasks
    query = query.order_by(Pointer.started_at.desc())

    raw_q = dict_args.get('status')
    if raw_q is not None:
        clean_q = raw_q
        query = query.filter(Pointer.status == clean_q)

    raw_q = dict_args.get('status__eq')
    if raw_q is not None:
        clean_q = raw_q
        query = query.filter(Pointer.status == clean_q)

    raw_q = dict_args.get('status__in')
    if raw_q is not None:
        clean_q = raw_q.split(',')
        query = query.filter(Pointer.status.in_(clean_q))

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/solved-tasks',
    methods=['GET'],
)
@bp.route(
    '/users/<identifier>/solved-tasks',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
@cacahuate.http.middleware.pagination
def view_user_solved_tasks(identifier):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    dict_args = flask.request.args.to_dict()

    query = user.solved_tasks
    query = query.order_by(Pointer.started_at.desc())

    raw_q = dict_args.get('status')
    if raw_q is not None:
        clean_q = raw_q
        query = query.filter(Pointer.status == clean_q)

    raw_q = dict_args.get('status__eq')
    if raw_q is not None:
        clean_q = raw_q
        query = query.filter(Pointer.status == clean_q)

    raw_q = dict_args.get('status__in')
    if raw_q is not None:
        clean_q = raw_q.split(',')
        query = query.filter(Pointer.status.in_(clean_q))

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/<category_codename>',
    methods=['GET'],
)
@bp.route(
    '/users/<identifier>/<category_codename>',
    methods=['GET'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.view_user',
])
@cacahuate.http.middleware.pagination
def view_user_quirks(identifier, category_codename):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    query = user.quirks.filter(Quirk.category.has(codename=category_codename))

    query = query.offset(flask.g.offset).limit(flask.g.limit)

    return flask.make_response(
        flask.jsonify({
            'items': [
                x.as_json()
                for x in query
            ],
        }),
        http.HTTPStatus.OK,  # 201
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/<category_codename>',
    methods=['POST'],
)
@bp.route(
    '/users/<identifier>/<category_codename>',
    methods=['POST'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def add_user_quirks(identifier, category_codename):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    cacahuate.http.validation.validate_json(
        flask.request.json,
        ['codename'],
    )

    quirk = db_session.query(Quirk).filter(
        Quirk.codename == flask.request.json['codename'],
        Quirk.category.has(codename=category_codename),
    ).one()

    user.quirks.append(quirk)

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/<category_codename>',
    methods=['PUT'],
)
@bp.route(
    '/users/<identifier>/<category_codename>',
    methods=['PUT'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.requires_json
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def set_user_quirks(identifier, category_codename):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    mapped = {
      x['codename']: x
      for x in flask.request.json
    }

    items = []
    for item in user.quirks:
        if item.category.codename != category_codename:
            items.append(item)
            continue

        if item.codename not in mapped:
            continue

        for k, v in mapped[item.codename].items():
            setattr(item, k, v)

        items.append(item)
        mapped.pop(item.codename)

    for item in mapped.values():
        items.append(
            db_session.query(Quirk).filter(
                Quirk.codename == item['codename'],
                Quirk.category.has(codename=category_codename),
            ).one(),
        )

    user.quirks = items

    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )


# TODO: Delete legacy url
@bp.route(
    '/user/<identifier>/<category_codename>/<codename>',
    methods=['DELETE'],
)
@bp.route(
    '/users/<identifier>/<category_codename>/<codename>',
    methods=['DELETE'],
)
@cacahuate.http.middleware.requires_auth
@cacahuate.http.middleware.permission_required([
    'auth.change_user',
])
def remove_user_quirk(identifier, category_codename, codename):
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).one()

    quirk = db_session.query(Quirk).filter(
        Quirk.codename == codename,
        Quirk.category.has(codename=category_codename),
    ).one()

    user.quirks.remove(quirk)
    db_session.commit()

    return flask.make_response(
        flask.jsonify({
            'identifier': identifier,
        }),
        http.HTTPStatus.ACCEPTED,  # 202
    )
