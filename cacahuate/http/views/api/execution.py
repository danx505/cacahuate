import datetime
import http
from json.decoder import JSONDecodeError

from cacahuate.database import db_session
from cacahuate.errors import (
    InvalidInputError,
    MalformedProcess,
    ProcessNotFound,
    RequiredListError,
)
from cacahuate.http.errors import (
    BadRequest,
    NotFound,
    UnprocessableEntity,
)
from cacahuate.http.middleware import (
    pagination,
    requires_auth,
    requires_json,
)
from cacahuate.http.mongo import mongo
from cacahuate.http.validation import (
    validate_auth,
    validate_json,
    validate_patch_inputs,
)
from cacahuate.models import (
    Execution,
    Pointer,
    User,
)
from cacahuate.mongo import json_prepare
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import make_node

import flask

import pymongo

import sqlalchemy.orm.exc


# TODO: is this really required? maybe get doesn't need to support search
def format_query(q):
    try:
        formated_q = flask.json.loads(q)
    except JSONDecodeError:
        formated_q = q
    return formated_q


bp = flask.Blueprint('cacahuate_execution_legacy', __name__)


@bp.route('/execution/search', methods=['POST'])
@requires_auth
@requires_json
@pagination
def execution_list_search():
    dict_args = flask.request.get_json()

    # format query
    exe_query = {
        k: v
        for k, v in dict_args.items()
        if k not in flask.current_app.config['INVALID_FILTERS']
    }

    # sort
    srt = {'started_at': -1}
    sort_query = exe_query.pop('sort', None)
    if sort_query and sort_query.split(',', 1)[0]:
        try:
            key, order = sort_query.split(',', 1)
        except ValueError:
            key, order = sort_query, 'ASCENDING'

        if order not in ['ASCENDING', 'DESCENDING']:
            order = 'ASCENDING'

        order = getattr(pymongo, order)
        srt = {key: order}

    # filter for user_identifier
    user_identifier = exe_query.pop('user_identifier', None)
    if user_identifier is not None:
        user = db_session.query(User).filter(
            User.identifier == user_identifier,
        ).first()
        if user is not None:
            execution_list = [item.id for item in user.activities.all()]
        else:
            execution_list = []
        db_session.commit()
        exe_query['id'] = {
            '$in': execution_list,
        }

    # filter for exclude/include
    exclude_list = exe_query.pop('exclude', None) or []
    exclude_map = {item: 0 for item in exclude_list}

    include_list = exe_query.pop('include', None) or []
    include_map = {item: 1 for item in include_list}

    # store project for future use
    prjct = {**include_map} or {**exclude_map}

    exe_collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]

    try:
        max_time_ms = (
            flask.current_app.config['MONGO_EXECUTION_COLLECTION_MAX_TIME_MS']
        )
        cursor_count = exe_collection.count_documents(
            exe_query,
            maxTimeMS=max_time_ms,
        )
        cursor = exe_collection.find(
            exe_query,
            prjct or None,
        ).sort(list(srt.items())).max_time_ms(
            max_time_ms,
        )

        return flask.make_response(
            flask.jsonify({
                'total_count': cursor_count,
                'items': list(map(
                    json_prepare,
                    cursor.skip(flask.g.offset).limit(flask.g.limit),
                )),
            }),
            http.HTTPStatus.OK,  # 200
        )
    except pymongo.errors.OperationFailure:
        flask.abort(400, 'Malformed query')
    except pymongo.errors.ExecutionTimeout:
        flask.abort(400, 'Cant handle requested query')


@bp.route('/execution/<execution_id>', methods=['GET'])
@requires_auth
def process_status(execution_id):
    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]

    try:
        exc = next(collection.find({'id': execution_id}))
    except StopIteration:
        raise sqlalchemy.orm.exc.NoResultFound(
            'Specified execution never existed, and never will',
        )

    return flask.jsonify({
        'data': json_prepare(exc),
    })


@bp.route('/execution/<pk>', methods=['PATCH'])
@requires_auth
def execution_patch(pk):
    execution = db_session.query(Execution).filter(
        Execution.id == pk,
        Execution.status == Execution.Status.ONGOING,
    ).first()

    if execution is None:
        flask.abort(404)

    validate_json(flask.request.json, ['comment', 'inputs'])

    if type(flask.request.json['inputs']) is not list:
        raise RequiredListError('inputs', 'request.body.inputs')

    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    execution_state = next(collection.find({'id': pk}))

    xml = Xml.load(
        flask.current_app.config,
        execution.process_name,
        direct=True,
    )

    processed_inputs = validate_patch_inputs(
        flask.request.json['inputs'],
        execution_state,
        xml,
    )

    handle.delay(flask.json.dumps({
        'command': 'patch',
        'execution_id': execution.id,
        'comment': flask.request.json['comment'],
        'inputs': processed_inputs,
        'user_identifier': flask.g.user.identifier,
    }))

    return flask.jsonify({
        'data': 'accepted',
    }), 202


@bp.route('/execution/<exe_id>/user', methods=['POST'])
@requires_auth
def execution_add_user(exe_id):
    ''' adds the user as a candidate for solving the given node, only if the
    node has an active pointer. '''
    # TODO possible race condition introduced here. How does this code work in
    # case the handler is moving the pointer?
    current_time = datetime.datetime.now()

    # get execution
    execution = db_session.query(Execution).filter(
        Execution.id == exe_id,
        Execution.status == Execution.Status.ONGOING,
    ).first()

    if execution is None:
        flask.abort(404)

    # validate the members needed
    validate_json(flask.request.json, ['identifier', 'node_id'])

    identifier = flask.request.json['identifier']
    node_id = flask.request.json['node_id']

    # get actual pointer
    pointer = execution.pointers.filter(
        Pointer.node_id == node_id,
        Pointer.status == Pointer.Status.ONGOING,
    ).first()
    if pointer is None:
        raise BadRequest([{
            'detail': f'{node_id} does not have a live pointer',
            'code': 'validation.no_live_pointer',
            'where': 'request.body.node_id',
        }])

    # get user
    user = db_session.query(User).filter(
        User.identifier == identifier,
    ).first()
    if user is None:
        raise InvalidInputError('user_id', 'request.body.identifier')

    # update user
    if pointer not in user.assigned_tasks:
        user.assigned_tasks.append(pointer)

    user_json = user.as_json()

    # update pointer
    ptr_collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]
    ptr = ptr_collection.find_one({
        'execution.id': exe_id,
        'id': pointer.id,
        'node.id': node_id,
        'state': Pointer.Status.ONGOING,
    })

    if not ptr:
        raise BadRequest([{
            'detail': f'{node_id} does not have a live pointer',
            'code': 'validation.no_live_pointer',
            'where': 'request.body.node_id',
        }])

    if user_json['identifier'] not in [
        x['identifier'] for x in ptr['notified_users']
    ]:
        ptr['notified_users'].append(user_json)

    ptr_collection.update_one(
        {
            'execution.id': exe_id,
            'id': pointer.id,
            'node.id': node_id,
            'state': Pointer.Status.ONGOING,
        },
        {'$set': {'notified_users': ptr['notified_users']}},
    )

    execution.updated_at = current_time
    pointer.updated_at = current_time
    db_session.commit()

    return flask.jsonify(user_json), 200


@bp.route('/execution/<pk>', methods=['DELETE'])
@requires_auth
def delete_process(pk):
    execution = db_session.query(Execution).filter(
        Execution.id == pk,
        Execution.status == Execution.Status.ONGOING,
    ).first()

    if execution is None:
        flask.abort(404)

    handle.delay(flask.json.dumps({
        'command': 'cancel',
        'execution_id': execution.id,
        'user_identifier': flask.g.user.identifier,
    }))

    return flask.jsonify({
        'data': 'accepted',
    }), 202


@bp.route('/execution', methods=['POST'])
@requires_auth
@requires_json
def start_process():
    validate_json(flask.request.json, ['process_name'])

    try:
        xml = Xml.load(
            flask.current_app.config,
            flask.request.json['process_name'],
        )
    except ProcessNotFound:
        raise NotFound([{
            'detail': '{} process does not exist'
                      .format(flask.request.json['process_name']),
            'where': 'request.body.process_name',
        }])
    except MalformedProcess as e:
        raise UnprocessableEntity([{
            'detail': str(e),
            'where': 'request.body.process_name',
        }])

    xmliter = iter(xml)
    node = make_node(next(xmliter), xmliter)

    # Check for authorization
    validate_auth(node, flask.g.user)

    # check if there are any forms present
    collected_input = node.validate_input(flask.request.json)

    # get rabbit channel for process queue
    execution = xml.start(
        node,
        collected_input,
        mongo.db,
        flask.g.user.identifier,
    )

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'step',
        'pointer_id': execution.pointers.filter(
            Pointer.status == Pointer.Status.ONGOING,
        ).first().id,
        'user_identifier': flask.g.user.identifier,
        'input': collected_input,
    }))

    return {
        'data': execution.as_json(),
    }, 201
