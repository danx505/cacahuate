from json.decoder import JSONDecodeError

from cacahuate.database import db_session
from cacahuate.http.errors import (
    Forbidden,
)
from cacahuate.http.middleware import (
    pagination,
    requires_auth,
    requires_json,
)
from cacahuate.http.mongo import mongo
from cacahuate.models import (
    Execution,
    Pointer,
    User,
)
from cacahuate.mongo import json_prepare
from cacahuate.mongo import make_context
from cacahuate.xml import (
    Xml,
    form_to_dict,
    get_text,
)

import flask

import pymongo


# TODO: is this really required? maybe get doesn't need to support search
def format_query(q):
    try:
        formated_q = flask.json.loads(q)
    except JSONDecodeError:
        formated_q = q
    return formated_q


bp = flask.Blueprint('cacahuate', __name__)


@bp.route('/', methods=['GET', 'POST'])
@requires_json
def index():
    ''' This is here to provide a successful response for the / url and also to
    provide a test for the json middleware '''
    if flask.request.method == 'GET':
        return {
            'hello': 'world',
        }
    elif flask.request.method == 'POST':
        return flask.request.json


@bp.route('/activity', methods=['GET'])
@requires_auth
def list_activities():
    activities = flask.g.user.activities.filter(
        Execution.status == Execution.Status.ONGOING,
    )

    return flask.jsonify({
        'data': [
            a.as_json()
            for a in activities
        ],
    })


@bp.route('/task', methods=['GET'])
@requires_auth
def task_list():
    tasks = flask.g.user.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).all()

    return flask.jsonify({
        'data': [
            {
                **t.as_json(),
                'execution': t.execution.as_json(),
            }
            for t in tasks
        ],
    })


@bp.route('/task/<pk>', methods=['GET'])
@requires_auth
def task_read(pk):
    pointer = db_session.query(Pointer).filter(
        Pointer.id == pk,
        Pointer.status == Pointer.Status.ONGOING,
    ).first()

    if pointer is None:
        flask.abort(404)

    if pointer not in flask.g.user.assigned_tasks:
        raise Forbidden([{
            'detail': 'Provided user does not have this task assigned',
            'where': 'request.authorization',
        }])

    execution = pointer.execution
    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    state = collection.find_one({
        'id': execution.id,
    })

    xml = Xml.load(
        flask.current_app.config,
        execution.process_name,
        direct=True,
    )
    xmliter = iter(xml)
    node = xmliter.find(lambda e: e.getAttribute('id') == pointer.node_id)

    xmliter.parser.expandNode(node)

    # Response body
    json_data = {
        **pointer.as_json(),
        'execution': pointer.execution.as_json(),
    }

    # Append node info
    json_data['node_type'] = node.tagName

    context = make_context(state, {})

    # Append forms
    forms = []
    for form in node.getElementsByTagName('form'):
        forms.append(form_to_dict(form, context=context))
    json_data['form_array'] = forms

    # If any append previous work done
    node_state = state['state']['items'][pointer.node_id]
    node_actors = node_state['actors']

    user_identifier = flask.g.user.identifier
    if user_identifier in node_actors['items']:
        action = node_actors['items'][user_identifier]

        json_data['prev_work'] = action['forms']

    # Append validation
    if node.tagName == 'validation':
        deps = [
            get_text(dep)
            for dep in node.getElementsByTagName('dep')
        ]

        fields = []
        for dep in deps:
            form_ref, input_name = dep.split('.')

            # TODO this could be done in O(log N + K)
            for node in state['state']['items'].values():
                if node['state'] != 'valid':
                    continue

                for identifier in node['actors']['items']:
                    actor = node['actors']['items'][identifier]
                    if actor['state'] != 'valid':
                        continue

                    for form_ix, form in enumerate(actor['forms']):
                        if form['state'] != 'valid':
                            continue

                        if form['ref'] != form_ref:
                            continue

                        if input_name not in form['inputs']['items']:
                            continue

                        inpt = form['inputs']['items'][input_name]

                        state_ref = [
                            node['id'],
                            identifier,
                            str(form_ix),
                        ]
                        state_ref = '.'.join(state_ref)
                        state_ref = state_ref + ':' + dep

                        field = {
                            'ref': state_ref,
                            **inpt,
                        }
                        del field['state']

                        fields.append(field)

        json_data['fields'] = fields

    return flask.jsonify({
        'data': json_data,
    })


@bp.route('/log', methods=['GET'])
@requires_auth
@pagination
def all_logs():
    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]

    dict_args = flask.request.args.to_dict()

    query = {
        k: dict_args[k]
        for k in dict_args
        if k not in flask.current_app.config['INVALID_FILTERS']
    }

    # filter for user_identifier
    user_identifier = query.pop('user_identifier', None)
    if user_identifier is not None:
        user = db_session.query(User).filter(
            User.identifier == user_identifier,
        ).first()
        if user is not None:
            pointer_list = [item.id for item in user.assigned_tasks.all()]
        else:
            pointer_list = []
        query['id'] = {
            '$in': pointer_list,
        }

    pipeline = [
        {'$match': query},
        {'$sort': {'started_at': -1}},
        {'$group': {
            '_id': '$execution.id',
            'latest': {'$first': '$$ROOT'},
        }},
        {'$replaceRoot': {'newRoot': '$latest'}},
        {'$sort': {'started_at': -1}},
        {'$skip': flask.g.offset},
        {'$limit': flask.g.limit},
    ]

    return flask.jsonify({
        'data': list(map(
            json_prepare,
            collection.aggregate(pipeline),
        )),
    })


@bp.route('/log/<pk>', methods=['GET'])
@requires_auth
@pagination
def list_logs(pk):
    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]
    node_id = flask.request.args.get('node_id')
    query = {'execution.id': pk}

    if node_id:
        query['node.id'] = node_id

    cursor = collection.find(query)

    return flask.jsonify({
        "data": list(map(
            json_prepare,
            cursor.skip(flask.g.offset).limit(flask.g.limit).sort([
                ('started_at', pymongo.DESCENDING),
            ]),
        )),
    })
