import http
from json.decoder import JSONDecodeError

from cacahuate.database import db_session
from cacahuate.errors import (
    ElementNotFound,
)
from cacahuate.http.errors import (
    BadRequest,
    Forbidden,
)
from cacahuate.http.middleware import (
    pagination,
    requires_auth,
    requires_json,
)
from cacahuate.http.mongo import mongo
from cacahuate.http.validation import (
    validate_json,
)
from cacahuate.models import (
    Execution,
    Pointer,
    User,
)
from cacahuate.mongo import json_prepare
from cacahuate.mongo import make_context
from cacahuate.tasks import handle
from cacahuate.xml import (
    Xml,
)
from cacahuate.xml.node import make_node

import flask

import pymongo

import sqlalchemy.orm.exc


# TODO: is this really required? maybe get doesn't need to support search
def format_query(q):
    try:
        formated_q = flask.json.loads(q)
    except JSONDecodeError:
        formated_q = q
    return formated_q


bp = flask.Blueprint('cacahuate_pointer_legacy', __name__)


@bp.route('/pointer', methods=['POST'])
@requires_auth
@requires_json
def continue_process():
    validate_json(flask.request.json, ['execution_id', 'node_id'])

    execution_id = flask.request.json['execution_id']
    node_id = flask.request.json['node_id']

    execution = db_session.query(Execution).filter(
        Execution.id == execution_id,
        Execution.status == Execution.Status.ONGOING,
    ).first()
    if execution is None:
        raise BadRequest([{
            'detail': 'execution_id is not valid',
            'code': 'validation.invalid',
            'where': 'request.body.execution_id',
        }])

    xml = Xml.load(
        flask.current_app.config,
        execution.process_name,
        direct=True,
    )
    xmliter = iter(xml)

    collection = mongo.db[flask.current_app.config['EXECUTION_COLLECTION']]
    state = collection.find_one({
        'id': execution.id,
    }) or {}

    try:
        continue_point = make_node(
            xmliter.find(lambda e: e.getAttribute('id') == node_id),
            xmliter,
            context=make_context(state, {}),
        )
    except ElementNotFound:
        raise BadRequest([{
            'detail': 'node_id is not a valid node',
            'code': 'validation.invalid_node',
            'where': 'request.body.node_id',
        }])

    pointer = execution.pointers.filter(
        Pointer.node_id == node_id,
        Pointer.status == Pointer.Status.ONGOING,
    ).first()
    if pointer is None:
        raise BadRequest([{
            'detail': 'node_id does not have a live pointer',
            'code': 'validation.no_live_pointer',
            'where': 'request.body.node_id',
        }])

    # Check for authorization
    if pointer not in flask.g.user.assigned_tasks:
        raise Forbidden([{
            'detail': 'Provided user does not have this task assigned',
            'where': 'request.authorization',
        }])

    # Validate asociated forms
    collected_input = continue_point.validate_input(flask.request.json)

    # trigger rabbit
    handle.delay(flask.json.dumps({
        'command': 'step',
        'pointer_id': pointer.id,
        'user_identifier': flask.g.user.identifier,
        'input': collected_input,
    }))

    return {
        'data': 'accepted',
    }, 202


@bp.route('/pointer/<pointer_id>', methods=['GET'])
@requires_auth
def read_pointer(pointer_id):
    collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]

    try:
        ptr = next(collection.find({'id': pointer_id}))
    except StopIteration:
        raise sqlalchemy.orm.exc.NoResultFound(
            'Specified pointer never existed, and never will',
        )

    return flask.jsonify({
        'data': json_prepare(ptr),
    })


@bp.route('/pointer/search', methods=['POST'])
@requires_auth
@requires_json
@pagination
def pointer_list_search():
    dict_args = flask.request.get_json()

    # format query
    ptr_query = {
        k: v
        for k, v in dict_args.items()
        if k not in flask.current_app.config['INVALID_FILTERS']
    }

    # sort
    srt = {'started_at': -1}
    sort_query = ptr_query.pop('sort', None)
    if sort_query and sort_query.split(',', 1)[0]:
        try:
            key, order = sort_query.split(',', 1)
        except ValueError:
            key, order = sort_query, 'ASCENDING'

        if order not in ['ASCENDING', 'DESCENDING']:
            order = 'ASCENDING'

        order = getattr(pymongo, order)
        srt = {key: order}

    # filter for user_identifier
    user_identifier = ptr_query.pop('user_identifier', None)
    if user_identifier is not None:
        user = db_session.query(User).filter(
            User.identifier == user_identifier,
        ).first()
        if user is not None:
            pointer_list = [item.id for item in user.assigned_tasks.all()]
        else:
            pointer_list = []
        db_session.commit()
        ptr_query['id'] = {
            '$in': pointer_list,
        }

    # filter for exclude/include
    exclude_list = ptr_query.pop('exclude', None) or []
    exclude_map = {item: 0 for item in exclude_list}

    include_list = ptr_query.pop('include', None) or []
    include_map = {item: 1 for item in include_list}

    # store project for future use
    prjct = {**include_map} or {**exclude_map}

    ptr_collection = mongo.db[flask.current_app.config['POINTER_COLLECTION']]

    try:
        max_time_ms = (
            flask.current_app.config['MONGO_POINTER_COLLECTION_MAX_TIME_MS']
        )
        cursor_count = ptr_collection.count_documents(
            ptr_query,
            maxTimeMS=max_time_ms,
        )
        cursor = ptr_collection.find(
            ptr_query,
            prjct or None,
        ).sort(list(srt.items())).max_time_ms(
            max_time_ms,
        )

        return flask.make_response(
            flask.jsonify({
                'total_count': cursor_count,
                'items': list(map(
                    json_prepare,
                    cursor.skip(flask.g.offset).limit(flask.g.limit),
                )),
            }),
            http.HTTPStatus.OK,  # 200
        )
    except pymongo.errors.OperationFailure:
        flask.abort(400, 'Malformed query')
    except pymongo.errors.ExecutionTimeout:
        flask.abort(400, 'Cant handle requested query')
