import datetime

import flask


class JSONEncoder(flask.json.JSONEncoder):
    ''' extend json-encoder class'''

    def default(self, o):
        if isinstance(o, datetime.datetime):
            if o.tzinfo is None or o.tzinfo.utcoffset(o) is None:
                return o.isoformat() + 'Z'
            else:
                return o.isoformat()

        return flask.json.JSONEncoder.default(self, o)
