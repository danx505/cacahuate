import json
import logging
from datetime import datetime
from datetime import timezone

from cacahuate.http.json import JSONEncoder
from cacahuate.jsontypes import Map
from cacahuate.jsontypes import MultiFormDict

LOGGER = logging.getLogger(__name__)

DATE_FIELDS = [
    'started_at',
    'finished_at',
]


def make_context(execution_data, config):
    ''' the proper and only way to get the ``'values'`` key out of
    an execution document from mongo. It takes care of the transformations
    needed for it to work in jinja templates and other contexts where the
    multiplicity of answers (multiforms) is relevant. '''
    context = {}

    try:
        for key, value in execution_data['values'].items():
            context[key] = MultiFormDict(value)
    except KeyError:
        pass
    except AttributeError:
        LOGGER.warning('Corrupted \'values\': {execution_id}'.format(
            execution_id=execution_data.get('id'),
        ))

    context['_env'] = config.get('PROCESS_ENV') or {}

    return context


def json_prepare(obj):
    ''' Takes ``obj`` from a mongo collection and returns it *as is* with two
    minor changes:

    * ``_id`` key removed
    * objects of type ``datetime`` converted
    * to their string isoformat representation
    '''
    return json.loads(json.dumps(
        {
            k: v for k, v in obj.items()
            if k != '_id'
        },
        cls=JSONEncoder,
    ))


def execution_entry(execution, state):
    return {
        '_type': 'execution',
        'id': execution.id,
        'name': execution.name,
        'process_name': execution.process_name,
        'description': execution.description,
        'status': execution.status,
        'started_at': execution.started_at,
        'finished_at': None,
        'state': state,
        'values': {
            '_execution': [{
                'id': execution.id,
                'name': execution.name,
                'process_name': execution.process_name,
                'description': execution.description,
                'started_at': datetime.now(timezone.utc).isoformat(),
            }],
        },
        'actor_list': [],
    }


def pointer_entry(node, pointer, notified_users=None):
    execution = pointer.execution

    return {
        'id': pointer.id,
        'started_at': pointer.started_at,
        'finished_at': None,
        'execution': execution.as_json(),
        'node': {
            'id': node.id,
            'name': pointer.name,
            'description': pointer.description,
            'type': type(node).__name__.lower(),
        },
        'actors': Map([], key='identifier').to_json(),
        'actor_list': [],
        'process_id': execution.process_name,
        'notified_users': notified_users or [],
        'state': pointer.status,
    }
