''' This file defines some basic classes that map the behaviour of the
equivalent xml nodes '''
import itertools
import json
import logging

from cacahuate.cascade import cascade_invalidate
from cacahuate.cascade import track_next_node
from cacahuate.database.query import query_executions
from cacahuate.errors import EndOfProcess
from cacahuate.errors import InconsistentState
from cacahuate.errors import InputError
from cacahuate.errors import InvalidInputError
from cacahuate.errors import MisconfiguredProvider
from cacahuate.errors import RequiredDictError
from cacahuate.errors import RequiredInputError
from cacahuate.errors import RequiredListError
from cacahuate.errors import ValidationErrors
from cacahuate.grammar import Condition
from cacahuate.grammar import ConditionTransformer
from cacahuate.http.errors import BadRequest
from cacahuate.imports import user_import
from cacahuate.jsontypes import Map
from cacahuate.models import get_or_create_user
from cacahuate.mongo import make_context
from cacahuate.templates import render_or
from cacahuate.xml import NODES
from cacahuate.xml import Xml
from cacahuate.xml import get_text
from cacahuate.xml.forms import Form

from case_conversion import pascalcase

from jinja2 import Template
from jinja2 import TemplateError

from jsonpath_ng import parse as jsonpathparse

import requests

LOGGER = logging.getLogger(__name__)


class AuthParam:

    def __init__(self, element):
        self.name = element.getAttribute('name')
        self.value = get_text(element)
        self.type = element.getAttribute('type')


class Node:
    ''' An XML tag that represents an action or instruction for the virtual
    machine '''

    def __init__(self, element, xmliter, *args, **kwargs):
        for attrname, value in element.attributes.items():
            setattr(self, attrname, value)

    def is_async(self):
        raise NotImplementedError('Must be implemented in subclass')

    def validate_input(self, json_data):
        raise NotImplementedError('Must be implemented in subclass')

    def next(  # noqa
        self,
        xml,
        state,
        mongo,
        config,
        *,
        skip_reverse=False,
    ):
        # Return next node by simple adjacency
        xmliter = iter(xml)
        xmliter.find(lambda e: e.getAttribute('id') == self.id)

        return make_node(xmliter.next_skipping_elifelse(), xmliter)

    def dependent_refs(self, invalidated, node_state):
        raise NotImplementedError('Must be implemented in subclass')

    def in_state(self, ref, node_state):
        ''' returns true if this ref is part of this state '''
        n, user, form, field = ref.split('.')

        i, ref = form.split(':')

        try:
            forms = node_state['actors']['items'][user]['forms']
            forms[int(i)]['inputs']['items'][field]

            return True
        except IndexError:
            return False
        except KeyError:
            return False

    def get_invalidated_fields(self, invalidated, state):
        ''' debe devolver un conjunto de referencias a campos que deben ser
        invalidados, a partir de campos invalidados previamente '''
        node_state = state['state']['items'][self.id]

        if node_state['state'] == 'unfilled':
            return []

        found_refs = []

        for ref in invalidated:
            # for refs in this node's forms
            if self.in_state(ref, node_state):
                found_refs.append(ref)

        found_refs += self.dependent_refs(invalidated, node_state)

        return found_refs

    def get_state(self):
        return {
            '_type': 'node',
            'type': type(self).__name__.lower(),
            'id': self.id,
            'comment': '',
            'state': 'unfilled',
            'actors': Map([], key='identifier').to_json(),
            'name': self.name,
            'description': self.description,
            'milestone': hasattr(self, 'milestone'),
        }

    # Interpolate name
    def get_name(self, context={}):
        return render_or(self._name, self._name, context)

    def set_name(self, name):
        self._name = name

    name = property(get_name, set_name)

    def name_template(self):
        return self._name

    # Interpolate description
    def get_description(self, context={}):
        return render_or(self._description, self._description, context)

    def set_description(self, description):
        self._description = description

    description = property(get_description, set_description)

    def description_template(self):
        return self._description


class FullyContainedNode(Node):
    ''' this type of node can load all of the xml element to memory as oposed
    to nodes that contain blocks of nodes, thus not being able of loading
    themselves to memory from the begining '''

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        xmliter.parser.expandNode(element)


class UserAttachedNode(FullyContainedNode):
    ''' Types of nodes that require human interaction, thus being asyncronous
    and containing a common structure like auth-filter and node-info
    '''

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        # node info
        node_info = element.getElementsByTagName('node-info')

        name = ''
        description = ''

        if len(node_info) == 1:
            node_info = node_info[0]

            node_name = node_info.getElementsByTagName('name')
            name = get_text(node_name[0])

            node_description = node_info.getElementsByTagName('description')
            description = get_text(node_description[0])

        self.name = name
        self.description = description

        # Actor resolving
        self.auth_params = []
        self.auth_backend = None

        filter_q = element.getElementsByTagName('auth-filter')

        if len(filter_q) > 0:
            filter_node = filter_q[0]

            self.auth_backend = filter_node.getAttribute('backend')
            self.auth_params = [
                AuthParam(x)
                for x in filter_node.getElementsByTagName('param')
            ]

    def resolve_params(self, state, config):
        computed_params = {}

        context = make_context(state or {}, config)

        for param in self.auth_params:
            if state is not None and param.type == 'ref':
                element_ref, req = param.value.split('#')

                if element_ref.startswith('user'):
                    pieces = element_ref.split('.', 1)

                    user_key = 'identifier'
                    if len(pieces) == 2:
                        user_key = pieces[-1]

                    value = ','.join([
                        x[user_key]
                        for x in [
                            item['actor'] for item in state['actor_list']
                            if item['node'] == req
                        ]
                    ])
                    if not value:
                        LOGGER.warning(
                            'No user data found {}:{}'.format(
                                req,
                                element_ref,
                            ),
                        )

                elif element_ref == 'form':
                    try:
                        _form, _input = req.split('.')

                        value = context[_form][_input]
                    except ValueError:
                        LOGGER.warning(
                            'No form data found {}:{}'.format(
                                req,
                                element_ref,
                            ),
                        )
                        value = None
            elif param.type == 'list':
                separator = ','
                value = render_or(param.value, param.value, context).split(
                    separator,
                )
            else:
                value = render_or(param.value, param.value, context)

            computed_params[param.name] = value

        return computed_params

    def get_actors(self, config, state):
        hi_pro_cls = user_import(
            self.auth_backend,
            'HierarchyProvider',
            config['CUSTOM_HIERARCHY_PROVIDERS'],
            'cacahuate.auth.hierarchy',
            config['ENABLED_HIERARCHY_PROVIDERS'],
        )

        hierarchy_provider = hi_pro_cls(config)

        users = hierarchy_provider.find_users(
            **self.resolve_params(state, config),
        )

        def render_users(user):
            try:
                identifier, data = user
                if type(identifier) is not str:
                    raise ValueError
                if type(data) is not dict:
                    raise ValueError
            except ValueError:
                raise MisconfiguredProvider((
                    'User returned by hierarchy provider is not in the form '
                    '("identifier", "data"), got: {user}'.format(user=user)
                ))

            return get_or_create_user(identifier, data)

        return list(map(render_users, users))


class Action(UserAttachedNode):
    ''' A node from the process's graph. It is initialized from an Element
    '''

    def __init__(self, element, xmliter, context=None, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        # Form resolving
        self.form_array = []

        form_array = element.getElementsByTagName('form-array')

        if len(form_array) > 0:
            for form_el in form_array[0].getElementsByTagName('form'):
                self.form_array.append(Form(form_el, context=context))

    def is_async(self):
        return True

    def dependent_refs(self, invalidated, node_state):
        ''' finds dependencies of the invalidated set in this node '''
        refs = set()
        actor = next(iter(node_state['actors']['items'].keys()))

        for dep in invalidated:
            _, depref = dep.split(':')

            for form in self.form_array:
                for field in form.inputs:
                    for dep in field.dependencies:
                        if depref == dep:
                            refs.add('{node}.{actor}.0:{form}.{input}'.format(
                                node=self.id,
                                actor=actor,
                                form=form.ref,
                                input=field.name,
                            ))

        return refs

    def validate_form_spec(self, form_specs, associated_data) -> dict:
        ''' Validates the given data against the spec contained in form.
            In case of failure raises an exception. In case of success
            returns the validated data.
        '''

        collected_forms = []

        min_forms, max_forms = form_specs.multiple

        if len(associated_data) < min_forms:
            raise BadRequest([{
                'detail': 'form count lower than expected for ref {}'.format(
                    form_specs.ref,
                ),
                'where': 'request.body.form_array',
            }])

        if len(associated_data) > max_forms:
            raise BadRequest([{
                'detail': 'form count higher than expected for ref {}'.format(
                    form_specs.ref,
                ),
                'where': 'request.body.form_array',
            }])

        for index, form in associated_data:
            if type(form) is not dict:
                raise BadRequest([{
                    'detail': 'each form must be a dict',
                    'where': 'request.body.form_array.{}.data'.format(index),
                }])

            if 'data' not in form:
                raise BadRequest([{
                    'detail': 'form.data is required',
                    'code': 'validation.required',
                    'where': 'request.body.form_array.{}.data'.format(index),
                }])

            if type(form['data']) is not dict:
                raise BadRequest([{
                    'detail': 'form.data must be a dict',
                    'code': 'validation.invalid',
                    'where': 'request.body.form_array.{}.data'.format(index),
                }])

            collected_forms.append(form_specs.validate(
                index,
                form['data'],
            ))

        return collected_forms

    def validate_input(self, json_data):
        if 'form_array' in json_data and type(
            json_data['form_array'],
        ) is not list:
            raise BadRequest({
                'detail': 'form_array has wrong type',
                'where': 'request.body.form_array',
            })

        collected_forms = []
        errors = []
        index = 0
        forms = json_data.get('form_array', [])

        for form_specs in self.form_array:
            ref = form_specs.ref

            # Ignore unexpected forms
            while len(forms) > index and (forms[index].get('ref') != ref):
                index += 1

            # Collect expected forms
            collected = []
            while len(forms) > index and forms[index]['ref'] == ref:
                collected.append((index, forms[index]))
                index += 1

            try:
                for data in self.validate_form_spec(form_specs, collected):
                    collected_forms.append(data)
            except ValidationErrors as e:
                errors += e.errors

        if len(errors) > 0:
            raise BadRequest(ValidationErrors(errors).to_json())

        return collected_forms


class Validation(UserAttachedNode):

    VALID_RESPONSES = ('accept', 'reject')

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        # Dependency resolving
        self.dependencies = []

        deps_node = element.getElementsByTagName('dependencies')

        if len(deps_node) > 0:
            for dep_node in deps_node[0].getElementsByTagName('dep'):
                self.dependencies.append(get_text(dep_node))

    def is_async(self):
        return True

    def next(  # noqa
        self,
        xml,
        state,
        mongo,
        config,
        *,
        skip_reverse=False,
    ):
        context = make_context(state, config)

        if skip_reverse or context[self.id]['response'] == 'accept':
            return super().next(xml, state, mongo, config)

        state_updates = cascade_invalidate(
            xml,
            state,
            context[self.id]['inputs'],
            context[self.id]['comment'],
        )

        # update state
        collection = mongo[config['EXECUTION_COLLECTION']]
        collection.update_one({
            'id': state['id'],
        }, {
            '$set': state_updates,
        })

        # reload state
        state = next(collection.find({'id': state['id']}))

        return track_next_node(xml, state, mongo, config)

    def validate_field(self, field, index):
        if type(field) is not dict:
            raise RequiredDictError(
                'inputs.{}'.format(index),
                'request.body.inputs.{}'.format(index),
            )

        if 'ref' not in field:
            raise RequiredInputError(
                'inputs.{}.ref'.format(index),
                'request.body.inputs.{}.ref'.format(index),
            )

        try:
            node, actor, ref, inpt = field['ref'].split('.')
            index, ref = ref.split(':')
        except ValueError:
            raise InvalidInputError(
                'inputs.{}.ref'.format(index),
                'request.body.inputs.{}.ref'.format(index),
            )

        if not self.in_dependencies(field['ref']):
            raise InvalidInputError(
                'inputs.{}.ref'.format(index),
                'request.body.inputs.{}.ref'.format(index),
            )

    def in_dependencies(self, ref):
        node, user, form, field = ref.split('.')
        index, ref = form.split(':')
        fref = ref + '.' + field

        for dep in self.dependencies:
            if dep == fref:
                return True

        return False

    def validate_input(self, json_data):
        if 'response' not in json_data:
            raise RequiredInputError('response', 'request.body.response')

        if json_data['response'] not in self.VALID_RESPONSES:
            raise InvalidInputError('response', 'request.body.response')

        if json_data['response'] == 'reject':
            if 'inputs' not in json_data:
                raise RequiredInputError('inputs', 'request.body.inputs')

            if any([
                type(json_data['inputs']) is not list,
                len(json_data['inputs']) == 0,
            ]):
                raise RequiredListError('inputs', 'request.body.inputs')

            for index, field in enumerate(json_data['inputs']):
                errors = []
                try:
                    self.validate_field(field, index)
                except InputError as e:
                    errors.append(e.to_json())

                if errors:
                    raise BadRequest(errors)

            if 'comment' not in json_data:
                raise RequiredInputError('comment', 'request.body.comment')

            if type(json_data['comment']) is not str:
                raise BadRequest([{
                    'detail': '\'comment\' must be a str',
                    'code': 'validation.invalid',
                    'where': 'request.body.comment',
                }])

        return [Form.state_json(self.id, [
            {
                'name': 'response',
                'value': json_data['response'],
                'value_caption': json_data['response'],
            },
            {
                'name': 'comment',
                'value': json_data['comment'],
                'value_caption': json_data['comment'],
            },
            {
                'name': 'inputs',
                'value': json_data.get('inputs'),
                'value_caption': json.dumps(json_data.get('inputs')),
            },
        ])]

    def dependent_refs(self, invalidated, node_state):
        ''' finds dependencies of the invalidated set in this node '''
        refs = set()
        actor = next(iter(node_state['actors']['items'].keys()))

        for inref in invalidated:
            if self.in_dependencies(inref):
                refs.add('{node}.{actor}.0:approval.response'.format(
                    node=self.id,
                    actor=actor,
                ))

        return refs


class CallFormInput(Node):

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        self.value = get_text(element)
        self.type = element.getAttribute('type')

    def render(self, context, state):
        # TODO: dry from resolve_params
        if state is not None and self.type == 'ref':
            element_ref, req = self.value.split('#')

            if element_ref.startswith('user'):
                pieces = element_ref.split('.', 1)

                user_key = 'identifier'
                if len(pieces) == 2:
                    user_key = pieces[-1]

                value = ','.join([
                    x[user_key]
                    for x in [
                        item['actor'] for item in state['actor_list']
                        if item['node'] == req
                    ]
                ])
                if not value:
                    LOGGER.warning(
                        'No user data found {}:{}'.format(
                            req,
                            element_ref,
                        ),
                    )

            elif element_ref == 'form':
                try:
                    _form, _input = req.split('.')

                    value = context[_form][_input]
                except ValueError:
                    LOGGER.warning(
                        'No form data found {}:{}'.format(
                            req,
                            element_ref,
                        ),
                    )
                    value = None

            elif self.type == 'list':
                separator = ','
                value = render_or(self.value, self.value, context).split(
                    separator,
                )
        else:
            value = render_or(self.value, self.value, context)

        return value


class CallForm(Node):

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        self.inputs = []

        for input_el in element.getElementsByTagName('input'):
            self.inputs.append(CallFormInput(input_el, xmliter))

    def render(self, context, state):
        res = {
            'ref': self.ref,
            'data': {
            },
        }

        for inpt in self.inputs:
            res['data'][inpt.name] = inpt.render(context, state)

        return res


class Call(FullyContainedNode):
    ''' Calls a subprocess '''

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        self.name = 'Call ' + self.id
        self.description = 'Call ' + self.id

        self.procname = get_text(element.getElementsByTagName('procname')[0])

        self.forms = []

        data_el = element.getElementsByTagName('data')[0]

        for form_el in data_el.getElementsByTagName('form'):
            self.forms.append(CallForm(form_el, xmliter))

    def is_async(self):
        return False

    def work(self, config, state, mongo):
        xml = Xml.load(config, self.procname)

        xmliter = iter(xml)
        node = make_node(next(xmliter), xmliter)
        context = make_context(state, config)

        data = {
            'form_array': [f.render(context, state) for f in self.forms],
        }

        collected_input = node.validate_input(data)

        execution = xml.start(
            node,
            collected_input,
            mongo,
            '__system__',
        )

        # trigger rabbit
        from cacahuate.tasks import handle
        handle.delay(json.dumps({
            'command': 'step',
            'pointer_id': execution.pointers.first().id,
            'user_identifier': '__system__',
            'input': collected_input,
        }))

        return [Form.state_json(self.id, [
            {
                'name': 'execution',
                'state': 'valid',
                'type': 'text',
                'value': execution.id,
                'value_caption': str(execution.id),
            },
        ])]

    def dependent_refs(self, invalidated, node_state):
        return set()


def map_execution(execution, fields):
    mapped = {
        'id': {
            'type':  'text',
            'name': 'id',
            'label': 'Id',
            'state': 'valid',
            'value': str(execution.id),
            'value_caption': str(execution.id),
        },
        'name': {
            'type':  'text',
            'name': 'name',
            'label': 'Name',
            'state': 'valid',
            'value': str(execution.name),
            'value_caption': str(execution.name),
        },
        'description': {
            'type':  'text',
            'name': 'description',
            'label': 'Description',
            'state': 'valid',
            'value': str(execution.description),
            'value_caption': str(execution.description),
        },
        'process_name': {
            'type':  'text',
            'name': 'process_name',
            'label': 'Process Name',
            'state': 'valid',
            'value': str(execution.process_name),
            'value_caption': str(execution.process_name),
        },
        'status': {
            'type':  'text',
            'name': 'status',
            'label': 'Status',
            'state': 'valid',
            'value': str(execution.status),
            'value_caption': str(execution.status),
        },
        'started_at': {
            'type':  'text',
            'name': 'started_at',
            'label': 'Started At',
            'state': 'valid',
            'value': execution.started_at.isoformat(),
            'value_caption': execution.started_at.isoformat(),
        },
    }

    return [
        value for key, value in mapped.items()
        if key in fields
    ]


class Connection(FullyContainedNode):
    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        type_el = element.getElementsByTagName('connection-type')[0]
        conn_type = get_text(type_el)
        self.connection_type = conn_type

        action_el = element.getElementsByTagName('connection-action')[0]
        conn_action = get_text(action_el)
        self.connection_action = conn_action

        node_info_el = element.getElementsByTagName('node-info')[0]

        name_el = node_info_el.getElementsByTagName('name')
        self.name = get_text(name_el[0])

        description_el = node_info_el.getElementsByTagName('description')
        self.description = get_text(description_el[0])

        projection_el = element.getElementsByTagName('projection')[0]

        self.projection_alias = projection_el.getAttribute('alias')

        self.projection_fields = []
        for field_el in projection_el.getElementsByTagName('field'):
            self.projection_fields.append(
                field_el.getAttribute('id'),
            )

        if conn_type == 'execution' and conn_action == 'create':
            process_name_el = element.getElementsByTagName('process-name')[0]
            self.process_name = get_text(process_name_el)

            data_el = element.getElementsByTagName('data')[0]

            self.forms = []
            for form_el in data_el.getElementsByTagName('form'):
                self.forms.append(CallForm(form_el, xmliter))

        if conn_type == 'execution' and conn_action == 'update':
            data_el = element.getElementsByTagName('data')[0]

            self.patches = []
            for form_el in data_el.getElementsByTagName('form'):
                form_ref = form_el.getAttribute('ref')
                node_ref = form_el.getAttribute('node')

                for input_el in data_el.getElementsByTagName('input'):
                    input_ref = input_el.getAttribute('name')
                    val = get_text(input_el)

                    self.patches.append({
                        'form_ref': form_ref,
                        'node_ref': node_ref,
                        'input_ref': input_ref,
                        'value': val,
                    })

        if conn_type == 'execution' and conn_action in ['search', 'update']:
            search_el = element.getElementsByTagName('search-filter')[0]

            self.filters = []
            for param_el in search_el.getElementsByTagName('param'):
                self.filters.append({
                    'name': param_el.getAttribute('name'),
                    'lookup': param_el.getAttribute('lookup'),
                    'type': param_el.getAttribute('type'),
                    'value': get_text(param_el),
                })

    def is_async(self):
        return False

    def work(self, config, state, mongo):
        context = make_context(state or {}, config)

        conn_type = self.connection_type
        conn_action = self.connection_action

        if conn_type == 'execution' and conn_action == 'create':
            process_name = render_or(
                self.process_name,
                self.process_name,
                context,
            )
            xml = Xml.load(config, process_name)

            xmliter = iter(xml)
            node = make_node(next(xmliter), xmliter)

            data = {
                'form_array': [f.render(context, state) for f in self.forms],
            }

            collected_input = node.validate_input(data)

            execution = xml.start(
                node,
                collected_input,
                mongo,
                '__system__',
            )

            # trigger rabbit
            from cacahuate.tasks import handle
            handle.delay(json.dumps({
                'command': 'step',
                'pointer_id': execution.pointers.first().id,
                'user_identifier': '__system__',
                'input': collected_input,
            }))

            return [
                Form.state_json(
                    self.projection_alias,
                    map_execution(execution, self.projection_fields),
                ),
            ]

        if conn_type == 'execution' and conn_action == 'search':
            payload = {}
            for item in self.filters:
                k = '{name}__{lookup}'.format(
                    name=item['name'],
                    lookup=item['lookup'],
                )
                v = render_or(
                    item['value'],
                    item['value'],
                    context,
                )
                if item['type'] == 'list':
                    payload[k] = v.split(',')
                else:
                    payload[k] = v

            query = query_executions(**payload)

            return [
                Form.state_json(
                    self.projection_alias,
                    map_execution(execution, self.projection_fields),
                ) for execution in query
            ]

        if conn_type == 'execution' and conn_action == 'update':
            payload = {}
            for item in self.filters:
                k = '{name}__{lookup}'.format(
                    name=item['name'],
                    lookup=item['lookup'],
                )
                v = render_or(
                    item['value'],
                    item['value'],
                    context,
                )
                if item['type'] == 'list':
                    payload[k] = v.split(',')
                else:
                    payload[k] = v

            query = query_executions(**payload)

            updates = []
            for item in self.patches:
                updates.append({
                    'ref': '{node_ref}.{form_ref}.{input_ref}'.format(
                        node_ref=item['node_ref'],
                        form_ref=item['form_ref'],
                        input_ref=item['input_ref'],
                    ),
                    'value': render_or(
                        item['value'],
                        item['value'],
                        context,
                    ),
                })

            # trigger rabbit
            from cacahuate.tasks import handle
            from cacahuate.http.validation import validate_patch_inputs
            commands = []
            forms = []
            for execution in query:
                collection = mongo[config['EXECUTION_COLLECTION']]
                execution_state = next(collection.find({'id': execution.id}))

                xml = Xml.load(
                    config,
                    execution.process_name,
                    direct=True,
                )

                try:
                    processed_inputs = validate_patch_inputs(
                        updates,
                        execution_state,
                        xml,
                        False,
                    )
                except Exception:
                    continue

                commands.append({
                    'command': 'patch',
                    'execution_id': execution.id,
                    'comment': 'System update',
                    'inputs': processed_inputs,
                    'user_identifier': '__system__',
                })

                forms.append(
                    Form.state_json(
                        self.projection_alias,
                        map_execution(execution, self.projection_fields),
                    ),
                )

            for command in commands:
                handle.delay(json.dumps(command))

            return forms

    def dependent_refs(self, invalidated, node_state):
        return set()


class Exit(FullyContainedNode):
    ''' A node that kills an execution with some status '''

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        self.name = 'Exit ' + self.id
        self.description = 'Exit ' + self.id

    def is_async(self):
        return False

    def next(  # noqa
        self,
        xml,
        state,
        mongo,
        config,
        *,
        skip_reverse=False,
    ):
        raise EndOfProcess

    def work(self, config, state, mongo):
        return []


class Conditional(Node):

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        raw_type = kwargs.get('type', 'IF')

        self.name = raw_type + ' ' + self.id
        self.description = raw_type + ' ' + self.id

        self.condition = xmliter.get_next_condition()

    def is_async(self):
        return False

    def next(  # noqa
        self,
        xml,
        state,
        mongo,
        config,
        *,
        skip_reverse=False,
    ):
        xmliter = iter(xml)

        # consume up to this node
        current_node = xmliter.find(lambda e: e.getAttribute('id') == self.id)

        if not make_context(state, config)[self.id]['condition']:
            xmliter.expand(current_node)

        return make_node(xmliter.next_skipping_elifelse(), xmliter)

    def work(self, config, state, mongo):
        tree = Condition().parse(self.condition)

        try:
            value = ConditionTransformer(
                make_context(state, config),
            ).transform(tree)
        except ValueError as e:
            raise InconsistentState(
                'Could not evaluate condition: {err}'.format(
                    err=str(e),
                ),
            )

        return [Form.state_json(self.id, [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': value,
                'value_caption': str(value),
            },
        ])]

    def dependent_refs(self, invalidated, node_state):
        ''' IF nodes should alwas be invalidated in case the value they depend
        on changes, so this function just returns this node's ref'''
        actor = next(iter(node_state['actors']['items'].keys()))

        return {'{node}.{actor}.0:approval.condition'.format(
            node=self.id,
            actor=actor,
        )}


class If(Conditional):

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, 'IF', *args, **kwargs)


class Elif(Conditional):

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, 'ELIF', *args, **kwargs)


class Else(Node):

    def is_async(self):
        return False

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        self.name = 'ELSE ' + self.id
        self.description = 'ELSE ' + self.id

    def work(self, config, state, mongo):
        return [Form.state_json(self.id, [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])]

    def dependent_refs(self, invalidated, node_state):
        ''' ELSE nodes should alwas be invalidated
        in case the value they depend on changes,
        so this function just returns this node's ref'''
        actor = next(iter(node_state['actors']['items'].keys()))

        return {'{node}.{actor}.0:approval.condition'.format(
            node=self.id,
            actor=actor,
        )}


class CaptureValue:

    def __init__(self, element):
        self.path = element.getAttribute('path')
        self.name = element.getAttribute('name')
        self.label = element.getAttribute('label')
        self.type = element.getAttribute('type')

    def capture(self, data, parentpath):
        if parentpath:
            path = '{}.{}'.format(parentpath, self.path)
        else:
            path = self.path

        try:
            value = jsonpathparse(path).find(data)[0].value
        except IndexError:
            raise ValueError('Could not match value')

        if self.type == 'int':
            value = int(value)
        elif self.type == 'float':
            value = float(value)

        return {
            'name': self.name,
            'value': value,
            'type': self.type,
            'label': self.label,
            'value_caption': str(value),
        }


class Capture:

    def __init__(self, element):
        self.id = element.getAttribute('id')
        self.multiple = bool(element.getAttribute('multiple'))
        self.path = element.getAttribute('path')

        self.values = [
            CaptureValue(value)
            for value in element.getElementsByTagName('value')
        ]

    def capture(self, data):
        if self.multiple:
            return self.capture_multiple(data)

        return [{
            'id': self.id,
            'items': [
                value.capture(data, self.path) for value in self.values
            ],
        }]

    def capture_multiple(self, data):
        match = jsonpathparse(self.path).find(data)
        if len(match) == 0:
            raise ValueError('Did not find a match with that path')

        items = list(itertools.chain(*[item.value for item in match]))
        return [
            {
                'id': self.id,
                'items': [
                    value.capture(localdata, None) for value in self.values
                ],
            }
            for localdata in items
        ]

    def __str__(self):
        return '<Capture id="{}" path="{}">'.format(self.id, self.path)


class Request(FullyContainedNode):
    ''' A node that makes a TCP Request '''

    def __init__(self, element, xmliter, *args, **kwargs):
        super().__init__(element, xmliter, *args, **kwargs)

        self.name = 'Request ' + self.id
        self.description = 'Request ' + self.id
        self.url = get_text(element.getElementsByTagName('url')[0])

        # Body and headers
        try:
            self.body = get_text(element.getElementsByTagName('body')[0])
        except IndexError:
            self.body = ''

        self.headers = []

        for header in element.getElementsByTagName('header'):
            self.headers.append(
                (header.getAttribute('name'), get_text(header)),
            )

        # Captures
        try:
            self.capture_type = element.getElementsByTagName(
                'captures',
            )[0].getAttribute('type')
        except IndexError:
            self.capture_type = None  # Indicates no capture

        self.captures = [
            Capture(capture)
            for capture in element.getElementsByTagName('capture')
        ]

        # Dependency resolving
        self.dependencies = []

        deps_node = element.getElementsByTagName('dependencies')

        if len(deps_node) > 0:
            for dep_node in deps_node[0].getElementsByTagName('dep'):
                self.dependencies.append(get_text(dep_node))

    def make_request(self, context):
        data_forms = []

        try:
            url = Template(self.url).render(**context)
            body = Template(self.body).render(**context)
            headers = {
                t[0]: Template(t[1]).render(**context)
                for t in self.headers
            }

            response = requests.request(
                self.method,
                url,
                headers=headers,
                data=body,
            )

            data_forms.append({
                'id': self.id,
                'items': [
                    {
                        'name': 'status_code',
                        'value': response.status_code,
                        'type': 'int',
                        'label': 'Status Code',
                        'value_caption': str(response.status_code),
                    },
                    {
                        'name': 'raw_response',
                        'value': response.text,
                        'type': 'text',
                        'label': 'Response',
                        'value_caption': response.text,
                    },
                ],
            })

            # Capture request data if specified
            if self.capture_type is not None:
                if self.capture_type == 'json':
                    data = response.json()
                else:
                    raise NotImplementedError('Only json captures joven')

                for capture in self.captures:
                    for form in capture.capture(data):
                        data_forms.append(form)
        except TemplateError:
            data_forms.append({
                'id': self.id,
                'items': [
                    {
                        'name': 'status_code',
                        'value': 0,
                        'type': 'int',
                        'label': 'Status Code',
                        'value_caption': '0',
                    },
                    {
                        'name': 'raw_response',
                        'value': 'Jinja error prevented this request',
                        'type': 'text',
                        'label': 'Response',
                        'value_caption': 'Jinja error prevented this request',
                    },
                ],
            })
        except requests.exceptions.ConnectionError as e:
            data_forms.append({
                'id': self.id,
                'items': [
                    {
                        'name': 'status_code',
                        'value': 0,
                        'type': 'int',
                        'label': 'Status Code',
                        'value_caption': '0',
                    },
                    {
                        'name': 'raw_response',
                        'value': str(e),
                        'type': 'text',
                        'label': 'Response',
                        'value_caption': str(e),
                    },
                ],
            })
        except requests.exceptions.JSONDecodeError as e:
            raise ValueError(
                f'Request to "{url}" failed to return JSON object: {str(e)}',
            )

        return data_forms

    def work(self, config, state, mongo):
        data_forms = self.make_request(make_context(state, config))

        return [
            Form.state_json(data_form['id'], [
                {
                    'name': item['name'],
                    'state': 'valid',
                    'type': item['type'],
                    'value': item['value'],
                    'label': item['label'],
                    'value_caption': item['value_caption'],
                    'hidden': False,
                }
                for item in data_form['items']
            ])
            for data_form in data_forms
        ]

    def is_async(self):
        return False

    def in_dependencies(self, ref):
        node, user, form, field = ref.split('.')
        index, ref = form.split(':')
        fref = ref + '.' + field

        for dep in self.dependencies:
            if dep == fref:
                return True

        return False

    def dependent_refs(self, invalidated, node_state):
        ''' finds dependencies of the invalidated set in this node '''
        refs = set()
        actor = next(iter(node_state['actors']['items'].keys()))

        for inref in invalidated:
            if self.in_dependencies(inref):
                refs.add('{node}.{actor}.0:{node}.status_code'.format(
                    node=self.id,
                    actor=actor,
                ))

        return refs


def make_node(element, xmliter, context=None) -> Node:
    if not context:
        context = {}

    ''' returns a build Node object given an Element object '''
    if element.tagName not in NODES:
        raise ValueError(
            'Class definition not found for node: {}'.format(element.tagName),
        )

    class_name = pascalcase(element.tagName)
    available_classes = __import__(__name__).xml.node

    return getattr(available_classes, class_name)(element, xmliter, context)
