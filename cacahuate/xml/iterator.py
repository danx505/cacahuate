from collections import deque
from typing import Callable
from xml.dom import pulldom
from xml.dom.minidom import Element
from xml.sax._exceptions import SAXParseException

from cacahuate.errors import ElementNotFound
from cacahuate.errors import MalformedProcess
from cacahuate.xml.utils import get_text


class XmlIter:
    def __init__(self, file_path, iterables):
        self.parser = pulldom.parse(open(file_path))
        self.block_stack = deque()
        self.iterables = iterables
        self._current_node = (None, None)
        self._scopes = ['root']

    def find(self, testfunc: Callable[[Element], bool]) -> Element:
        ''' Given an interator returned by the previous function, tries
        to find the first node matching the given condition '''
        for element in self:
            if testfunc(element):
                return element

        raise ElementNotFound(
            'node matching the given condition was not found',
        )

    def get_next_condition(self):
        for event, node in self.parser:
            if event != pulldom.START_ELEMENT:
                continue

            if node.tagName != 'condition':
                raise ElementNotFound(
                    'Requested a condition but found {}'.format(
                        node.tagName,
                    ),
                )

            self.parser.expandNode(node)

            return get_text(node)

        raise ElementNotFound('Condition not found')

    def next_skipping_elifelse(self):
        old_stack = len(self.block_stack)
        proposed_next = next(self)
        new_stack = len(self.block_stack)

        if new_stack < old_stack:
            while proposed_next.tagName in ('elif', 'else'):
                self.parser.expandNode(proposed_next)
                proposed_next = next(self)

        return proposed_next

    def expand(self, node):
        self.parser.expandNode(node)

    @property
    def scope(self):
        if self._current_node[0] in ['if', 'elif', 'else']:
            return (
                self._scopes[-2],
                len(self._scopes) - 2,
            )

        return (
            self._scopes[-1],
            len(self._scopes) - 1,
        )

    def __next__(self):
        try:
            for event, node in self.parser:
                is_block = getattr(node, 'tagName', None) == 'block'
                is_iterable = getattr(node, 'tagName', None) in self.iterables

                if event == pulldom.END_ELEMENT:
                    if getattr(node, 'tagName', None) in [
                        'if',
                        'elif',
                        'else',
                    ]:
                        self._scopes.pop()

                    if is_block:
                        self.block_stack.pop()

                if event == pulldom.START_ELEMENT:
                    if is_iterable:
                        self._current_node = (
                            getattr(node, 'tagName', None),
                            node.getAttribute('id'),
                        )

                    if getattr(node, 'tagName', None) in [
                        'if',
                        'elif',
                        'else',
                    ]:
                        self._scopes.append(node.getAttribute('id'))

                    if is_block:
                        self.block_stack.append(1)
                    elif is_iterable:
                        return node
        except SAXParseException:
            raise MalformedProcess

        raise StopIteration

    def __iter__(self):
        return self
