from cacahuate.grammar import Condition
from cacahuate.grammar import ConditionTransformer

import lark.exceptions


def get_text(node):
    node.normalize()

    if node.firstChild is not None:
        return node.firstChild.nodeValue or ''

    return ''


def evaluate_required_tag(element, context):
    required_tags = element.getElementsByTagName('required')

    if len(required_tags) > 0:
        raw_condition = get_text(required_tags[0])
        tree = Condition().parse(raw_condition)

        try:
            return ConditionTransformer(
                context,
            ).transform(tree)
        except lark.exceptions.VisitError:
            return True

    return element.getAttribute('required') == 'required'
