import importlib
import os
from typing import TextIO
from xml.dom import minidom

from cacahuate.database import db_session
from cacahuate.errors import MalformedProcess
from cacahuate.errors import ProcessNotFound
from cacahuate.jsontypes import SortedMap
from cacahuate.models import (
    Category,
    Execution,
    Pointer,
    Quirk,
)
from cacahuate.mongo import (
    execution_entry,
    pointer_entry,
)
from cacahuate.templates import render_or
from cacahuate.xml.iterator import XmlIter
from cacahuate.xml.utils import (
    evaluate_required_tag,
    get_text,
)

from jsonpath_ng import parse as jsonpathparse


XML_ATTRIBUTES = {
    'public': lambda a: a == 'true',
    'author': str,
    'date': str,
    'name': str,
    'description': lambda x: x,
}

NODES = (
    'action',
    'validation',
    'exit',
    'if',
    'elif',
    'else',
    'request',
    'call',
    'connection',
)


class Xml:

    def __init__(self, config, filename):
        try:
            self.id, self.version, _ = filename.split('.')
        except ValueError:
            raise MalformedProcess(
                'Name of process is invalid, must be name.version.xml',
            )

        self.versions = [self.version]
        self.filename = filename
        self.config = config

        try:
            info_node = self.get_info_node()
        except StopIteration:
            raise MalformedProcess('This process lacks the process-info node')

        if info_node.tagName != 'process-info':
            raise MalformedProcess('process-info node must be the first node')

        for attr, func in XML_ATTRIBUTES.items():
            try:
                node = info_node.getElementsByTagName(attr)[0]
            except IndexError:
                raise MalformedProcess(
                    'Process\' metadata lacks node {}'.format(attr),
                )

            setattr(self, attr, func(get_text(node)))

    def get_file_path(self):
        return os.path.join(self.config['XML_PATH'], self.filename)

    def get_dom(self):
        return minidom.parse(self.get_file_path())

    # Interpolate name
    def get_name(self, context={}):
        return render_or(self._name, self.filename, context)

    def set_name(self, name):
        self._name = name

    name = property(get_name, set_name)

    def name_template(self):
        return self._name

    # Interpolate description
    def get_description(self, context={}):
        return render_or(self._description, self._description, context)

    def set_description(self, description):
        self._description = description

    description = property(get_description, set_description)

    def description_template(self):
        return self._description

    @classmethod
    def load(cls, config: dict, common_name: str, direct=False) -> TextIO:
        ''' Loads an xml file and returns the corresponding TextIOWrapper for
        further usage. The file might contain multiple versions so the latest
        one is chosen.

        common_name is the prefix of the file to find. If multiple files with
        the same prefix are found the last in lexicographical order is
        returned.'''
        if direct:
            # skip looking for the most recent version
            return Xml(config, common_name)

        pieces = common_name.split('.')

        try:
            name = pieces[0]
            version = pieces[1]
        except IndexError:
            name, version = common_name, None

        files = sorted(os.listdir(config['XML_PATH']), reverse=True)

        for filename in files:
            fpieces = filename.split('.')

            try:
                fname = fpieces[0]
                fversion = fpieces[1]
            except IndexError:
                # Process with malformed name, sorry
                continue

            if fname == name:
                if version:
                    if fversion == version:
                        return Xml(config, filename)
                else:
                    return Xml(config, filename)

        else:
            raise ProcessNotFound(common_name)

    def start(self, node, collected_input, mongo, user_identifier):
        # the first set of values
        context = {}
        for form in collected_input:
            form_dict = {}

            for name, inpt in form['inputs']['items'].items():
                form_dict[name] = inpt['value_caption']

            context[form['ref']] = form_dict

        # save the data
        execution = Execution(
            process_name=self.filename,
            name=self.get_name(context),
            name_template=self.name_template(),
            description=self.get_description(context),
            description_template=self.description_template(),
            status=Execution.Status.ONGOING,
            id=self.generate_execution_id(),
        )

        pointer = Pointer(
            node_id=node.id,
            node_type=type(node).__name__.lower(),
            name=node.get_name(context),
            description=node.get_description(context),
            execution=execution,
            status=Pointer.Status.ONGOING,
            id=self.generate_pointer_id(),
        )

        db_session.add(execution)
        db_session.add(pointer)
        db_session.commit()

        # log to mongo
        collection = mongo[self.config['POINTER_COLLECTION']]
        collection.insert_one(pointer_entry(
            node,
            pointer,
        ))

        collection = mongo[self.config['EXECUTION_COLLECTION']]
        collection.insert_one(execution_entry(
            execution,
            self.get_state(),
        ))

        return execution

    def make_iterator(self, iterables):
        return XmlIter(self.get_file_path(), iterables)

    def get_info_node(self):
        xmliter = self.make_iterator(['process-info'])
        info_node = next(xmliter)
        xmliter.parser.expandNode(info_node)

        return info_node

    def __iter__(self):
        ''' Returns an inerator over the nodes and edges of a process defined
        by the xmlfile descriptor. Uses XMLPullParser so no memory is consumed
        for this task. '''
        return self.make_iterator(NODES)

    def get_node(self, node_id):
        from cacahuate.xml.node import make_node  # noqa
        xmliter = iter(self)

        return make_node(
            xmliter.find(lambda e: e.getAttribute('id') == node_id),
            xmliter,
        )

    def get_state(self):
        from cacahuate.xml.node import make_node  # noqa

        xmliter = iter(self)
        items = []

        for node in xmliter:
            built_node = make_node(node, xmliter)

            items.append(built_node.get_state())

        return SortedMap(items, key='id').to_json()

    @classmethod
    def processes(cls, config):
        # Get all processes
        files = sorted(os.listdir(config['XML_PATH']), reverse=True)

        # Load only the oldest processes
        processes = []

        for filename in files:
            try:
                pk, version, _ = filename.split('.')
            except ValueError:
                continue

            try:
                xml = cls.load(config, filename, direct=True)
            except ProcessNotFound:
                continue
            except MalformedProcess:
                continue

            if not xml.public:
                continue

            if len(processes) == 0 or processes[-1].id != pk:
                processes.append(xml)
            else:
                processes[-1].versions.append(version)

        return processes

    def to_json(self):
        return {
            'id': self.id,
            'version': self.version,
            'author': self.author,
            'date': self.date,
            'name': self.name,
            'description': self.description,
            'versions': self.versions,
        }

    def _generate_id(self):
        func = getattr(
            importlib.import_module(
                self.config['DB_ID_FUNCTION'].rsplit('.', 1)[0],
            ),
            self.config['DB_ID_FUNCTION'].rsplit('.', 1)[1],
        )

        return func()

    def generate_execution_id(self):
        new_id = self._generate_id()

        while db_session.query(Execution).filter(
            Execution.id == new_id,
        ).count() != 0:
            new_id = self._generate_id()

        return new_id

    def generate_pointer_id(self):
        new_id = self._generate_id()

        while db_session.query(Pointer).filter(
            Pointer.id == new_id,
        ).count() != 0:
            new_id = self._generate_id()

        return new_id


def get_node_info(node):
    # Get node-info
    node_info = node.getElementsByTagName('node-info')
    name = None
    description = None

    if len(node_info) == 1:
        node_info = node_info[0]

        node_name = node_info.getElementsByTagName('name')
        name = get_text(node_name[0])

        node_description = node_info.getElementsByTagName('description')
        description = get_text(node_description[0])

    return {
        'name': name,
        'description': description,
    }


SUPPORTED_ATTRS = {
    'default': str,
    'helper': str,
    'label': str,
    'name': str,
    'placeholder': str,
    'provider': str,
    'regex': str,
    'type': str,
}


def extract_option_filters(element):
    filters = []

    for fltr in element.getElementsByTagName('filter'):
        filters.append({
            'name': fltr.getAttribute('name'),
            'value': get_text(fltr),
        })

    return filters


def parse_input(inpt, context):
    options = []
    for opt in inpt.getElementsByTagName('option'):
        ref_attr = opt.getAttribute('ref')
        if ref_attr:
            ref_type, ref_val = ref_attr.split('#')
            if ref_type == 'form':
                if not len(jsonpathparse(ref_val).find(context)):
                    continue

                label_path = opt.getAttribute('label')
                value_path = opt.getAttribute('value')

                match = jsonpathparse(ref_val).find(context)[0].value.all()
                for item in match:
                    options.append({
                        'value': jsonpathparse(value_path).find(item)[0].value,
                        'label': jsonpathparse(label_path).find(item)[0].value,
                    })

            elif ref_type == 'model':
                cursor = []

                label_path = opt.getAttribute('label')
                value_path = opt.getAttribute('value')

                # if model is quirk
                if ref_val == 'quirk':
                    cursor = db_session.query(Quirk).join(Category)

                    filters = extract_option_filters(opt)

                    for fltr in filters:
                        if fltr['name'] == 'category__codename':
                            cursor = cursor.filter(
                                Category.codename == fltr['value'],
                            )

                # get some values
                label_path = opt.getAttribute('label')

                # actual search
                for item in cursor:
                    options.append({
                        'value': getattr(item, value_path),
                        'label': getattr(item, label_path),
                    })

        else:
            options.append({
                'value': opt.getAttribute('value'),
                'label': opt.getAttribute('label') or get_text(opt),
            })

    return sorted(
        options,
        key=lambda x: x['label'],
    )


def input_to_dict(inpt, context=None):
    if not context:
        context = {}

    input_attrs = [
        (attr, func(inpt.getAttribute(attr)))
        for attr, func in SUPPORTED_ATTRS.items()
    ]

    options = parse_input(inpt, context)
    input_attrs.append(('options', options))

    required = evaluate_required_tag(inpt, context)
    input_attrs.append(('required', required))

    return dict(filter(
        lambda a: a[1],
        input_attrs,
    ))


def form_to_dict(form, context=None):
    if not context:
        context = {}

    inputs = form.getElementsByTagName('input')

    form_dict = {
        'ref': form.getAttribute('id'),
        'inputs': [],
    }

    if form.getAttribute('multiple'):
        form_dict['multiple'] = form.getAttribute('multiple')

    for inpt in inputs:
        form_dict['inputs'].append(input_to_dict(inpt, context=context))

    return form_dict


def get_element_by(dom, tag_name, attr, value):
    for el in dom.getElementsByTagName(tag_name):
        if el.getAttribute(attr) == value:
            return el
