import collections
import re
import xml.dom.pulldom
from typing import Any, TypeVar

from cacahuate.xml.utils import get_text


INPUT_TYPE_MAP = {
    'text': 'text',
    'datetime': 'datetime',
    'password': 'text',
    'checkbox': 'text',
    'radio': 'text',
    'select': 'text',
    'file': 'file',
    'date': 'date',
    'int': 'integer',
    'float': 'float',
    'link': 'text',
    'currency': 'float',
}

INPUT_WIDGET_MAP = {
    'text': 'text',
    'datetime': 'datetime',
    'password': 'password',
    'checkbox': 'checkboxselectmultiple',
    'radio': 'radioselect',
    'select': 'select',
    'file': 'file',
    'date': 'date',
    'int': 'number',
    'float': 'number',
    'link': 'url',
    'currency': 'number',
}


KeyType = TypeVar('KeyType')


def deep_update(
    mapping: dict[KeyType, Any],
    *updating_mappings: dict[KeyType, Any],
) -> dict[KeyType, Any]:
    updated_mapping = mapping.copy()
    for updating_mapping in updating_mappings:
        for k, v in updating_mapping.items():
            if k in updated_mapping and isinstance(
                updated_mapping[k],
                dict,
            ) and isinstance(v, dict):
                updated_mapping[k] = deep_update(updated_mapping[k], v)
            else:
                updated_mapping[k] = v
    return updated_mapping


def clean_multiple_attr(attr):
    rng = (1, 1)

    if attr:
        nums = re.compile(r'\d+').findall(attr)
        nums = [int(x) for x in nums]
        if len(nums) == 1:
            rng = (nums[0], nums[0])
        elif len(nums) == 2:
            rng = (nums[0], nums[1])
        else:
            rng = (0, float('inf'))

    return rng


def parse_dependencies(node, parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'dependencies':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'dep':
            parser.expandNode(node)
            items.append(get_text(node))

    return items


def parse_option(node, parser):
    ref = node.getAttribute('ref')
    if ref:
        data = {
            'ref': ref,
            'value': node.getAttribute('value'),
            'label': node.getAttribute('label'),
            'filters': [],
        }

        for subnode in node.getElementsByTagName('filter'):
            data['filters'].append({
                'name': subnode.getAttribute('name'),
                'value': get_text(subnode),
            })

        return data

    else:
        data = {
            'value': node.getAttribute('value'),
            'label': node.getAttribute('label'),
        }

        if not data['label']:
            parser.expandNode(node)
            data['label'] = get_text(node)

        return data


def parse_options(node, parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'options':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'option':
            items.append(
                parse_option(node, parser),
            )

    return items


def parse_input(node, parser):
    data = {
        'id': node.getAttribute('name'),
        'type': node.getAttribute('type'),
        'label': node.getAttribute('label'),
        'dependencies': [],
    }

    if node.getAttribute('required') == 'required':
        data['required'] = 'TRUE'
    else:
        data['required'] = 'FALSE'

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'input':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'dependencies':
            data['dependencies'] = parse_dependencies(node, parser)

        if node_tag == 'options':
            data['options'] = parse_options(node, parser)

        if node_tag == 'required':
            parser.expandNode(node)
            data['required'] = get_text(node)

    return data


def parse_form(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'inputs': [],
    }

    (
        data['min_amount'],
        data['max_amount'],
    ) = clean_multiple_attr(node.getAttribute('multiple'))

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'form':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'input':
            data['inputs'].append(
                parse_input(node, parser),
            )

    return data


def parse_form_array(parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'form-array':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'form':
            items.append(
                parse_form(node, parser),
            )

    return items


def parse_action(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'action',
        'milestone': node.getAttribute('milestone') == 'true',
        'forms': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'action':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'name':
            parser.expandNode(node)
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)
            data['description'] = get_text(node)

        if node_tag == 'form-array':
            data['forms'] = parse_form_array(parser)

    return data


def parse_validation(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'validation',
        'milestone': node.getAttribute('milestone') == 'true',
        'dependencies': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'validation':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'name':
            parser.expandNode(node)
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)
            data['description'] = get_text(node)

        if node_tag == 'dependencies':
            data['dependencies'] = parse_dependencies(node, parser)

    return data


def parse_if(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'if',
        'milestone': node.getAttribute('milestone') == 'true',
        'condition': None,
        'children': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'if':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'condition':
            parser.expandNode(node)
            data['condition'] = get_text(node)

        if node_tag == 'block':
            data['children'] = parse_block(parser)

    return data


def parse_elif(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'elif',
        'milestone': node.getAttribute('milestone') == 'true',
        'condition': None,
        'children': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'elif':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'condition':
            parser.expandNode(node)
            data['condition'] = get_text(node)

        if node_tag == 'block':
            data['children'] = parse_block(parser)

    return data


def parse_else(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'else',
        'milestone': node.getAttribute('milestone') == 'true',
        'children': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'else':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'block':
            data['children'] = parse_block(parser)

    return data


def parse_request(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'request',
        'milestone': node.getAttribute('milestone') == 'true',
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'request':
            break

    return data


def parse_call(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'call',
        'milestone': node.getAttribute('milestone') == 'true',
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'call':
            break

    return data


def parse_connection(node, parser):
    data = {
        'id': node.getAttribute('id'),
        'type': 'connection',
        'milestone': node.getAttribute('milestone') == 'true',
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'connection':
            break

        if node_tag == 'name':
            parser.expandNode(node)
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)
            data['description'] = get_text(node)

    return data


def parse_block(parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'block':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        parser_map = {
            'action': parse_action,
            'validation': parse_validation,
            'if': parse_if,
            'elif': parse_elif,
            'else': parse_else,
            'request': parse_request,
            'call': parse_call,
            'connection': parse_connection,
        }

        if node_tag in parser_map:
            items.append(
                parser_map[node_tag](node, parser),
            )

    return items


def parse_process(parser):
    items = []

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'process':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        parser_map = {
            'action': parse_action,
            'validation': parse_validation,
            'if': parse_if,
            'elif': parse_elif,
            'else': parse_else,
            'request': parse_request,
            'call': parse_call,
            'connection': parse_connection,
        }

        if node_tag in parser_map:
            items.append(
                parser_map[node_tag](node, parser),
            )

    return items


def parse_process_info(node, parser):
    data = {
        'author': None,
        'date': None,
        'name': None,
        'public': False,
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'process-info':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'author':
            parser.expandNode(node)
            data['author'] = get_text(node)

        if node_tag == 'date':
            parser.expandNode(node)
            data['date'] = get_text(node)

        if node_tag == 'public':
            parser.expandNode(node)
            data['public'] = get_text(node) == 'true'

        if node_tag == 'name':
            parser.expandNode(node)
            data['name'] = get_text(node)

        if node_tag == 'description':
            parser.expandNode(node)
            data['description'] = get_text(node)

    return data


def parse_xml(parser: xml.dom.pulldom.DOMEventStream):
    data = {
        'process-info': None,
        'process': [],
    }

    for event, node in parser:
        node_tag = getattr(node, 'tagName', None)
        if event == xml.dom.pulldom.END_ELEMENT and node_tag == 'process-spec':
            break

        if event != xml.dom.pulldom.START_ELEMENT:
            continue

        if node_tag == 'process-info':
            data['process-info'] = parse_process_info(node, parser)

        if node_tag == 'process':
            data['process'] = parse_process(parser)

    return data


def translate_field(field):
    return {
        'id': field['id'],
        'type': INPUT_TYPE_MAP.get(field['type']),
        'label': field['label'],
        'widget': INPUT_WIDGET_MAP.get(field['type']),
    }


def translate_form(form):
    return {
        'id': form['id'],
        'min_amount': form['min_amount'],
        'max_amount': form['max_amount'],
        'fields': collections.OrderedDict(
            (field['id'], translate_field(field))
            for field in form['inputs']
        ),
    }


def translate_action(task, requirements: set, predecessors: set):
    fieldsets = collections.OrderedDict(
        (form['id'], translate_form(form))
        for form in task['forms']
    )

    fields = []
    for fieldset_id, fieldset in fieldsets.items():
        for field_id, field in fieldset['fields'].items():
            fields.append(f'{fieldset_id}.{field_id}')

        fieldset['fields'] = dict(fieldset['fields'])
    fieldsets = dict(fieldsets)

    return {
        'tasks': {
            task['id']: {
                'id': task['id'],
                'type': task['type'],
                'name': task['name'],
                'description': task['description'],
                'predecessors': predecessors.copy(),
                'requirements': requirements.copy(),
                'fields': fields,
                'condition': None,
            },
        },
        'fieldsets': fieldsets,
        'leaves': set(),
    }


def translate_validation(task, requirements: set, predecessors: set):
    return {
        'tasks': {
            task['id']: {
                'id': task['id'],
                'type': task['type'],
                'name': task['name'],
                'description': task['description'],
                'predecessors': predecessors.copy(),
                'requirements': requirements.copy(),
                'fields': task['dependencies'].copy(),
                'condition': None,
            },
        },
        'fieldsets': {},
        'leaves': set(),
    }


def translate_ifelifelse(task, requirements: set, predecessors: set):
    local_requirements = requirements.copy()
    if task.get('condition') is not None:
        local_requirements.add(task.get('condition'))

    data = translate_tasks(
        task['children'],
        local_requirements.copy(),
        {task['id']},
    )

    return {
        'tasks': deep_update(
            {task['id']: {
                'id': task['id'],
                'type': task['type'],
                'name': '',
                'description': '',
                'predecessors': predecessors.copy(),
                'requirements': requirements.copy(),
                'condition': task.get('condition'),
                'fields': [],
            }},
            data['tasks'],
        ),
        'fieldsets': data['fieldsets'],
        'leaves': data['leaves'].copy(),
    }


def translate_request(task, requirements: set, predecessors: set):
    return {
        'tasks': {
            task['id']: {
                'id': task['id'],
                'type': task['type'],
                'name': '',
                'description': '',
                'predecessors': predecessors.copy(),
                'requirements': requirements.copy(),
                'fields': [],
                'condition': None,
            },
        },
        'fieldsets': {},
        'leaves': set(),
    }


def translate_call(task, requirements: set, predecessors: set):
    return {
        'tasks': {
            task['id']: {
                'id': task['id'],
                'type': task['type'],
                'name': '',
                'description': '',
                'predecessors': predecessors.copy(),
                'requirements': requirements.copy(),
                'fields': [],
                'condition': None,
            },
        },
        'fieldsets': {},
        'leaves': set(),
    }


def translate_connection(task, requirements: set, predecessors: set):
    return {
        'tasks': {
            task['id']: {
                'id': task['id'],
                'type': task['type'],
                'name': '',
                'description': '',
                'predecessors': predecessors.copy(),
                'requirements': requirements.copy(),
                'fields': [],
                'condition': None,
            },
        },
        'fieldsets': {},
        'leaves': set(),
    }


def translate_task(task, requirements: set, predecessors: set):
    translator_map = {
        'action': translate_action,
        'validation': translate_validation,
        'if': translate_ifelifelse,
        'elif': translate_ifelifelse,
        'else': translate_ifelifelse,
        'request': translate_request,
        'call': translate_call,
        'connection': translate_connection,
    }

    fn = translator_map[task['type']]

    return fn(
        task,
        requirements.copy(),
        predecessors.copy(),
    )


def translate_tasks(tasks, requirements: set, predecessors: set):
    translated = {
        'tasks': {},
        'fieldsets': {},
        'leaves': predecessors.copy(),
    }

    for index, task in enumerate(tasks):
        local_requirements = requirements.copy()
        if task['type'] in ['elif', 'else']:
            previous_task = tasks[index-1]
            previous_condition = previous_task['condition']
            previous_requirements = (
                translated['tasks'][previous_task['id']]['requirements']
            )
            local_requirements.add(f'!({previous_condition})')
            local_requirements.update(previous_requirements)

        local_predecessors = translated['leaves'].copy()
        if task['type'] in ['elif', 'else']:
            previous_task = tasks[index-1]
            local_predecessors = {tasks[index-1]['id']}
            local_predecessors.update(
                translated['tasks'][previous_task['id']]['predecessors'],
            )

        data = translate_task(
            task,
            local_requirements,
            local_predecessors,
        )
        translated['tasks'] = deep_update(
            translated['tasks'],
            data['tasks'],
        )
        translated['fieldsets'] = deep_update(
            translated['fieldsets'],
            data['fieldsets'],
        )

        if task['type'] == 'if':
            translated['leaves'] = data['leaves']

        if task['type'] in ['elif', 'else']:
            translated['leaves'].update(data['leaves'])

        if task['type'] not in ['if', 'elif', 'else']:
            translated['leaves'] = {task['id']}

    return translated


def translate_parsed_process(parsed_process):
    data = translate_tasks(
        parsed_process['process'],
        set(),
        set(),
    )

    return {
        'name': parsed_process['process-info']['name'],
        'description': parsed_process['process-info']['description'],
        'author': parsed_process['process-info']['author'],
        'public': parsed_process['process-info']['public'],
        'tasks': data['tasks'],
        'fieldsets': data['fieldsets'],
    }
