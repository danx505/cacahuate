import cacahuate.models

import graphene
import graphene.relay

import graphene_sqlalchemy


class Execution(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Execution
        interfaces = (
            graphene.relay.Node,
        )


class Pointer(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Pointer
        interfaces = (
            graphene.relay.Node,
        )


class Value(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Value
        interfaces = (
            graphene.relay.Node,
        )


class User(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.User
        interfaces = (
            graphene.relay.Node,
        )


class Group(graphene_sqlalchemy.SQLAlchemyObjectType):
    class Meta:
        model = cacahuate.models.Group
        interfaces = (
            graphene.relay.Node,
        )


class Query(
    graphene.ObjectType,
):
    node = graphene.relay.Node.Field()

    execution = graphene.relay.Node.Field(Execution)
    executions = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Execution,
        id__in=graphene.List(graphene.String),
        process_name__in=graphene.List(graphene.String),
        process_name__startswith=graphene.String(),
        process_name__contains=graphene.String(),
        process_name__icontains=graphene.String(),
        name__contains=graphene.String(),
        name__icontains=graphene.String(),
        started_at__gt=graphene.DateTime(),
        started_at__ge=graphene.DateTime(),
        started_at__lt=graphene.DateTime(),
        started_at__le=graphene.DateTime(),
        updated_at__gt=graphene.DateTime(),
        updated_at__ge=graphene.DateTime(),
        updated_at__lt=graphene.DateTime(),
        updated_at__le=graphene.DateTime(),
        finished_at__gt=graphene.DateTime(),
        finished_at__ge=graphene.DateTime(),
        finished_at__lt=graphene.DateTime(),
        finished_at__le=graphene.DateTime(),
        started_at__time__gt=graphene.Time(),
        started_at__time__ge=graphene.Time(),
        started_at__time__lt=graphene.Time(),
        started_at__time__le=graphene.Time(),
        updated_at__time__gt=graphene.Time(),
        updated_at__time__ge=graphene.Time(),
        updated_at__time__lt=graphene.Time(),
        updated_at__time__le=graphene.Time(),
        finished_at__time__gt=graphene.Time(),
        finished_at__time__ge=graphene.Time(),
        finished_at__time__lt=graphene.Time(),
        finished_at__time__le=graphene.Time(),
        started_at__date__gt=graphene.Date(),
        started_at__date__ge=graphene.Date(),
        started_at__date__lt=graphene.Date(),
        started_at__date__le=graphene.Date(),
        updated_at__date__gt=graphene.Date(),
        updated_at__date__ge=graphene.Date(),
        updated_at__date__lt=graphene.Date(),
        updated_at__date__le=graphene.Date(),
        finished_at__date__gt=graphene.Date(),
        finished_at__date__ge=graphene.Date(),
        finished_at__date__lt=graphene.Date(),
        finished_at__date__le=graphene.Date(),
        status__in=graphene.List(graphene.String),
        tasks__status__in=graphene.List(graphene.String),
        tasks__actors__identifier__in=graphene.List(graphene.String),
        tasks__actors__fullname__contains=graphene.List(graphene.String),
        tasks__actors__fullname__icontains=graphene.List(graphene.String),
        tasks__actors__groups__codename__in=graphene.List(graphene.String),
        tasks__candidates__identifier__in=graphene.List(graphene.String),
        tasks__candidates__fullname__contains=graphene.List(graphene.String),
        tasks__candidates__fullname__icontains=graphene.List(graphene.String),
        tasks__candidates__groups__codename__in=graphene.List(graphene.String),
    )

    pointer = graphene.relay.Node.Field(Pointer)
    pointers = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Pointer,
        id__in=graphene.List(graphene.String),
        node_id__in=graphene.List(graphene.String),
        node_type__in=graphene.List(graphene.String),
        name__contains=graphene.String(),
        name__icontains=graphene.String(),
        started_at__gt=graphene.DateTime(),
        started_at__ge=graphene.DateTime(),
        started_at__lt=graphene.DateTime(),
        started_at__le=graphene.DateTime(),
        updated_at__gt=graphene.DateTime(),
        updated_at__ge=graphene.DateTime(),
        updated_at__lt=graphene.DateTime(),
        updated_at__le=graphene.DateTime(),
        finished_at__gt=graphene.DateTime(),
        finished_at__ge=graphene.DateTime(),
        finished_at__lt=graphene.DateTime(),
        finished_at__le=graphene.DateTime(),
        started_at__time__gt=graphene.Time(),
        started_at__time__ge=graphene.Time(),
        started_at__time__lt=graphene.Time(),
        started_at__time__le=graphene.Time(),
        updated_at__time__gt=graphene.Time(),
        updated_at__time__ge=graphene.Time(),
        updated_at__time__lt=graphene.Time(),
        updated_at__time__le=graphene.Time(),
        finished_at__time__gt=graphene.Time(),
        finished_at__time__ge=graphene.Time(),
        finished_at__time__lt=graphene.Time(),
        finished_at__time__le=graphene.Time(),
        started_at__date__gt=graphene.Date(),
        started_at__date__ge=graphene.Date(),
        started_at__date__lt=graphene.Date(),
        started_at__date__le=graphene.Date(),
        updated_at__date__gt=graphene.Date(),
        updated_at__date__ge=graphene.Date(),
        updated_at__date__lt=graphene.Date(),
        updated_at__date__le=graphene.Date(),
        finished_at__date__gt=graphene.Date(),
        finished_at__date__ge=graphene.Date(),
        finished_at__date__lt=graphene.Date(),
        finished_at__date__le=graphene.Date(),
        status__in=graphene.List(graphene.String),
        execution_id__in=graphene.List(graphene.String),
        execution__process_name__startswith=graphene.String(),
        execution__process_name__contains=graphene.String(),
        actors__identifier__in=graphene.List(graphene.String),
        actors__fullname__contains=graphene.List(graphene.String),
        actors__fullname__icontains=graphene.List(graphene.String),
        actors__groups__codename__in=graphene.List(graphene.String),
        candidates__identifier__in=graphene.List(graphene.String),
        candidates__fullname__contains=graphene.List(graphene.String),
        candidates__fullname__icontains=graphene.List(graphene.String),
        candidates__groups__codename__in=graphene.List(graphene.String),
    )

    value = graphene.relay.Node.Field(Value)
    values = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Value,
    )

    user = graphene.relay.Node.Field(User)
    users = graphene_sqlalchemy.SQLAlchemyConnectionField(
        User,
        identifier__in=graphene.List(graphene.String),
        fullname__contains=graphene.String(),
        fullname__icontains=graphene.String(),
        groups__codename__in=graphene.List(graphene.String),
    )

    group = graphene.relay.Node.Field(Group)
    groups = graphene_sqlalchemy.SQLAlchemyConnectionField(
        Group,
        codename__in=graphene.List(graphene.String),
        name__contains=graphene.String(),
        name__icontains=graphene.String(),
        users__identifier__in=graphene.List(graphene.String),
    )

    @staticmethod
    def resolve_executions(
        root,
        info,
        **kwargs,
    ):
        from cacahuate.database.query import query_executions
        query = query_executions(**kwargs)
        return query.all()

    @staticmethod
    def resolve_pointers(
        root,
        info,
        **kwargs,
    ):
        from cacahuate.database.query import query_pointers
        query = query_pointers(**kwargs)
        return query.all()

    @staticmethod
    def resolve_users(
        root,
        info,
        **kwargs,
    ):
        from cacahuate.database.query import query_users
        query = query_users(**kwargs)
        return query.all()

    @staticmethod
    def resolve_groups(
        root,
        info,
        **kwargs,
    ):
        from cacahuate.database.query import query_groups
        query = query_groups(**kwargs)
        return query.all()


schema = graphene.Schema(
    query=Query,
)
