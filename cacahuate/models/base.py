import enum
import json

import cacahuate.http.json

import sqlalchemy
import sqlalchemy.sql
import sqlalchemy.types
from sqlalchemy.orm import (
    ColumnProperty,
    class_mapper,
)


class ModelMixin:
    @classmethod
    def get_fields(cls, ignore=None):
        if ignore is None:
            ignore = []

        return [
            prop.key for prop
            in class_mapper(cls).iterate_properties
            if all([
                isinstance(prop, ColumnProperty),
                prop.key not in ignore,
            ])
        ]

    def as_json(self, include=None):
        if include is None:
            include = ['*']

        base_dict = {
            **{
                field: getattr(self, field)
                for field in self.get_fields()
                if any([
                    include is None,
                    field in include,
                    '*' in include,
                ])
            },
        }

        if '_type' in include or '*' in include:
            base_dict['_type'] = str(self.__class__.__name__).lower()

        return json.loads(json.dumps(
            base_dict,
            cls=cacahuate.http.json.JSONEncoder,
        ))


class EnumField(sqlalchemy.types.TypeDecorator):
    impl = sqlalchemy.String
    cache_ok = True

    def __init__(self, enumtype, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._enumtype = enumtype

    def process_bind_param(self, value, dialect):
        if isinstance(value, enum.Enum):
            return value
        if isinstance(value, str):
            return value
        return value.value

    def process_result_value(self, value, dialect):
        return self._enumtype(value)
