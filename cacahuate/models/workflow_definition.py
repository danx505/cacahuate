import datetime
import enum
import typing

import cacahuate.database
from cacahuate.models.base import (
    EnumField,
    ModelMixin,
)

import sqlalchemy
import sqlalchemy.sql
from sqlalchemy.ext.associationproxy import (
    AssociationProxy,
    association_proxy,
)
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
)
from sqlalchemy.schema import (
    UniqueConstraint,
)


task_predecessor: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'task_predecessor',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'task_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('taskdefinition.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'predecessor_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('taskdefinition.id'),
        primary_key=True,
    ),
    UniqueConstraint(
        'task_id',
        'predecessor_id',
    ),
)


task_rollbacktarget: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'task_rollbacktarget',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'task_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('taskdefinition.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'target_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('taskdefinition.id'),
        primary_key=True,
    ),
    UniqueConstraint(
        'task_id',
        'target_id',
    ),
)


class WorkflowDefinition(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'workflowdefinition'

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
        index=True,
    )

    created_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        server_default=sqlalchemy.sql.func.now(),
        nullable=False,
    )
    updated_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=False,
        server_default=sqlalchemy.sql.func.now(),
        onupdate=sqlalchemy.sql.func.now(),
        index=True,
    )

    identifier: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        unique=True,
        index=True,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        index=True,
    )

    description: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
    )

    workflow_name: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )

    workflow_description: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )

    tasks: Mapped[list['TaskDefinition']] = relationship(
        'TaskDefinition',
        back_populates='workflow',
        lazy='dynamic',
        cascade='all,delete',
    )

    fieldsets: Mapped[list['FieldsetDefinition']] = relationship(
        'FieldsetDefinition',
        back_populates='workflow',
        lazy='dynamic',
        cascade='all,delete',
    )


class TaskDefinition(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'taskdefinition'
    __table_args__ = (
        UniqueConstraint(
            'identifier',
            'workflow_id',
        ),
    )

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
        index=True,
    )

    identifier: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        index=True,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        index=True,
    )

    description: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
    )

    task_name: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )

    task_description: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )

    condition: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )

    task_field_associations: Mapped[list['TaskField']] = relationship(
        'TaskField',
        back_populates='task',
        cascade='all, delete-orphan',
    )
    fields: AssociationProxy[list['Field']] = association_proxy(
        'task_field_associations',
        'field',
    )

    preceding_tasks: Mapped[list['TaskDefinition']] = relationship(
        'TaskDefinition',
        secondary=task_predecessor,
        primaryjoin='TaskDefinition.id == task_predecessor.c.task_id',
        secondaryjoin='TaskDefinition.id == task_predecessor.c.predecessor_id',
        back_populates='following_tasks',
        lazy='dynamic',
        cascade='all,delete',
    )
    following_tasks: Mapped[list['TaskDefinition']] = relationship(
        'TaskDefinition',
        secondary=task_predecessor,
        primaryjoin='TaskDefinition.id == task_predecessor.c.predecessor_id',
        secondaryjoin='TaskDefinition.id == task_predecessor.c.task_id',
        back_populates='preceding_tasks',
        lazy='dynamic',
        cascade='all,delete',
    )

    rollback_targets: Mapped[list['TaskDefinition']] = relationship(
        'TaskDefinition',
        secondary=task_rollbacktarget,
        primaryjoin='TaskDefinition.id == task_rollbacktarget.c.task_id',
        secondaryjoin='TaskDefinition.id == task_rollbacktarget.c.target_id',
        back_populates='rollback_sources',
        lazy='dynamic',
        cascade='all,delete',
    )
    rollback_sources: Mapped[list['TaskDefinition']] = relationship(
        'TaskDefinition',
        secondary=task_rollbacktarget,
        primaryjoin='TaskDefinition.id == task_rollbacktarget.c.target_id',
        secondaryjoin='TaskDefinition.id == task_rollbacktarget.c.task_id',
        back_populates='rollback_targets',
        lazy='dynamic',
        cascade='all,delete',
    )

    workflow_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('workflowdefinition.id'),
        nullable=False,
        index=True,
    )
    workflow: Mapped['WorkflowDefinition'] = relationship(
        'WorkflowDefinition',
        back_populates='tasks',
    )


class FieldsetDefinition(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'fieldsetdefinition'
    __table_args__ = (
        UniqueConstraint(
            'identifier',
            'workflow_id',
        ),
    )

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
        index=True,
    )

    identifier: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        index=True,
    )

    label: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
    )

    min_amount: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        nullable=False,
        default=1,
    )

    max_amount: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        nullable=False,
        default=1,
    )

    fields: Mapped[list['Field']] = relationship(
        'Field',
        back_populates='fieldset',
        lazy='dynamic',
        cascade='all,delete',
    )

    workflow_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('workflowdefinition.id'),
        index=True,
    )
    workflow: Mapped['WorkflowDefinition'] = relationship(
        'WorkflowDefinition',
        back_populates='fieldsets',
    )


class Field(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'field'
    __table_args__ = (
        UniqueConstraint(
            'identifier',
            'fieldset_id',
        ),
    )

    class FieldType(str, enum.Enum):
        BINARY = 'binary'
        BOOLEAN = 'boolean'
        CHAR = 'char'
        DATE = 'date'
        DATETIME = 'datetime'
        EMAIL = 'email'
        FILE = 'file'
        FLOAT = 'float'
        INTEGER = 'integer'
        JSON = 'json'
        TEXT = 'text'
        URL = 'url'

    class Widget(str, enum.Enum):
        TEXT = 'text'
        NUMBER = 'number'
        EMAIL = 'email'
        URL = 'url'
        PASSWORD = 'password'
        DATE = 'date'
        DATETIME = 'datetime'
        TEXTAREA = 'textarea'

        CHECKBOX = 'checkbox'
        SELECT = 'select'
        SELECTMULTIPLE = 'selectmultiple'
        RADIOSELECT = 'radioselect'
        CHECKBOXSELECTMULTIPLE = 'checkboxselectmultiple'

        FILE = 'file'

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.Integer,
        primary_key=True,
        index=True,
    )

    identifier: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        index=True,
    )

    variant: Mapped[str] = mapped_column(
        EnumField(enumtype=FieldType, length=100),
        nullable=False,
    )

    label: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
    )

    widget: Mapped[str] = mapped_column(
        EnumField(enumtype=Widget, length=100),
        nullable=False,
    )

    field_task_associations: Mapped[list['TaskField']] = relationship(
        'TaskField',
        back_populates='field',
        cascade='all, delete-orphan',
    )
    tasks: AssociationProxy[list['TaskDefinition']] = association_proxy(
        'field_task_associations',
        'task',
    )

    fieldset_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('fieldsetdefinition.id'),
        index=True,
    )
    fieldset: Mapped['FieldsetDefinition'] = relationship(
        'FieldsetDefinition',
        back_populates='fields',
    )


class TaskField(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'task_field'
    __table_args__ = (
        UniqueConstraint(
            'task_id',
            'field_id',
        ),
    )

    class Permissions(str, enum.Enum):
        EDITABLE = 'editable'
        READ_ONLY = 'read_only'

    task_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('taskdefinition.id'),
        primary_key=True,
    )
    task: Mapped['TaskDefinition'] = relationship(
        'TaskDefinition',
        back_populates='task_field_associations',
    )

    permission: Mapped[str] = mapped_column(
        EnumField(enumtype=Permissions, length=20),
        nullable=False,
    )

    field_id: Mapped[int] = mapped_column(
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('field.id'),
        primary_key=True,
    )
    field: Mapped['Field'] = relationship(
        'Field',
        back_populates='field_task_associations',
    )


def parse_yaml_workflow(src):
    workflow = WorkflowDefinition(
        identifier=src['identifier'],
        name=src['name'],
        description=src['description'],
    )

    field_map = {}
    for raw_fieldset in src['fieldsets']:
        fieldset = FieldsetDefinition(
            identifier=raw_fieldset['identifier'],
            label=raw_fieldset['label'],
            min_amount=raw_fieldset['min_amount'],
            max_amount=raw_fieldset['max_amount'],
        )
        workflow.fieldsets.append(fieldset)

        for raw_field in raw_fieldset['fields']:
            field = Field(
                identifier=raw_field['identifier'],
                label=raw_field['label'],
                variant=raw_field['variant'],
                widget=raw_field['widget'],
            )
            fieldset.fields.append(field)
            field_map[f'{fieldset.identifier}.{field.identifier}'] = field

    task_map = {}
    for raw_task in src['tasks']:
        task = TaskDefinition(
            identifier=raw_task['identifier'],
            name=raw_task['name'],
            description=raw_task['description'],
            condition=raw_task['condition'],
        )
        workflow.tasks.append(task)
        task_map[f'{task.identifier}'] = task

        for ref in raw_task['preceding_tasks']:
            related_task = task_map[ref]
            task.preceding_tasks.append(related_task)

        for ref in raw_task['rollback_targets']:
            related_task = task_map[ref]
            task.rollback_targets.append(related_task)

        for raw_field in raw_task['fields']:
            ref = raw_field['field_ref']
            field = field_map[ref]
            task_field = TaskField(
                field=field,
                task=task,
                permission=raw_field['permission'],
            )
            task.task_field_associations.append(task_field)
    return workflow
