import datetime
import enum
import typing
import uuid

import cacahuate.database
from cacahuate.models.base import (
    EnumField,
    ModelMixin,
)

import sqlalchemy
import sqlalchemy.sql
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
    validates,
)


def id_function():
    return str(uuid.uuid4())


activity_actor: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'activity_actor',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'execution_id',
        sqlalchemy.String(80),
        sqlalchemy.ForeignKey('execution.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('user.id'),
        primary_key=True,
    ),
)


task_candidate: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'task_candidate',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'pointer_id',
        sqlalchemy.String(80),
        sqlalchemy.ForeignKey('pointer.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('user.id'),
        primary_key=True,
    ),
)


task_actor: typing.Final[sqlalchemy.Table] = sqlalchemy.Table(
    'task_actor',
    cacahuate.database.Base.metadata,
    sqlalchemy.Column(
        'pointer_id',
        sqlalchemy.String(80),
        sqlalchemy.ForeignKey('pointer.id'),
        primary_key=True,
    ),
    sqlalchemy.Column(
        'user_id',
        sqlalchemy.Integer,
        sqlalchemy.ForeignKey('user.id'),
        primary_key=True,
    ),
)


class Execution(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'execution'

    class Status(str, enum.Enum):
        CANCELLED = 'cancelled'
        FINISHED = 'finished'
        ONGOING = 'ongoing'

    id: Mapped[str] = mapped_column(  # noqa: A003
        sqlalchemy.String(80),
        primary_key=True,
        default=id_function,
        index=True,
    )

    process_name: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        index=True,
    )
    name: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
    )
    name_template: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )
    description: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
    )
    description_template: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        default='',
    )

    pointers: Mapped[list['Pointer']] = relationship(
        'Pointer',
        back_populates='execution',
        lazy='dynamic',
        cascade='all,delete',
    )

    values: Mapped[list['cacahuate.models.workflow_instance.Value']] = relationship(  # noqa: E501
        'cacahuate.models.workflow_instance.Value',
        back_populates='execution',
        lazy='dynamic',
        cascade='all,delete',
    )

    actors: Mapped[list['cacahuate.models.admin.User']] = relationship(
        'cacahuate.models.admin.User',
        secondary=activity_actor,
        back_populates='activities',
        lazy='dynamic',
    )

    started_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=True,
        server_default=sqlalchemy.sql.func.now(),
        index=True,
    )

    updated_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=False,
        server_default=sqlalchemy.sql.func.now(),
        onupdate=sqlalchemy.sql.func.now(),
        index=True,
    )

    finished_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=True,
        index=True,
    )

    status: Mapped[str] = mapped_column(
        EnumField(enumtype=Status, length=20),
        default=Status.ONGOING,
        nullable=False,
        index=True,
    )

    @validates('id')
    def validate_id(self, key, some_string) -> str:
        if len(some_string) <= 1:
            raise ValueError('"id" too short')
        return some_string


class Pointer(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'pointer'

    class Status(str, enum.Enum):
        CANCELLED = 'cancelled'
        FINISHED = 'finished'
        ONGOING = 'ongoing'

    class NodeType(str, enum.Enum):
        ACTION = 'action'
        VALIDATION = 'validation'
        EXIT = 'exit'
        IF = 'if'
        ELIF = 'elif'
        ELSE = 'else'
        REQUEST = 'request'
        CALL = 'call'
        CONNECTION = 'connection'

    id: Mapped[str] = mapped_column(  # noqa: A003
        sqlalchemy.String(80),
        primary_key=True,
        default=id_function,
        index=True,
    )

    node_id: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
    )
    node_type: Mapped[str] = mapped_column(
        EnumField(enumtype=NodeType, length=100),
        nullable=False,
    )

    name: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
    )
    description: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
    )

    candidates: Mapped[list['cacahuate.models.admin.User']] = relationship(
        'cacahuate.models.admin.User',
        secondary=task_candidate,
        back_populates='assigned_tasks',
        lazy='dynamic',
    )

    actors: Mapped[list['cacahuate.models.admin.User']] = relationship(
        'cacahuate.models.admin.User',
        secondary=task_actor,
        back_populates='solved_tasks',
        lazy='dynamic',
    )

    started_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=True,
        server_default=sqlalchemy.sql.func.now(),
        index=True,
    )

    updated_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=False,
        server_default=sqlalchemy.sql.func.now(),
        onupdate=sqlalchemy.sql.func.now(),
        index=True,
    )

    finished_at: Mapped[datetime.datetime] = mapped_column(
        sqlalchemy.DateTime(timezone=True),
        nullable=True,
        index=True,
    )

    status: Mapped[str] = mapped_column(
        EnumField(enumtype=Status, length=20),
        default=Status.ONGOING,
        nullable=False,
        index=True,
    )

    execution_id: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        sqlalchemy.ForeignKey('execution.id'),
        index=True,
    )
    execution: Mapped['Execution'] = relationship(
        'Execution',
        back_populates='pointers',
    )

    @validates('id')
    def validate_id(self, key, some_string) -> str:
        if len(some_string) <= 1:
            raise ValueError('"id" too short')
        return some_string
