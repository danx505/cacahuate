import enum

import cacahuate.database
from cacahuate.models.base import (
    EnumField,
    ModelMixin,
)

import sqlalchemy
import sqlalchemy.sql
from sqlalchemy.orm import (
    Mapped,
    mapped_column,
    relationship,
)


class Value(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'value'

    class Status(str, enum.Enum):
        VALID = 'valid'
        INVALID = 'invalid'

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.BigInteger,
        primary_key=True,
        index=True,
    )

    execution_id: Mapped[str] = mapped_column(
        sqlalchemy.String(80),
        sqlalchemy.ForeignKey('execution.id'),
        index=True,
    )
    execution: Mapped['cacahuate.models.legacy.Execution'] = relationship(
        'cacahuate.models.legacy.Execution',
        back_populates='values',
    )

    fieldset_ref: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        index=True,
    )

    fieldset_index: Mapped[str] = mapped_column(
        sqlalchemy.Integer,
        nullable=False,
        index=True,
    )

    field_ref: Mapped[str] = mapped_column(
        sqlalchemy.String(100),
        nullable=False,
        index=True,
    )

    value_caption: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        index=True,
    )

    value_render: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        index=True,
    )

    value_entry: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        index=True,
    )

    status: Mapped[str] = mapped_column(
        EnumField(enumtype=Status, length=20),
        nullable=False,
    )

    options: Mapped[list['ValueOption']] = relationship(
        'ValueOption',
        back_populates='value',
        lazy='dynamic',
        cascade='all,delete',
    )


class ValueOption(cacahuate.database.Base, ModelMixin):
    __tablename__ = 'value_option'

    id: Mapped[int] = mapped_column(  # noqa: A003
        sqlalchemy.BigInteger,
        primary_key=True,
        index=True,
    )

    value_caption: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        index=True,
    )

    value_render: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        index=True,
    )

    value_entry: Mapped[str] = mapped_column(
        sqlalchemy.Text,
        nullable=False,
        index=True,
    )

    value_id: Mapped[str] = mapped_column(
        sqlalchemy.BigInteger,
        sqlalchemy.ForeignKey('value.id'),
        index=True,
    )
    value: Mapped['Value'] = relationship(
        'Value',
        back_populates='options',
    )


def unpack_execution_values(instance, values_dict):
    built = []

    def parse_value(instance, data, raw_value):
        if isinstance(raw_value, dict):
            # Code to parse links
            if all(k in raw_value for k in [
                'label',
                'href',
            ]):
                value = raw_value['href']
                return Value(
                    fieldset_ref=data['fieldset_ref'],
                    fieldset_index=data['fieldset_index'],
                    field_ref=data['field_ref'],
                    value_caption='',
                    value_render='',
                    value_entry=str(value) if value is not None else '',
                    status=Value.Status.VALID,
                )

            # Code to parse files
            if all(k in raw_value for k in [
                'id',
                'mime',
                'name',
                'type',
            ]):
                value = raw_value['id']
                return Value(
                    fieldset_ref=data['fieldset_ref'],
                    fieldset_index=data['fieldset_index'],
                    field_ref=data['field_ref'],
                    value_caption='',
                    value_render='',
                    value_entry=str(value) if value is not None else '',
                    status=Value.Status.VALID,
                )

        # Code to parse checkbox
        if isinstance(raw_value, list):
            return Value(
                fieldset_ref=data['fieldset_ref'],
                fieldset_index=data['fieldset_index'],
                field_ref=data['field_ref'],
                value_caption='',
                value_render='',
                value_entry='',
                options=[
                    ValueOption(
                        value_caption='',
                        value_render='',
                        value_entry=str(value) if value is not None else '',
                    )
                    for value in raw_value
                ],
                status=Value.Status.VALID,
            )

        # Code to store integers, strings, floats, etc
        return Value(
            fieldset_ref=data['fieldset_ref'],
            fieldset_index=data['fieldset_index'],
            field_ref=data['field_ref'],
            value_caption='',
            value_render='',
            value_entry=str(raw_value) if raw_value is not None else '',
            status=Value.Status.VALID,
        )

    for fieldset_ref, fieldsets in values_dict.items():
        # Ignore system fieldsets
        if fieldset_ref in ['_env', '_execution']:
            continue

        for fieldset_index, fieldset in enumerate(fieldsets.all()):
            # Ignore validation fieldsets
            if all(k in fieldset.keys() for k in [
                'response',
                'comment',
                'inputs',
            ]):
                continue

            for field_ref, value in fieldset.items():
                built.append(parse_value(
                    instance=instance,
                    data={
                        'fieldset_ref': fieldset_ref,
                        'fieldset_index': fieldset_index,
                        'field_ref': field_ref,
                    },
                    raw_value=value,
                ))

    return built
