import sqlalchemy
import sqlalchemy.ext.declarative
import sqlalchemy.orm


engine = None
db_session = sqlalchemy.orm.scoped_session(
    lambda: sqlalchemy.orm.create_session(
        autoflush=False,
        autocommit=False,
        bind=engine,
    ),
)

Base = sqlalchemy.orm.declarative_base()
Base.query = db_session.query_property()


def init_engine(uri, **kwargs):
    global engine
    engine = sqlalchemy.create_engine(uri, **kwargs)
    return engine


def init_db(engine):
    from cacahuate.models import (  # noqa
        Category,
        Execution,
        Group,
        Permission,
        Pointer,
        Quirk,
        User,
        Value,
        ValueOption,
        activity_actor,
        group_permission,
        group_user,
        task_actor,
        task_candidate,
        user_permission,
        user_quirk,
    )

    Base.metadata.create_all(bind=engine)
