from cacahuate.database import db_session
from cacahuate.models import (
    Execution,
    Group,
    Permission,
    Pointer,
    Quirk,
    User,
)

from sqlalchemy import (
    Date,
    Time,
    and_,
    asc,
    desc,
    or_,
    select,
)
from sqlalchemy.orm import (
    ColumnProperty,
    aliased,
    class_mapper,
)
from sqlalchemy.sql import (
    cast,
    func,
)


def extract_filters(model, prefix='', alias=None, **kwargs):
    base_operators = {
        'eq': '__eq__',
        'in': 'in_',
        'not_in': 'not_in',
        'contains': 'contains',
        'icontains': 'icontains',
        'startswith': 'startswith',
        'istartswith': 'istartswith',
        'gt': '__gt__',
        'ge': '__ge__',
        'lt': '__lt__',
        'le': '__le__',
    }

    cast_operators = {
        'time__gt': (Time, '__gt__'),
        'time__ge': (Time, '__ge__'),
        'time__lt': (Time, '__lt__'),
        'time__le': (Time, '__le__'),
        'date__gt': (Date, '__gt__'),
        'date__ge': (Date, '__ge__'),
        'date__lt': (Date, '__lt__'),
        'date__le': (Date, '__le__'),
    }

    fields = model.get_fields()

    fltrs = []
    for fld in fields:
        fld_candidate = f'{prefix}{fld}'
        for op, fn in base_operators.items():
            q_arg = kwargs.get(f'{fld_candidate}__{op}')
            if q_arg is not None:
                source = model
                if alias:
                    source = alias

                model_field = getattr(source, fld)
                q_fn = getattr(model_field, fn)
                fltrs.append(q_fn(q_arg))

        for op, (cast_type, fn) in cast_operators.items():
            q_arg = kwargs.get(f'{fld_candidate}__{op}')
            if q_arg is not None:
                source = model
                if alias:
                    source = alias

                model_field = getattr(source, fld)
                q_fn = getattr(cast(model_field, cast_type), fn)
                fltrs.append(q_fn(q_arg))

    return fltrs


def query_executions(**kwargs):
    stmt = select(Execution)

    q_arg = kwargs.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Execution.name).contains(func.lower(q_arg)),
            Execution.id.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Execution,
        **kwargs,
    ):
        stmt = stmt.filter(fltr)

    task, task_join = (aliased(Pointer), False)
    for fltr in extract_filters(
        model=Pointer,
        prefix='tasks__',
        alias=task,
        **kwargs,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        stmt = stmt.filter(fltr)

    task_actor, task_actor_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='tasks__actors__',
        alias=task_actor,
        **kwargs,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_actor_join:
            task_actor_join = True
            stmt = stmt.join(task_actor, task.actors)
        stmt = stmt.filter(fltr)

    task_actor_group, task_actor_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='tasks__actors__groups__',
        alias=task_actor_group,
        **kwargs,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_actor_join:
            task_actor_join = True
            stmt = stmt.join(task_actor, task.actors)
        if not task_actor_group_join:
            task_actor_group_join = True
            stmt = stmt.join(task_actor_group, task_actor.groups)
        stmt = stmt.filter(fltr)

    task_candidate, task_candidate_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='tasks__candidates__',
        alias=task_candidate,
        **kwargs,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_candidate_join:
            task_candidate_join = True
            stmt = stmt.join(task_candidate, task.candidates)
        stmt = stmt.filter(fltr)

    task_candidate_group, task_candidate_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='tasks__candidates__groups__',
        alias=task_candidate_group,
        **kwargs,
    ):
        if not task_join:
            task_join = True
            stmt = stmt.join(task, Execution.pointers)
        if not task_candidate_join:
            task_candidate_join = True
            stmt = stmt.join(task_candidate, task.candidates)
        if not task_candidate_group_join:
            task_candidate_group_join = True
            stmt = stmt.join(task_candidate_group, task_candidate.groups)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(desc(Execution.started_at))

    q_arg = kwargs.get('offset')
    if q_arg is not None:
        stmt = stmt.offset(q_arg)

    q_arg = kwargs.get('limit')
    if q_arg is not None:
        stmt = stmt.limit(q_arg)

    return db_session.scalars(stmt)


def query_pointers(**kwargs):
    stmt = select(Pointer)

    q_arg = kwargs.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Pointer.name).contains(func.lower(q_arg)),
            Pointer.id.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Pointer,
        **kwargs,
    ):
        stmt = stmt.filter(fltr)

    execution, execution_join = (aliased(Execution), False)
    for fltr in extract_filters(
        model=Execution,
        prefix='execution__',
        alias=execution,
        **kwargs,
    ):
        if not execution_join:
            execution_join = True
            stmt = stmt.join(execution, Pointer.execution)
        stmt = stmt.filter(fltr)

    actor, actor_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='actors__',
        alias=actor,
        **kwargs,
    ):
        if not actor_join:
            actor_join = True
            stmt = stmt.join(actor, Pointer.actors)
        stmt = stmt.filter(fltr)

    actor_group, actor_group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='actors__groups__',
        alias=actor_group,
        **kwargs,
    ):
        if not actor_join:
            actor_join = True
            stmt = stmt.join(actor, Pointer.actors)
        if not actor_group_join:
            actor_group_join = True
            stmt = stmt.join(actor_group, actor.groups)
        stmt = stmt.filter(fltr)

    candidate, candidate_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='candidates__',
        alias=candidate,
        **kwargs,
    ):
        if not candidate_join:
            candidate_join = True
            stmt = stmt.join(candidate, Pointer.candidates)
        stmt = stmt.filter(fltr)

    candidate_group, candidate_groups_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='candidates__groups__',
        alias=candidate_group,
        **kwargs,
    ):
        if not candidate_join:
            candidate_join = True
            stmt = stmt.join(candidate, Pointer.candidates)
        if not candidate_groups_join:
            candidate_groups_join = True
            stmt = stmt.join(candidate_group, candidate.groups)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(desc(Pointer.started_at))

    q_arg = kwargs.get('offset')
    if q_arg is not None:
        stmt = stmt.offset(q_arg)

    q_arg = kwargs.get('limit')
    if q_arg is not None:
        stmt = stmt.limit(q_arg)

    return db_session.scalars(stmt)


def query_users(**kwargs):
    stmt = select(User)

    q_arg = kwargs.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(User.fullname).contains(func.lower(q_arg)),
            User.identifier.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=User,
        **kwargs,
    ):
        stmt = stmt.filter(fltr)

    group, group_join = (aliased(Group), False)
    for fltr in extract_filters(
        model=Group,
        prefix='groups__',
        alias=group,
        **kwargs,
    ):
        if not group_join:
            group_join = True
            stmt = stmt.join(group, User.groups)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(User.fullname))

    q_arg = kwargs.get('offset')
    if q_arg is not None:
        stmt = stmt.offset(q_arg)

    q_arg = kwargs.get('limit')
    if q_arg is not None:
        stmt = stmt.limit(q_arg)

    return db_session.scalars(stmt)


def query_groups(**kwargs):
    stmt = select(Group)

    q_arg = kwargs.get('search_query')
    if q_arg is not None:
        stmt = stmt.filter(or_(
            func.lower(Group.name).contains(func.lower(q_arg)),
            Group.codename.__eq__(q_arg),
        ))

    for fltr in extract_filters(
        model=Group,
        **kwargs,
    ):
        stmt = stmt.filter(fltr)

    user, user_join = (aliased(User), False)
    for fltr in extract_filters(
        model=User,
        prefix='users__',
        alias=user,
        **kwargs,
    ):
        if not user_join:
            user_join = True
            stmt = stmt.join(user, Group.users)
        stmt = stmt.filter(fltr)

    stmt = stmt.distinct()
    stmt = stmt.order_by(asc(Group.name))

    q_arg = kwargs.get('offset')
    if q_arg is not None:
        stmt = stmt.offset(q_arg)

    q_arg = kwargs.get('limit')
    if q_arg is not None:
        stmt = stmt.limit(q_arg)

    return db_session.scalars(stmt)


def filter_users(**data):
    keys = data.keys()

    def has_q_prefix(key, prefix):
        return key.startswith(f'{prefix}.') or key.startswith(f'{prefix}__')

    def remove_prefix(text, prefix):
        if text.startswith(prefix):
            return text[len(prefix):]
        return text

    def remove_q_prefix(key, prefix):
        if key.startswith(f'{prefix}.'):
            return remove_prefix(key, f'{prefix}.')
        if key.startswith(f'{prefix}__'):
            return remove_prefix(key, f'{prefix}__')
        return key

    def clean_q_string(raw_q, default_field=None, default_filter=None):
        if not default_field:
            default_field = 'id'

        if not default_filter:
            default_filter = 'eq'

        clean_q = raw_q
        if clean_q == '':
            clean_q = default_field

        if clean_q.split('__') == 1:
            clean_q = f'{clean_q}__{default_filter}'

        return clean_q

    query = db_session.query(User)

    user_fields = [
        prop.key for prop in class_mapper(User).iterate_properties
        if isinstance(prop, ColumnProperty)
    ]
    base_filters = [
        k for k in keys
        if any(k.startswith(fld) for fld in user_fields)
    ]

    if base_filters:
        built = {
            '{q}'.format(
                q=clean_q_string(
                    k,  # remove prefix not needed
                    default_field='identifier',
                    default_filter='in',
                ),
            ): data.pop(k) for k in base_filters
        }

        for fltr, mtch in built.items():
            key, comp = fltr.split('__')
            if comp == 'eq':
                query = query.filter(
                    getattr(User, key).__eq__(mtch),
                )
            if comp == 'in':
                query = query.filter(
                    getattr(User, key).in_(mtch),
                )

    group_filters = [
        k for k in keys if has_q_prefix(k, 'groups')
    ]

    if group_filters:
        built = {
            '{q}'.format(
                q=clean_q_string(
                    remove_q_prefix(k, 'groups'),
                    default_field='codename',
                    default_filter='in',
                ),
            ): data.pop(k) for k in group_filters
        }

        for fltr, mtch in built.items():
            key, comp = fltr.split('__')
            if comp == 'eq':
                query = query.filter(User.groups.any(
                    getattr(Group, key) == mtch,
                ))
            if comp == 'in':
                query = query.filter(User.groups.any(
                    getattr(Group, key).in_(mtch),
                ))

    permission_filters = [
        k for k in keys if has_q_prefix(k, 'permissions')
    ]

    if permission_filters:
        built = {
            '{q}'.format(
                q=clean_q_string(
                    remove_q_prefix(k, 'permissions'),
                    default_field='codename',
                    default_filter='in',
                ),
            ): data.pop(k) for k in permission_filters
        }

        for fltr, mtch in built.items():
            key, comp = fltr.split('__')
            if comp == 'eq':
                query = query.filter(User.permissions.any(
                    getattr(Permission, key) == mtch,
                ))
            if comp == 'in':
                query = query.filter(User.permissions.any(
                    getattr(Permission, key).in_(mtch),
                ))

    category_filters = keys

    if category_filters:
        queries = {}
        for fltr in category_filters:
            prefix, suffix = fltr.split('__', 1)
            current = queries.setdefault(prefix, [])
            current.append(suffix)

        for category, quirk_filters in queries.items():
            built = {
                '{q}'.format(
                    q=clean_q_string(
                        k,
                        default_field='codename',
                        default_filter='in',
                    ),
                ): data.pop(f'{category}__{k}') for k in quirk_filters
            }

            for fltr, mtch in built.items():
                key, comp = fltr.split('__')
                if comp == 'eq':
                    query = query.filter(User.quirks.any(and_(
                        getattr(Quirk, key) == mtch,
                        Quirk.category.has(codename=category),
                    )))
                if comp == 'in':
                    query = query.filter(User.quirks.any(and_(
                        getattr(Quirk, key).in_(mtch),
                        Quirk.category.has(codename=category),
                    )))

    return query
