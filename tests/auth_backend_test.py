import cacahuate.models
from cacahuate.auth.hierarchy.admin import AdminHierarchyProvider
from cacahuate.auth.hierarchy.backref import BackrefHierarchyProvider
from cacahuate.database import db_session


def test_backref_backend(config):
    br = BackrefHierarchyProvider(config)

    users = br.find_users(identifier='juan')

    assert len(users) == 1

    user = users[0]

    assert user[0] == 'juan'
    assert user[1] == {
        'identifier': 'juan',
        'email': 'juan',
        'fullname': 'juan',
    }


def test_admin_backend(config):
    groups = [
        cacahuate.models.Group(**item)
        for item in [{
            'codename': f'group_{index}',
            'name': f'Group {index}',
        } for index in range(4)]
    ]

    permissions = [
        cacahuate.models.Permission(**item)
        for item in [{
            'codename': f'permission_{index}',
            'name': f'Permission {index}',
        } for index in range(4)]
    ]

    categories = [
        cacahuate.models.Category(**item)
        for item in [{
            'codename': f'category_{index}',
            'name': f'Category {index}',
        } for index in range(4)]
    ]

    quirks = [
        cacahuate.models.Quirk(**item)
        for item in [{
            'codename': f'quirk_{index}',
            'name': f'Quirk {index}',
        } for index in range(16)]
    ]

    categories[0].quirks.extend(quirks[0:4])
    categories[1].quirks.extend(quirks[4:8])
    categories[2].quirks.extend(quirks[8:12])
    categories[3].quirks.extend(quirks[12:16])

    db_session.add_all(groups)
    db_session.add_all(permissions)
    db_session.add_all(categories)
    db_session.add_all(quirks)
    db_session.commit()

    def build_user_data(index, is_staff=False, is_superuser=False):
        return {
            'identifier': f'user_{index}',
            'fullname': f'Test User U{index}',
            'email': f'test.user_{index}@email.foo',
            'is_staff': is_staff,
            'is_superuser': is_superuser,
        }

    users = [
        cacahuate.models.User(**item)
        for item in [
            build_user_data(0, False, False),
            build_user_data(1, False, True),
            build_user_data(2, True, False),
            build_user_data(3, True, True),
        ]
    ]

    groups[0].users.extend([users[0], users[1]])
    groups[1].users.extend([users[1], users[2]])
    groups[2].users.extend([users[2], users[3]])
    groups[3].users.extend([users[0], users[3]])

    permissions[0].users.extend([users[0], users[3]])
    permissions[1].users.extend([users[0], users[1]])
    permissions[2].users.extend([users[1], users[2]])
    permissions[3].users.extend([users[2], users[3]])

    users[0].quirks.extend([quirks[0], quirks[4], quirks[8], quirks[12]])
    users[1].quirks.extend([quirks[1], quirks[5], quirks[9], quirks[13]])
    users[2].quirks.extend([quirks[2], quirks[6], quirks[10], quirks[14]])
    users[3].quirks.extend([quirks[3], quirks[7], quirks[11], quirks[15]])

    db_session.add_all(users)
    db_session.commit()

    admin_hi_pro = AdminHierarchyProvider(config)

    # test no filter
    assert sorted(
        u.identifier for u in users
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users()
    )

    # test identifier
    assert sorted(
        u.identifier for u in (users[1], users[3])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'identifier__in': [
                users[1].identifier,
                users[3].identifier,
            ],
        })
    )

    # test email
    assert sorted(
        u.identifier for u in (users[0], users[2])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'email__in': [
                users[0].email,
                users[2].email,
            ],
        })
    )

    # test group
    assert sorted(
        u.identifier for u in (users[0], users[1], users[2])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'groups__codename__in': [
                groups[0].codename,
                groups[1].codename,
            ],
        })
    )

    assert sorted(
        u.identifier for u in (users[0], users[1])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'groups__codename__in': [
                groups[0].codename,
                groups[1].codename,
            ],
            'groups__name__in': [
                groups[0].name,
            ],
        })
    )

    assert [] == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'groups__codename__in': [
                groups[0].codename,
            ],
            'groups__name__in': [
                groups[2].name,
            ],
        })
    )

    # test permission
    assert sorted(
        u.identifier for u in (users[0], users[1], users[3])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'permissions__codename__in': [
                permissions[0].codename,
                permissions[1].codename,
            ],
        })
    )

    assert sorted(
        u.identifier for u in (users[0], users[1])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'permissions__codename__in': [
                permissions[0].codename,
                permissions[1].codename,
            ],
            'permissions__name__in': [
                permissions[1].name,
            ],
        })
    )

    assert [] == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'permissions__codename__in': [
                permissions[0].codename,
            ],
            'permissions__name__in': [
                permissions[2].name,
            ],
        })
    )

    # test quirk
    assert sorted(
        u.identifier for u in (users[0], users[1])
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            f'{categories[0].codename}__codename__in': [
                quirks[0].codename,
                quirks[1].codename,
            ],
        })
    )

    assert sorted(
        u.identifier for u in (users[1],)
    ) == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            f'{categories[0].codename}__codename__in': [
                quirks[0].codename,
                quirks[1].codename,
            ],
            f'{categories[0].codename}__name__in': [
                quirks[1].name,
            ],
        })
    )

    assert [] == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            f'{categories[0].codename}__codename__in': [
                'invalid',
            ],
        })
    )

    assert [] == sorted(
        u.identifier for u in admin_hi_pro.find_users(**{
            'invalid__codename__in': [
                quirks[0].codename,
                quirks[1].codename,
            ],
        })
    )
