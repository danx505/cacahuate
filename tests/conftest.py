import contextlib
import os

import cacahuate.database
import cacahuate.http.app

import itacate

import pymongo

import pytest


def ismybirthday(value):
    if value.month == 5 and value.day == 10:
        return 'Today is my birthday!'
    return 'Is not my birthday'


TESTING_SETTINGS = {
    'CUSTOM_LOGIN_PROVIDERS': {
        'hardcoded': 'tests.hardcoded_login',
    },
    'CUSTOM_HIERARCHY_PROVIDERS': {
        'hardcoded': 'tests.hardcoded_hierarchy',
        'noparam': 'tests.noparam_hierarchy',
    },
    'PROCESS_ENV': {
        'FOO': 'var',
    },
    'TEMPLATE_PATH': os.path.join(os.path.dirname(__file__), 'templates'),
    'JINJA_FILTERS': {
        'ismybirthday': ismybirthday,
    },
    'ENABLED_LOGIN_PROVIDERS': [],
}


@pytest.fixture(scope='session')
def config():
    ''' Returns a fully loaded configuration dict '''
    con = itacate.Config(
        os.path.join(
            os.path.dirname(os.path.realpath(__file__)),
            '..',
        ),
    )

    con.from_object('cacahuate.settings')
    con.from_mapping(TESTING_SETTINGS)

    return con


@pytest.fixture(scope='session')
def app():
    '''
    Create a Flask app context for the tests.
    '''
    def shutdown_session(exception=None):
        pass

    cacahuate.http.app.shutdown_session = shutdown_session

    app = cacahuate.http.app.create_app()
    app.config.from_mapping(TESTING_SETTINGS)
    yield app


@pytest.fixture(scope='session')
def client(app):
    ''' makes and returns a testclient for the flask application '''
    with app.test_client() as client:
        yield client


@pytest.fixture
def mongo(config):
    uri = config['MONGO_URI']
    client = pymongo.MongoClient(uri)
    database_name = pymongo.uri_parser.parse_uri(uri)['database']
    db = client[database_name]

    yield db

    collection = db[config['POINTER_COLLECTION']]
    collection.drop()

    collection_execution = db[config['EXECUTION_COLLECTION']]
    collection_execution.drop()


@pytest.fixture(scope='session')
def engine(config):
    return cacahuate.database.init_engine(
        config['SQLALCHEMY_DATABASE_URI'],
    )


@pytest.fixture(autouse=True)
def tables(engine):
    cacahuate.database.init_db(engine)

    # clean tables before test
    with contextlib.closing(engine.connect()) as con:
        trans = con.begin()
        for table in reversed(cacahuate.database.Base.metadata.sorted_tables):
            con.execute(table.delete())
        trans.commit()

    yield
