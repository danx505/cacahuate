from cacahuate.database import db_session
from cacahuate.models import (
    parse_yaml_workflow,
)

import pytest

from sqlalchemy.exc import IntegrityError

import yaml


def test_workflowdefinition_valid():
    with open('tests/fixtures/valid.yml', 'r') as stream:
        src = yaml.safe_load(stream)

    instance = parse_yaml_workflow(src)

    db_session.add(instance)
    db_session.commit()

    assert instance.identifier == 'workflow-test'
    assert sorted(
        item.identifier
        for item in instance.fieldsets
    ) == sorted([
        'set_a',
        'set_b',
        'set_c',
    ])


def test_workflowdefinition_duplicated_fieldset():
    with open('tests/fixtures/duplicated_fieldset.yml', 'r') as stream:
        src = yaml.safe_load(stream)

    instance = parse_yaml_workflow(src)

    with pytest.raises(IntegrityError):
        db_session.add(instance)
        db_session.commit()

    db_session.rollback()


def test_workflowdefinition_duplicated_field():
    with open('tests/fixtures/duplicated_field.yml', 'r') as stream:
        src = yaml.safe_load(stream)

    instance = parse_yaml_workflow(src)

    with pytest.raises(IntegrityError):
        db_session.add(instance)
        db_session.commit()

    db_session.rollback()


def test_workflowdefinition_duplicated_task():
    with open('tests/fixtures/duplicated_task.yml', 'r') as stream:
        src = yaml.safe_load(stream)

    instance = parse_yaml_workflow(src)

    with pytest.raises(IntegrityError):
        db_session.add(instance)
        db_session.commit()

    db_session.rollback()
