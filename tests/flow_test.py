from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import Pointer
from cacahuate.xml import Xml
from cacahuate.xml.node import Form, make_node

from tests.utils import make_user


def test_variable_proc_name(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    user = make_user('juan', 'Juan')

    xml = Xml.load(config, 'variable_name.2020-01-17.xml')
    xmliter = iter(xml)
    node = make_node(next(xmliter), xmliter)

    collected_input = [Form.state_json('form01', [
        {
            'name': 'data01',
            'type': 'text',
            'value': '1',
            'value_caption': '1',
        },
    ])]
    execution = xml.start(node, collected_input, mongo, user.identifier)
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': collected_input,
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node02'

    assert execution.name == 'Variable name process in step 1'
    assert execution.description == 'Description is also variable: 1, , '

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form02', [
            {
                'name': 'data02',
                'type': 'text',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node03'

    assert execution.name == 'Variable name process in step 2'
    assert execution.description == 'Description is also variable: 1, 2, '

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form03', [
            {
                'name': 'data03',
                'type': 'text',
                'value': '3',
                'value_caption': '3',
            },
        ])],
    })

    # test stops here because execution gets deleted


def test_variable_proc_name_mix(config, mongo, mocker):
    ''' Test where the name is related to
    multiple forms in diferent nodes of the execution'''
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    user = make_user('juan', 'Juan')

    xml = Xml.load(config, 'variable_name_mix.2020-01-28.xml')
    xmliter = iter(xml)
    node = make_node(next(xmliter), xmliter)

    collected_input = [Form.state_json('form01', [
        {
            'name': 'data01',
            'type': 'text',
            'value': '1',
            'value_caption': '1',
        },
    ])]
    execution = xml.start(node, collected_input, mongo, user.identifier)
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': collected_input,
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node02'

    assert execution.name == 'Variable name process in step 10'
    assert execution.description == 'Description is also variable: 1, , '

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form02', [
            {
                'name': 'data02',
                'type': 'text',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node03'

    assert execution.name == 'Variable name process in step 210'
    assert execution.description == 'Description is also variable: 1, 2, '

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form03', [
            {
                'name': 'data03',
                'type': 'text',
                'value': '3',
                'value_caption': '3',
            },
        ])],
    })

    # test stops here because execution gets deleted


def test_variable_proc_name_pointers(config, mongo, mocker):
    ''' Test pointer name's update'''
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)

    user = make_user('juan', 'Juan')

    xml = Xml.load(config, 'variable_name_mix.2020-01-28.xml')
    xmliter = iter(xml)
    node = make_node(next(xmliter), xmliter)

    collected_input = [Form.state_json('form01', [
        {
            'name': 'data01',
            'type': 'text',
            'value': '1',
            'value_caption': '1',
        },
    ])]
    execution = xml.start(node, collected_input, mongo, user.identifier)
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': collected_input,
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node02'

    assert execution.name == 'Variable name process in step 10'
    assert execution.description == 'Description is also variable: 1, , '

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form02', [
            {
                'name': 'data02',
                'type': 'text',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node03'

    assert execution.name == 'Variable name process in step 210'
    assert execution.description == 'Description is also variable: 1, 2, '

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form03', [
            {
                'name': 'data03',
                'type': 'text',
                'value': '3',
                'value_caption': '3',
            },
        ])],
    })

    # now check pointers last state
    query = {'execution.id': execution.id}

    assert mongo[config["POINTER_COLLECTION"]].count_documents(query) == 3

    expected_name = 'Variable name process in step 3210'
    expected_desc = 'Description is also variable: 1, 2, 3'

    cursor = mongo[config["POINTER_COLLECTION"]].find(query)
    for item in cursor:
        assert item['execution']['name'] == expected_name
        assert item['execution']['description'] == expected_desc
