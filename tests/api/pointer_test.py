from random import choice
from string import ascii_letters

from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import Execution
from cacahuate.tasks import handle
from cacahuate.xml.node import Form

from flask import json

import tests.utils
from tests.utils import (
    make_auth_header,
    make_date,
    make_pointer,
    make_user,
)


def test_continue_process_asks_for_user(client):
    res = client.post('/v1/pointer')

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'You must provide basic authorization headers',
            'where': 'request.authorization',
        }],
    }


def test_continue_process_requires(client):
    juan = make_user('juan', 'Juan')

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({}))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'execution_id' is required",
                'code': 'validation.required',
                'where': 'request.body.execution_id',
            },
            {
                'detail': "'node_id' is required",
                'code': 'validation.required',
                'where': 'request.body.node_id',
            },
        ],
    }


def test_continue_process_asks_living_objects(client):
    ''' the app must validate that the ids sent are real objects '''
    juan = make_user('juan', 'Juan')

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': 'verde',
        'node_id': 'nada',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'execution_id is not valid',
                'code': 'validation.invalid',
                'where': 'request.body.execution_id',
            },
        ],
    }


def test_continue_process_requires_valid_node(client):
    juan = make_user('juan', 'Juan')
    exc = Execution(
        process_name='simple.2018-02-19.xml',
        name=tests.utils.random_string(),
        description=tests.utils.random_string(),
        status='ongoing',
    )
    db_session.add(exc)
    db_session.commit()

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'notarealnode',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'node_id is not a valid node',
                'code': 'validation.invalid_node',
                'where': 'request.body.node_id',
            },
        ],
    }


def test_continue_process_requires_living_pointer(client):
    juan = make_user('juan', 'Juan')
    exc = Execution(
        process_name='simple.2018-02-19.xml',
        name=tests.utils.random_string(),
        description=tests.utils.random_string(),
        status='ongoing',
    )
    db_session.add(exc)
    db_session.commit()

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'mid_node',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'node_id does not have a live pointer',
                'code': 'validation.no_live_pointer',
                'where': 'request.body.node_id',
            },
        ],
    }


def test_continue_process_requires_user_hierarchy(client):
    ''' a node whose auth has a filter must be completed by a person matching
    the filter '''
    juan = make_user('juan', 'Juan')
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': ptr.execution.id,
        'node_id': ptr.node_id,
    }))

    assert res.status_code == 403
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'Provided user does not have this task assigned',
            'where': 'request.authorization',
        }],
    }


def test_continue_process_requires_data(client):
    manager = make_user('juan_manager', 'Juanote')
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    manager.assigned_tasks.extend([ptr])

    db_session.commit()

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(manager)}, data=json.dumps({
        'execution_id': ptr.execution.id,
        'node_id': ptr.node_id,
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [{
            'detail': "form count lower than expected for ref mid_form",
            'where': 'request.body.form_array',
        }],
    }


def test_continue_process(client, mocker, config):
    mocker.patch('cacahuate.tasks.handle.delay')

    manager = make_user('juan_manager', 'Juanote')
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    manager.assigned_tasks.extend([ptr])

    db_session.commit()

    exc = ptr.execution

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(manager)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': ptr.node_id,
        'form_array': [
            {
                'ref': 'mid_form',
                'data': {
                    'data': 'yes',
                },
            },
        ],
    }))

    assert res.status_code == 202
    assert json.loads(res.data) == {
        'data': 'accepted',
    }

    # rabbit is called
    handle.delay.assert_called_once()

    json_message = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan_manager',
        'input': [Form.state_json('mid_form', [
            {
                "name": "data",
                "type": "text",
                "value": "yes",
                'label': 'data',
                'value_caption': 'yes',
                'state': 'valid',
                'hidden': False,
            },
        ])],
    }

    args = handle.delay.call_args[0][0]
    body = json.loads(args)
    assert body == json_message

    # makes a useful call for the handler
    handler = Handler(config)

    pointer, user, inputs = handler.recover_step(json_message)

    assert pointer.id == ptr.id


def test_validation_requirements(client):
    juan = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    exc = ptr.execution

    juan.assigned_tasks.append(ptr)

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'response' is required",
                'code': 'validation.required',
                'where': 'request.body.response',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': ''.join(choice(ascii_letters) for c in range(10)),
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'response' value invalid",
                'code': 'validation.invalid',
                'where': 'request.body.response',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs' is required",
                'code': 'validation.required',
                'where': 'request.body.inputs',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': 'de',
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs' must be a list",
                'code': 'validation.required_list',
                'where': 'request.body.inputs',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs' must be a list",
                'code': 'validation.required_list',
                'where': 'request.body.inputs',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': ['de'],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs.0' must be an object",
                'code': 'validation.required_dict',
                'where': 'request.body.inputs.0',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
        }],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs.0.ref' is required",
                'code': 'validation.required',
                'where': 'request.body.inputs.0.ref',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
            'ref': 'de',
        }],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'inputs.0.ref' value invalid",
                'code': 'validation.invalid',
                'where': 'request.body.inputs.0.ref',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
            'ref': 'start_node.juan.0:work.task',
        }],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'comment' is required",
                'code': 'validation.required',
                'where': 'request.body.comment',
            },
        ],
    }

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'reject',
        'inputs': [{
            'ref': 'start_node.juan.0:work.task',
        }],
        'comment': [],
    }))

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'comment' must be a str",
                'code': 'validation.invalid',
                'where': 'request.body.comment',
            },
        ],
    }


def test_validation_approval(client, mocker, config):
    ''' the api for an approval '''
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    exc = ptr.execution

    juan.assigned_tasks.append(ptr)

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': 'approval_node',
        'response': 'accept',
        'comment': 'I like the previous work',
    }))

    assert res.status_code == 202

    # rabbit is called
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan',
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'accept',
                'value_caption': 'accept',
            },
            {
                'name': 'comment',
                'value': 'I like the previous work',
                'value_caption': 'I like the previous work',
            },
            {
                'name': 'inputs',
                'value': None,
                'value_caption': 'null',
            },
        ])],
    }


def test_validation_reject(client, mocker, config):
    ''' the api for a reject '''
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    exc = ptr.execution

    juan.assigned_tasks.append(ptr)

    res = client.post('/v1/pointer', headers={**{
        'Content-Type': 'application/json',
    }, **make_auth_header(juan)}, data=json.dumps({
        'execution_id': exc.id,
        'node_id': ptr.node_id,
        'response': 'reject',
        'comment': 'I dont like it',
        'inputs': [{
            'ref': 'start_node.juan.0:work.task',
        }],
    }))

    assert res.status_code == 202

    # rabbit is called
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan',
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I dont like it',
                'value_caption': 'I dont like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:work.task',
                }],
                'value_caption': '[{"ref": "start_node.juan.0:work.task"}]',
            },
        ])],
    }


def test_fetch_pointers(client, mongo, config):
    def make_node_reg(exec_id, process_id, node_id, started_at, finished_at):
        return {
            'started_at': started_at,
            'finished_at': finished_at,
            'execution': {
                'id': exec_id,
            },
            'node': {
                'id': node_id,
            },
            'process_id': process_id,
        }

    ptr_01 = make_node_reg(
        'aaaaaaaa',
        'simple.2018-02-19', 'mid_node',
        make_date(2018, 5, 20, 5, 5, 5),
        make_date(2018, 5, 20, 5, 5, 5),
    )
    ptr_02 = make_node_reg(
        'aaaaaaaa',
        'simple.2018-02-19', 'mid_node',
        make_date(2018, 5, 20, 5, 5, 5),
        make_date(2018, 5, 20, 5, 5, 5),
    )
    ptr_03 = make_node_reg(
        'cccccccc',
        'simple.2018-02-19', 'mid_node',
        make_date(2018, 5, 22, 7, 7, 7),
        make_date(2018, 5, 22, 7, 7, 7),
    )
    ptr_04 = make_node_reg(
        'dddddddd',
        'simple.2018-02-19', 'mid_node',
        make_date(2018, 5, 23, 8, 8, 8),
        make_date(2018, 5, 23, 8, 8, 8),
    )
    ptr_05 = make_node_reg(
        'eeeeeeee',
        'simple.2018-02-19', 'mid_node',
        make_date(2018, 5, 24, 9, 9, 9),
        make_date(2018, 5, 24, 9, 9, 9),
    )

    mongo[config["POINTER_COLLECTION"]].insert_many([
        ptr_01.copy(),
        ptr_02.copy(),
        ptr_03.copy(),
        ptr_04.copy(),
        ptr_05.copy(),
    ])

    # test simple request
    res_simple = client.post(
        '/v1/pointer/search',
        headers=make_auth_header(),
        json={},
    )
    data_simple = json.loads(res_simple.data)

    expected_pointers_simple = [
        ptr_05.copy(),
        ptr_04.copy(),
        ptr_03.copy(),
        ptr_02.copy(),
        ptr_01.copy(),
    ]
    for item in expected_pointers_simple:
        item['started_at'] = item['started_at'].isoformat() + '+00:00'
        item['finished_at'] = item['finished_at'].isoformat() + '+00:00'

    assert data_simple == {
        'total_count': 5,
        'items': expected_pointers_simple,
    }

    # test sorted
    res_sorted = client.post(
        '/v1/pointer/search',
        headers=make_auth_header(),
        json={
            'sort': 'execution.id,ASCENDING',
        },
    )
    data_sorted = json.loads(res_sorted.data)

    expected_pointers_sorted = [
        ptr_01.copy(),
        ptr_02.copy(),
        ptr_03.copy(),
        ptr_04.copy(),
        ptr_05.copy(),
    ]
    for item in expected_pointers_sorted:
        item['started_at'] = item['started_at'].isoformat() + '+00:00'
        item['finished_at'] = item['finished_at'].isoformat() + '+00:00'

    assert data_sorted == {
        'total_count': 5,
        'items': expected_pointers_sorted,
    }

    # test include
    res_include = client.post(
        '/v1/pointer/search',
        headers=make_auth_header(),
        json={
            'include': ['execution.id'],
        },
    )
    data_include = json.loads(res_include.data)

    temp_list = [
        ptr_05.copy(),
        ptr_04.copy(),
        ptr_03.copy(),
        ptr_02.copy(),
        ptr_01.copy(),
    ]
    expected_pointers_include = [
        {'execution': {'id': p['execution']['id']}} for p in temp_list
    ]

    assert data_include == {
        'total_count': 5,
        'items': expected_pointers_include,
    }

    # test limit
    res_limit = client.post(
        '/v1/pointer/search',
        headers=make_auth_header(),
        json={
            'limit': '1',
        },
    )
    data_limit = json.loads(res_limit.data)

    expected_pointers_limit = [
        ptr_05.copy(),
    ]
    for item in expected_pointers_limit:
        item['started_at'] = item['started_at'].isoformat() + '+00:00'
        item['finished_at'] = item['finished_at'].isoformat() + '+00:00'

    assert data_limit == {
        'total_count': 5,
        'items': expected_pointers_limit,
    }

    # test skip
    res_offset = client.post(
        '/v1/pointer/search',
        headers=make_auth_header(),
        json={
            'offset': '2',
        },
    )
    data_offset = json.loads(res_offset.data)

    expected_pointers_offset = [
        ptr_03.copy(),
        ptr_02.copy(),
        ptr_01.copy(),
    ]
    for item in expected_pointers_offset:
        item['started_at'] = item['started_at'].isoformat() + '+00:00'
        item['finished_at'] = item['finished_at'].isoformat() + '+00:00'

    assert data_offset == {
        'total_count': 5,
        'items': expected_pointers_offset,
    }
