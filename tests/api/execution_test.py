from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.jsontypes import (
    Map,
    SortedMap,
)
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

from flask import json

import tests.utils
from tests.utils import (
    assert_near_date,
    make_auth_header,
    make_pointer,
    make_user,
)


def test_start_process_requirements(client, mongo, config):
    # first requirement is to have authentication
    res = client.post('/v1/execution', headers={
        'Content-Type': 'application/json',
    }, data=json.dumps({
        'process_name': 'simple',
    }))

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'You must provide basic authorization headers',
            'where': 'request.authorization',
        }],
    }

    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).count() == 0

    # next, validate the form data
    res = client.post(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(),
        },
        data=json.dumps({
            'process_name': 'simple',
        }),
    )

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [{
            'detail': "form count lower than expected for ref start_form",
            'where': 'request.body.form_array',
        }],
    }

    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).count() == 0

    res = client.post(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(),
        },
        data='{}',
    )

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': "'process_name' is required",
                'where': 'request.body.process_name',
                'code': 'validation.required',
            },
        ],
    }

    # we need an existing process to start
    res = client.post(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(),
        },
        data=json.dumps({
            'process_name': 'foo',
        }),
    )

    assert res.status_code == 404
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'foo process does not exist',
                'where': 'request.body.process_name',
            },
        ],
    }

    # no registry should be created yet
    assert mongo[config["POINTER_COLLECTION"]].count_documents({}) == 0


def test_start_process(client, mocker, config, mongo):
    mocker.patch('cacahuate.tasks.handle.delay')

    process_name = 'simple'
    xml = Xml.load(config, process_name)

    juan = make_user('juan', 'Juan')

    res = client.post(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'process_name': process_name,
            'form_array': [{
                'ref': 'start_form',
                'data': {
                    'data': 'yes',
                },
            }],
        }),
    )

    assert res.status_code == 201

    exc = db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).first()

    assert exc.process_name == xml.filename

    ptr = exc.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()

    assert ptr.node_id == 'start_node'

    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    json_message = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': 'juan',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'type': 'text',
                'value': 'yes',
                'value_caption': 'yes',
                'name': 'data',
                'state': 'valid',
                'hidden': False,
            },
        ])],
    }

    assert json.loads(args) == json_message

    handler = Handler(config)

    pointer, user, inpt = handler.recover_step(json_message)

    assert pointer.id == ptr.id

    # mongo has a registry
    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert_near_date(reg['started_at'])
    assert reg['finished_at'] is None
    assert reg['execution']['id'] == exc.id
    assert reg['node']['id'] == ptr.node_id

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert_near_date(reg['started_at'])

    del reg['started_at']
    del reg['_id']

    assert len(reg['values']['_execution'][0]['id']) > 0
    assert len(reg['values']['_execution'][0]['started_at']) > 0
    del reg['values']['_execution'][0]['id']
    del reg['values']['_execution'][0]['started_at']

    assert reg == {
        '_type': 'execution',
        'id': exc.id,
        'name': exc.name,
        'description': exc.description,
        'status': 'ongoing',
        'finished_at': None,
        'process_name': xml.filename,
        'status': 'ongoing',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'start_node': xml.get_node('start_node').get_state(),
                'mid_node': xml.get_node('mid_node').get_state(),
                'final_node': xml.get_node('final_node').get_state(),
            },
            'item_order': [
                'start_node',
                'mid_node',
                'final_node',
            ],
        },
        'values': {
            '_execution': [{
                'description': 'A simple process that does nothing',
                'name': 'Simplest process ever started with: yes',
                'process_name': 'simple.2018-02-19.xml',
            }],
        },
        'actor_list': [],
    }


def test_patch_requirements(client, mongo, config):
    juan = make_user('juan', 'Juan')
    ptr = make_pointer('exit_request.2018-03-20.xml', 'requester')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })
    mongo[config['EXECUTION_COLLECTION']].update_one({
        'id': exc.id,
    }, {
        '$set': {
            'state.items.requester.actors': Map([{
                "_type": "actor",
                "forms": [{
                    '_type': 'form',
                    'state': 'valid',
                    'ref': 'exit_form',
                    'inputs': SortedMap([{
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'yes',
                        'name': 'reason',
                    }], key='name').to_json(),
                }],
                "state": "valid",
                "user": {
                    "_type": "user",
                    "identifier": "__system__",
                    "fullname": "System",
                },
            }], key=lambda a: a['user']['identifier']).to_json(),
        },
    })

    # 'inputs' key is required
    res = client.patch(
        '/v1/execution/{}'.format(exc.id),
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'comment': 'I dont like it',
        }),
    )

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [{
            'code': 'validation.required',
            'detail': '\'inputs\' is required',
            'where': 'request.body.inputs',
        }],
    }

    # all refs must exist
    res = client.patch(
        '/v1/execution/{}'.format(exc.id),
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'comment': 'I dont like it',
            'inputs': [{
                'ref': 'node_id.form_ref.input_name',
            }],
        }),
    )

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'node node_id not found',
                'code': 'validation.invalid',
                'where': 'request.body.inputs.0.ref',
            },
        ],
    }

    # ref must pass validation if value present
    res = client.patch(
        '/v1/execution/{}'.format(exc.id),
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'comment': 'I dont like it',
            'inputs': [{
                'ref': 'requester.exit_form.reason',
                'value': '',
            }],
        }),
    )

    assert res.status_code == 400
    assert json.loads(res.data) == {
        'errors': [
            {
                'detail': 'value invalid: \'reason\' is required',
                'where': 'request.body.inputs.0.value',
                'code': 'validation.invalid',
            },
        ],
    }


def test_patch_just_invalidate(client, mongo, config, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('exit_request.2018-03-20.xml', 'requester')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })
    mongo[config['EXECUTION_COLLECTION']].update_one({
        'id': exc.id,
    }, {
        '$set': {
            'state.items.requester.actors': Map([{
                "_type": "actor",
                "forms": [{
                    '_type': 'form',
                    'state': 'valid',
                    'ref': 'exit_form',
                    'inputs': SortedMap([{
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'yes',
                        'name': 'reason',
                    }], key='name').to_json(),
                }],
                "state": "valid",
                "user": {
                    "_type": "user",
                    "identifier": "juan",
                    "fullname": "System",
                },
            }], key=lambda a: a['user']['identifier']).to_json(),
        },
    })

    res = client.patch(
        '/v1/execution/{}'.format(exc.id),
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'comment': 'a comment',
            'inputs': [{
                'ref': 'requester.exit_form.reason',
            }],
        }),
    )

    assert res.status_code == 202

    # message is queued
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    json_message = {
        'command': 'patch',
        'execution_id': exc.id,
        'comment': 'a comment',
        'inputs': [{
            'ref': 'requester.juan.0:exit_form.reason',
        }],
        'user_identifier': 'juan',
    }

    body = json.loads(args)
    assert body == json_message


def test_patch_just_invalidate_multiple(client, mongo, config, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('exit_request.2018-03-20.xml', 'requester')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })
    mongo[config['EXECUTION_COLLECTION']].update_one({
        'id': exc.id,
    }, {
        '$set': {
            'state.items.requester.actors': Map([{
                "_type": "actor",
                "forms": [{
                    '_type': 'form',
                    'state': 'valid',
                    'ref': 'exit_form',
                    'inputs': SortedMap([{
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'yes',
                        'name': 'reason',
                    }], key='name').to_json(),
                }, {
                    '_type': 'form',
                    'state': 'valid',
                    'ref': 'comment_form',
                    'inputs': SortedMap([{
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'first comment',
                        'name': 'comment',
                    }], key='name').to_json(),
                }, {
                    '_type': 'form',
                    'state': 'valid',
                    'ref': 'comment_form',
                    'inputs': SortedMap([{
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'second comment',
                        'name': 'comment',
                    }], key='name').to_json(),
                }],
                "state": "valid",
                "user": {
                    "_type": "user",
                    "identifier": "juan",
                    "fullname": "System",
                },
            }], key=lambda a: a['user']['identifier']).to_json(),
        },
    })

    res = client.patch(
        '/v1/execution/{}'.format(exc.id),
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'comment': 'a comment',
            'inputs': [{
                'ref': 'requester.comment_form.1.comment',
            }],
        }),
    )

    assert res.status_code == 202

    # message is queued
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    json_message = {
        'command': 'patch',
        'execution_id': exc.id,
        'comment': 'a comment',
        'inputs': [{
            'ref': 'requester.juan.2:comment_form.comment',
        }],
        'user_identifier': 'juan',
    }

    body = json.loads(args)
    assert body == json_message


def test_patch_set_value(client, mongo, config, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')
    ptr = make_pointer('exit_request.2018-03-20.xml', 'requester')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })
    mongo[config['EXECUTION_COLLECTION']].update_one({
        'id': exc.id,
    }, {
        '$set': {
            'state.items.requester.actors': Map([{
                "_type": "actor",
                "forms": [
                    {
                        '_type': 'form',
                        'state': 'valid',
                        'ref': 'exit_form',
                        'inputs': SortedMap([{
                            '_type': 'field',
                            'state': 'valid',
                            'value': 'want to pee',
                            'name': 'reason',
                        }], key='name').to_json(),
                    },
                    {
                        '_type': 'form',
                        'state': 'valid',
                        'ref': 'code_form',
                        'inputs': SortedMap([{
                            '_type': 'field',
                            'state': 'valid',
                            'value': 'kadabra',
                            'name': 'code',
                        }], key='name').to_json(),
                    },
                ],
                "state": "valid",
                "user": {
                    "_type": "user",
                    "identifier": "juan",
                    "fullname": "System",
                },
            }], key=lambda a: a['user']['identifier']).to_json(),
        },
    })

    res = client.patch(
        '/v1/execution/{}'.format(exc.id),
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'comment': 'pee is not a valid reason',
            'inputs': [
                {
                    'ref': 'requester.exit_form.reason',
                    'value': 'am hungry',
                },
                {
                    'ref': 'requester.code_form.code',
                    'value': 'alakazam',
                },
            ],
        }),
    )

    assert res.status_code == 202

    # message is queued
    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    json_message = {
        'command': 'patch',
        'execution_id': exc.id,
        'comment': 'pee is not a valid reason',
        'inputs': [
            {
                'ref': 'requester.juan.0:exit_form.reason',
                'value': 'am hungry',
                'value_caption': 'am hungry',
            },
            {
                'ref': 'requester.juan.1:code_form.code',
                'value': 'alakazam',
                'value_caption': 'alakazam',
            },
        ],
        'user_identifier': 'juan',
    }

    body = json.loads(args)
    assert body == json_message


def test_execution_has_node_info(client, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    juan = make_user('juan', 'Juan')

    res = client.post(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data=json.dumps({
            'process_name': 'simple',
            'form_array': [
                {
                    'ref': 'start_form',
                    'data': {
                        'data': 'yes',
                    },
                },
            ],
        }),
    )

    assert res.status_code == 201

    exe = db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).first()
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()

    assert exe.name == 'Simplest process ever started with: yes'
    assert exe.description == 'A simple process that does nothing'

    assert ptr.name == 'Primer paso'
    assert ptr.description == 'Resolver una tarea'


def test_log_has_node_info(client, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    res = client.post(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(),
        },
        data=json.dumps({
            'process_name': 'simple',
            'form_array': [
                {
                    'ref': 'start_form',
                    'data': {
                        'data': 'yes',
                    },
                },
            ],
        }),
    )

    assert res.status_code == 201

    body = json.loads(res.data)
    execution_id = body['data']['id']

    res = client.get(
        '/v1/log/{}'.format(execution_id),
        headers=make_auth_header(),
    )
    body = json.loads(res.data)
    data = body['data'][0]

    assert data['node']['id'] == 'start_node'
    assert data['node']['name'] == 'Primer paso'
    assert data['node']['description'] == 'Resolver una tarea'

    assert data['execution']['id'] == execution_id
    assert data['execution']['name'] == (
        'Simplest process ever started with: yes'
    )
    assert data['execution']['description'] == (
        'A simple process that does nothing'
    )


def test_delete_process(config, client, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    p_0 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    execution = p_0.execution
    user = tests.utils.make_user(
        tests.utils.random_string(),
        tests.utils.random_string(),
    )

    res = client.delete(
        '/v1/execution/{}'.format(execution.id),
        headers=make_auth_header(user),
    )

    assert res.status_code == 202

    handle.delay.assert_called_once()

    args = handle.delay.call_args[0][0]

    assert json.loads(args) == {
        'execution_id': execution.id,
        'command': 'cancel',
        'user_identifier': user.identifier,
    }


def test_status_notfound(client):
    res = client.get(
        '/v1/execution/doo',
        headers=make_auth_header(),
    )

    assert res.status_code == 404


def test_status(config, client, mongo):
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')
    execution = ptr.execution

    mongo[config['EXECUTION_COLLECTION']].insert_one({
        'id': execution.id,
    })

    res = client.get(
        '/v1/execution/{}'.format(execution.id),
        headers=make_auth_header(),
    )

    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': {
            'id': execution.id,
        },
    }


def test_add_user(client, mocker, config, mongo):
    # variables: users
    juan = make_user('juan', 'Juan')
    luis = make_user('luis', 'Luis')

    # variables: pointer and execution
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'id': ptr.id,
        'node': {
            'id': ptr.node_id,
        },
        'notified_users': [],
        'execution': exc.as_json(),
        'state': 'ongoing',
    })

    # user has no task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # add the user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header()},
        data=json.dumps({
            'identifier': 'luis',
            'node_id': 'approval_node',
        }),
    )
    # successful post
    assert res.status_code == 200

    # user has one task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    # test notified_users (log)
    res = client.get(
        '/v1/log/{}'.format(exc.id),
        headers=make_auth_header(juan),
    )

    notified_users = json.loads(res.data)['data'][0]['notified_users']
    assert res.status_code == 200
    assert notified_users == [luis.as_json()]


def test_add_user_new(client, mocker, config, mongo):
    # variables: users
    juan = make_user('juan', 'Juan')
    luis = make_user('luis', 'Luis')
    beto = make_user('beto', 'Beto')

    # variables: pointer and execution
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'id': ptr.id,
        'node': {
            'id': ptr.node_id,
        },
        'notified_users': [],
        'execution': exc.as_json(),
        'state': 'ongoing',
    })

    # user has no task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0
    assert beto.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # add the user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header(juan)},
        data=json.dumps({
            'identifier': 'luis',
            'node_id': 'approval_node',
        }),
    )
    # successful post
    assert res.status_code == 200

    # user has one task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    # test notified_users (log)
    res = client.get(
        '/v1/log/{}'.format(exc.id),
        headers=make_auth_header(juan),
    )

    notified_users = json.loads(res.data)['data'][0]['notified_users']
    assert res.status_code == 200
    assert notified_users == [luis.as_json()]

    # add the second user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header(juan)},
        data=json.dumps({
            'identifier': 'beto',
            'node_id': 'approval_node',
        }),
    )
    # successful post
    assert res.status_code == 200

    # user has one task assigned
    assert beto.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    # test notified_users (log)
    res = client.get(
        '/v1/log/{}'.format(exc.id),
        headers=make_auth_header(juan),
    )

    notified_users = json.loads(res.data)['data'][0]['notified_users']
    assert res.status_code == 200
    assert notified_users == [luis.as_json(), beto.as_json()]


def test_add_user_duplicate(client, mocker, config, mongo):
    # variables: users
    juan = make_user('juan', 'Juan')
    luis = make_user('luis', 'Luis')

    # variables: pointer and execution
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'id': ptr.id,
        'node': {
            'id': ptr.node_id,
        },
        'notified_users': [],
        'execution': exc.as_json(),
        'state': 'ongoing',
    })

    # user has no task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # add the user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header(juan)},
        data=json.dumps({
            'identifier': 'luis',
            'node_id': 'approval_node',
        }),
    )
    # successful post
    assert res.status_code == 200

    # user has one task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    # test notified_users (log)
    res = client.get(
        '/v1/log/{}'.format(exc.id),
        headers=make_auth_header(juan),
    )

    notified_users = json.loads(res.data)['data'][0]['notified_users']
    assert res.status_code == 200
    assert notified_users == [luis.as_json()]

    # add the second user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header(juan)},
        data=json.dumps({
            'identifier': 'luis',
            'node_id': 'approval_node',
        }),
    )
    # successful post
    assert res.status_code == 200

    # user has one task assigned
    assert luis.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    # test notified_users (log)
    res = client.get(
        '/v1/log/{}'.format(exc.id),
        headers=make_auth_header(juan),
    )

    notified_users = json.loads(res.data)['data'][0]['notified_users']
    assert res.status_code == 200
    assert notified_users == [luis.as_json()]


def test_add_user_requirements_id(client, mocker, config, mongo):
    juan = make_user('juan', 'Juan')

    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })

    # try add the user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header(juan)},
        data=json.dumps({
            'identifier': 'luis',
            'node_id': 'approval_node',
        }),
    )

    # post requires valid user id
    assert res.status_code == 400


def test_add_user_requirements_node(client, mocker, config, mongo):
    juan = make_user('juan', 'Juan')
    make_user('luis', 'Luis')

    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')
    exc = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name, direct=True).get_state(),
    })

    # try add the user
    res = client.post(
        '/v1/execution/{}/user'.format(exc.id),
        headers={
            **{'Content-Type': 'application/json'},
            **make_auth_header(juan)},
        data=json.dumps({
            'identifier': 'luis',
            'node_id': 'final_node',
        }),
    )
    # post requires valid living node
    assert res.status_code == 400


def test_start_process_error_405(client, mongo, config):
    juan = make_user('juan', 'Juan')

    res = client.put(
        '/v1/execution',
        headers={
            **{
                'Content-Type': 'application/json',
            },
            **make_auth_header(juan),
        },
        data='{}',
    )

    data = json.loads(res.data)
    assert res.status_code == 405
    assert data['errors'][0]['detail'] == (
        "The method is not allowed for the requested URL."
    )
