from flask import json

from tests.utils import (
    make_auth_header,
    make_date,
)

EXECUTION_ID = '15asbs'


def test_list_processes(client):
    res = client.get(
        '/v1/process',
        headers=make_auth_header(),
    )

    body = json.loads(res.data)
    exit_req = list(filter(
        lambda xml: xml['id'] == 'simple', body['data'],
    ))[0]

    assert res.status_code == 200
    assert exit_req == {
        'id': 'simple',
        'version': '2018-02-19',
        'author': 'categulario',
        'date': '2018-02-19',
        'name': 'Simplest process ever',
        'description': 'A simple process that does nothing',
        'versions': ['2018-02-19'],
        'form_array': [
            {
                'ref': 'start_form',
                'inputs': [
                    {
                        'type': 'text',
                        'name': 'data',
                        'required': True,
                        'label': 'Info',
                    },
                ],
            },
        ],
    }


def test_list_processes_multiple(client):
    res = client.get(
        '/v1/process',
        headers=make_auth_header(),
    )

    body = json.loads(res.data)
    exit_req = list(filter(
        lambda xml: xml['id'] == 'form-multiple', body['data'],
    ))[0]

    assert res.status_code == 200
    assert exit_req == {
        'id': 'form-multiple',
        'version': '2018-04-08',
        'author': 'categulario',
        'date': '2018-04-08',
        'name': 'Con un formulario múltiple',
        'description':
            'Este proceso tiene un formulario que puede enviar muchas copias',
        'versions': ['2018-04-08'],
        'form_array': [
            {
                'ref': 'single_form',
                'inputs': [
                    {
                        'type': 'text',
                        'name': 'name',
                        'required': True,
                        'label': 'Single Form',
                    },
                ],
            },
            {
                'ref': 'multiple_form',
                'multiple': '1-10',
                'inputs': [
                    {
                        'type': 'text',
                        'name': 'phone',
                        'required': True,
                        'label': 'Multi Form',
                    },
                ],
            },
        ],
    }


def test_read_process(client):
    res = client.get(
        '/v1/process/oldest?version=2018-02-14',
        headers=make_auth_header(),
    )
    data = json.loads(res.data)
    assert res.status_code == 200
    assert data['data']['name'] == 'Oldest process'
    assert data['data']['version'] == '2018-02-14'

    res = client.get(
        '/v1/process/oldest',
        headers=make_auth_header(),
    )
    data = json.loads(res.data)
    assert res.status_code == 200
    assert data['data']['name'] == 'Oldest process v2'
    assert data['data']['version'] == '2018-02-17'

    res = client.get(
        '/v1/process/prueba',
        headers=make_auth_header(),
    )
    data = json.loads(res.data)
    assert res.status_code == 404
    assert data['errors'][0]['detail'] == 'prueba process does not exist'


def test_node_statistics(client, mongo, config):
    def make_node_reg(process_id, node_id, started_at, finished_at):
        return {
            'started_at': started_at,
            'finished_at': finished_at,
            'execution': {
                'id': EXECUTION_ID,
            },
            'node': {
                'id': node_id,
            },
            'process_id': process_id,
        }

    mongo[config["POINTER_COLLECTION"]].insert_many([
        make_node_reg(
            'simple.2018-02-19', 'test1',
            make_date(),
            make_date(2018, 5, 10, 4, 5, 6),
        ),
        make_node_reg(
            'simple.2018-02-19', 'test2',
            make_date(),
            make_date(2018, 5, 10, 6, 3, 3),
        ),
        make_node_reg(
            'simple.2018-02-19', 'test1',
            make_date(),
            make_date(2018, 5, 10, 8, 2, 9),
        ),
        make_node_reg(
            'simple.2018-02-19', 'test2',
            make_date(),
            make_date(2018, 5, 10, 3, 4, 5),
        ),
        make_node_reg(
            'simple.2018-02-19',
            'test2',
            make_date(),
            None,
        ),
    ])

    res = client.get(
        '/v1/process/{}/statistics'.format(
            'simple.2018-02-19',
        ),
        headers=make_auth_header(),
    )

    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': [
            {
                'average': 540217.5,
                'max': 547329.0,
                'min': 533106.0,
                'node': 'test1',
                'process_id': 'simple.2018-02-19',
            },
            {
                'average': 534814.0,
                'max': 540183.0,
                'min': 529445.0,
                'node': 'test2',
                'process_id': 'simple.2018-02-19',
            },
        ],
    }


def test_process_statistics(client, mongo, config):
    def make_exec_reg(process_id, started_at, finished_at):
        return {
            'started_at': started_at,
            'finished_at': finished_at,
            'status': 'finished',
            'process': {
                'id': process_id,
                'version': 'v1',
            },
        }

    mongo[config["EXECUTION_COLLECTION"]].insert_many([
        make_exec_reg('p1', make_date(), make_date(2018, 5, 10, 4, 5, 6)),
        make_exec_reg('p2', make_date(), make_date(2018, 5, 10, 10, 34, 32)),
        make_exec_reg('p1', make_date(), make_date(2018, 5, 11, 22, 41, 10)),
        make_exec_reg('p2', make_date(), make_date(2018, 6, 23, 8, 15, 1)),
    ])

    res = client.get(
        '/v1/process/statistics',
        headers=make_auth_header(),
    )

    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': [
            {
                'average': 609788.0,
                'max': 686470.0,
                'min': 533106.0,
                'process': 'p1',
            },
            {
                'average': 2453086.5,
                'max': 4349701.0,
                'min': 556472.0,
                'process': 'p2',
            },

        ],
    }


def test_pagination_execution_log(client, mongo, config):
    def make_exec_reg(process_id, started_at, finished_at):
        return {
            'started_at': started_at,
            'finished_at': finished_at,
            'status': 'finished',
            'process': {
                'id': process_id,
                'version': 'v1',
            },
        }

    mongo[config["EXECUTION_COLLECTION"]].insert_many([
        make_exec_reg('p1', make_date(), make_date(2018, 5, 10, 4, 5, 6)),
        make_exec_reg('p2', make_date(), make_date(2018, 5, 10, 10, 34, 32)),
        make_exec_reg('p3', make_date(), make_date(2018, 5, 11, 22, 41, 10)),
        make_exec_reg('p4', make_date(), make_date(2018, 6, 23, 8, 15, 1)),
        make_exec_reg('p5', make_date(), make_date(2018, 6, 11, 4, 5, 6)),
        make_exec_reg('p6', make_date(), make_date(2018, 6, 12, 5, 6, 32)),
        make_exec_reg('p7', make_date(), make_date(2018, 6, 13, 6, 7, 10)),
        make_exec_reg('p8', make_date(), make_date(2018, 6, 14, 7, 8, 1)),
    ])

    res = client.get(
        '/v1/process/statistics?offset=2&limit=2',
        headers=make_auth_header(),
    )
    assert res.status_code == 200
    assert json.loads(res.data)['data'][0]["process"] == 'p3'
    assert json.loads(res.data)['data'][1]["process"] == 'p4'
    assert len(json.loads(res.data)['data']) == 2


def test_get_xml(client):
    res = client.get(
        '/v1/process/validation-multiform.xml',
        headers=make_auth_header(),
    )
    assert res.status_code == 200
    assert res.headers['Content-Type'] == 'text/xml; charset=utf-8'
    assert res.data.startswith(b'<?xml version="1.0" encoding="UTF-8"?>')
