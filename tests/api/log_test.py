from datetime import datetime

from cacahuate.database import db_session

from flask import json

from tests.utils import (
    make_auth_header,
    make_date,
    make_pointer,
    make_user,
)

EXECUTION_ID = '15asbs'


def test_logs_all(mongo, client, config):
    mongo[config["POINTER_COLLECTION"]].insert_many([
        {
            'started_at': datetime(2018, 4, 1, 21, 45),
            'finished_at': None,
            'execution': {
                'id': EXECUTION_ID,
            },
            'node': {
                'id': 'first_node',
            },
        },
        {
            'started_at': datetime(2018, 4, 1, 21, 46),
            'finished_at': None,
            'execution': {
                'id': EXECUTION_ID,
            },
            'node': {
                'id': 'mid_node',
            },
        },
        {
            'started_at': datetime(2018, 4, 1, 21, 45),
            'finished_at': None,
            'execution': {
                'id': 'xxxxffff',
            },
            'node': {
                'id': 'mid_node',
            },
        },
        {
            'started_at': datetime(2018, 4, 1, 21, 44),
            'finished_at': None,
            'execution': {
                'id': EXECUTION_ID,
            },
            'node': {
                'id': 'another_node',
            },
        },
    ])

    res = client.get(
        '/v1/log',
        headers=make_auth_header(),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [
            {
                'started_at': '2018-04-01T21:46:00+00:00',
                'finished_at': None,
                'execution': {
                    'id': EXECUTION_ID,
                },
                'node': {
                    'id': 'mid_node',
                },
            },
            {
                'started_at': '2018-04-01T21:45:00+00:00',
                'finished_at': None,
                'execution': {
                    'id': 'xxxxffff',
                },
                'node': {
                    'id': 'mid_node',
                },
            },
        ],
    }


def test_logs_filter_user(mongo, client, config):
    juan = make_user('user', 'User')

    ptr_01 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    ptr_02 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    ptr_03 = make_pointer('exit_request.2018-03-20.xml', 'requester')
    ptr_04 = make_pointer('validation.2018-05-09.xml', 'approval_node')

    juan.assigned_tasks.extend([ptr_01, ptr_02, ptr_04])

    db_session.commit()

    ptr_01_json = {**ptr_01.as_json(), 'execution': ptr_01.execution.as_json()}
    ptr_02_json = {**ptr_02.as_json(), 'execution': ptr_02.execution.as_json()}
    ptr_03_json = {**ptr_03.as_json(), 'execution': ptr_03.execution.as_json()}
    ptr_04_json = {**ptr_04.as_json(), 'execution': ptr_04.execution.as_json()}

    # set started_at to ptrs
    ptr_01_json['started_at'] = '2018-04-01T21:45:00+00:00'
    ptr_02_json['started_at'] = '2018-04-01T21:46:00+00:00'
    ptr_04_json['started_at'] = '2018-04-01T21:48:00+00:00'

    mongo[config["POINTER_COLLECTION"]].insert_many([
        ptr_01_json.copy(),
        ptr_02_json.copy(),
        ptr_03_json.copy(),
        ptr_04_json.copy(),
    ])

    res = client.get(
        '/v1/log?user_identifier={}'.format(juan.identifier),
        headers=make_auth_header(juan),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [
            ptr_04_json,
            ptr_02_json,
            ptr_01_json,
        ],
    }


def test_logs_filter_user_invalid(mongo, client, config):
    ptr_01 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    ptr_02 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    ptr_03 = make_pointer('exit_request.2018-03-20.xml', 'requester')
    ptr_04 = make_pointer('validation.2018-05-09.xml', 'approval_node')

    mongo[config["POINTER_COLLECTION"]].insert_many([
        {**ptr_01.as_json(), 'execution': ptr_01.execution.as_json()},
        {**ptr_02.as_json(), 'execution': ptr_02.execution.as_json()},
        {**ptr_03.as_json(), 'execution': ptr_03.execution.as_json()},
        {**ptr_04.as_json(), 'execution': ptr_04.execution.as_json()},
    ])

    res = client.get(
        '/v1/log?user_identifier=foo',
        headers=make_auth_header(),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [],
    }


def test_logs_filter_key_valid(mongo, client, config):
    mongo[config["POINTER_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': EXECUTION_ID,
        },
        'node': {
            'id': 'mid_node',
        },
        'one_key': 'foo',
    })

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 50),
        'finished_at': None,
        'execution': {
            'id': EXECUTION_ID,
        },
        'node': {
            'id': '4g9lOdPKmRUf2',
        },
        'one_key': 'bar',
    })

    res = client.get(
        '/v1/log?one_key=foo',
        headers=make_auth_header(),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [
            {
                'started_at': '2018-04-01T21:45:00+00:00',
                'finished_at': None,
                'execution': {
                    'id': EXECUTION_ID,
                },
                'node': {
                    'id': 'mid_node',
                },
                'one_key': 'foo',
            },
        ],
    }


def test_logs_filter_key_invalid(mongo, client, config):
    mongo[config["POINTER_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': EXECUTION_ID,
        },
        'node': {
            'id': 'mid_node',
        },
    })

    res = client.get(
        '/v1/log?limit=foo',
        headers=make_auth_header(),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [
            {
                'started_at': '2018-04-01T21:45:00+00:00',
                'finished_at': None,
                'execution': {
                    'id': EXECUTION_ID,
                },
                'node': {
                    'id': 'mid_node',
                },
            },
        ],
    }


def test_logs_filter_value_invalid(mongo, client, config):
    mongo[config["POINTER_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': EXECUTION_ID,
        },
        'node': {
            'id': 'mid_node',
        },
        'one_key': 'bar',
    })

    res = client.get(
        '/v1/log?one_key=foo',
        headers=make_auth_header(),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [],
    }


def test_logs_activity(mongo, client, config):
    mongo[config["POINTER_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': EXECUTION_ID,
        },
        'node': {
            'id': 'mid_node',
        },
    })

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 50),
        'finished_at': None,
        'execution': {
            'id': EXECUTION_ID,
        },
        'node': {
            'id': '4g9lOdPKmRUf2',
        },
    })

    res = client.get(
        '/v1/log/{}?node_id=mid_node'.format(EXECUTION_ID),
        headers=make_auth_header(),
    )

    ans = json.loads(res.data)

    assert res.status_code == 200
    assert ans == {
        "data": [{
            'started_at': '2018-04-01T21:45:00+00:00',
            'finished_at': None,
            'execution': {
                'id': EXECUTION_ID,
            },
            'node': {
                'id': 'mid_node',
            },
        }],
    }


def test_pagination_v1_log(client, mongo, config):
    def make_node_reg(process_id, node_id, started_at, finished_at):
        return {
            'started_at': started_at,
            'finished_at': finished_at,
            'execution': {
                'id': EXECUTION_ID,
            },
            'node': {
                'id': node_id,
            },
            'process_id': process_id,
        }

    mongo[config["POINTER_COLLECTION"]].insert_many([
        make_node_reg(
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 20, 5, 5, 5),
            make_date(2018, 5, 20, 5, 5, 5),
        ),
        make_node_reg(
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 21, 6, 6, 6),
            make_date(2018, 5, 21, 6, 6, 6),
        ),
        make_node_reg(
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 22, 7, 7, 7),
            make_date(2018, 5, 22, 7, 7, 7),
        ),
        make_node_reg(
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 23, 8, 8, 8),
            make_date(2018, 5, 23, 8, 8, 8),
        ),
        make_node_reg(
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 24, 9, 9, 9),
            make_date(2018, 5, 24, 9, 9, 9),
        ),
    ])

    res = client.get(
        '/v1/log/{}?node_id=mid_node&offset=2&limit=2'.format(EXECUTION_ID),
        headers=make_auth_header(),
    )
    assert json.loads(res.data)['data'][0]["finished_at"] == (
        '2018-05-22T07:07:07+00:00'
    )
    assert json.loads(res.data)['data'][1]["finished_at"] == (
        '2018-05-21T06:06:06+00:00'
    )
    assert len(json.loads(res.data)['data']) == 2


def test_pagination_v1_log_all(client, mongo, config):
    def make_node_reg(exec_id, process_id, node_id, started_at, finished_at):
        return {
            'started_at': started_at,
            'finished_at': finished_at,
            'execution': {
                'id': exec_id,
            },
            'node': {
                'id': node_id,
            },
            'process_id': process_id,
        }

    mongo[config["POINTER_COLLECTION"]].insert_many([
        make_node_reg(
            'aaaaaaaa',
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 20, 5, 5, 5),
            make_date(2018, 5, 20, 5, 5, 5),
        ),
        make_node_reg(
            'bbbbbbbb',
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 21, 6, 6, 6),
            make_date(2018, 5, 21, 6, 6, 6),
        ),
        make_node_reg(
            'cccccccc',
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 22, 7, 7, 7),
            make_date(2018, 5, 22, 7, 7, 7),
        ),
        make_node_reg(
            'dddddddd',
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 23, 8, 8, 8),
            make_date(2018, 5, 23, 8, 8, 8),
        ),
        make_node_reg(
            'eeeeeeee',
            'simple.2018-02-19', 'mid_node',
            make_date(2018, 5, 24, 9, 9, 9),
            make_date(2018, 5, 24, 9, 9, 9),
        ),
    ])

    res = client.get(
        '/v1/log?offset=2&limit=2',
        headers=make_auth_header(),
    )
    assert json.loads(res.data)['data'][0]["started_at"] == (
        '2018-05-22T07:07:07+00:00'
    )
    assert json.loads(res.data)['data'][1]["started_at"] == (
        '2018-05-21T06:06:06+00:00'
    )
    assert len(json.loads(res.data)['data']) == 2
