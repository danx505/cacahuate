from cacahuate.database import db_session
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

from flask import json

from tests.utils import (
    make_auth_header,
    make_date,
    make_pointer,
    make_user,
)


def test_task_list_requires_auth(client):
    res = client.get('/v1/task')

    assert res.status_code == 401
    assert json.loads(res.data) == {
        'errors': [{
            'detail': 'You must provide basic authorization headers',
            'where': 'request.authorization',
        }],
    }


def test_task_list(client):
    juan = make_user('user', 'User')

    pointer = make_pointer('simple.2018-02-19.xml', 'mid_node')
    juan.assigned_tasks.extend([pointer])

    db_session.commit()

    res = client.get('/v1/task', headers=make_auth_header(juan))

    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': [{
            **pointer.as_json(),
            'execution': pointer.execution.as_json(),
        }],
    }


def test_task_read_requires(client):
    # auth
    res = client.get('/v1/task/foo')

    assert res.status_code == 401

    # real pointer
    res = client.get('/v1/task/foo', headers=make_auth_header())

    assert res.status_code == 404

    # assigned task
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    res = client.get('/v1/task/{}'.format(ptr.id), headers=make_auth_header())

    assert res.status_code == 403


def test_task_read(client, config, mongo):
    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')
    juan = make_user('juan', 'Juan')

    juan.assigned_tasks.extend([ptr])

    db_session.commit()

    execution = ptr.execution

    execution.started_at = make_date(2020, 8, 21, 4, 5, 6)
    execution.status = 'ongoing'

    db_session.commit()

    state = Xml.load(config, execution.process_name).get_state()

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': state,
    })

    res = client.get(
        '/v1/task/{}'.format(ptr.id),
        headers=make_auth_header(juan),
    )

    assert res.status_code == 200
    assert json.loads(res.data) == {
        'data': {
            **ptr.as_json(),
            'execution': ptr.execution.as_json(),
            'node_type': 'action',
            'form_array': [
                {
                    'ref': 'mid_form',
                    'inputs': [
                        {
                            'name': 'data',
                            'required': True,
                            'type': 'text',
                            'label': 'data',
                        },
                    ],
                },
            ],
        },
    }


def test_task_validation(client, mongo, config):
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')
    juan = make_user('juan', 'Juan')

    juan.assigned_tasks.append(ptr)

    execution = ptr.execution

    state = Xml.load(config, execution.process_name).get_state()
    node = state['items']['start_node']

    node['state'] = 'valid'
    node['actors']['items']['juan'] = {
        '_type': 'actor',
        'state': 'valid',
        'user': {
            '_type': 'user',
            'identifier': 'juan',
            'fullname': None,
        },
        'forms': [Form.state_json('work', [
            {
                '_type': 'field',
                'state': 'valid',
                'label': 'task',
                'name': 'task',
                'value': 'Get some milk and eggs',
            },
        ])],
    }

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': state,
    })

    res = client.get(
        '/v1/task/{}'.format(ptr.id),
        headers=make_auth_header(juan),
    )
    body = json.loads(res.data)['data']

    assert res.status_code == 200
    assert body == {
        **ptr.as_json(),
        'execution': execution.as_json(),
        'node_type': 'validation',
        'form_array': [],
        'fields': [
            {
                '_type': 'field',
                'ref': 'start_node.juan.0:work.task',
                'label': 'task',
                'name': 'task',
                'value': 'Get some milk and eggs',
            },
        ],
    }


def test_task_with_prev_work(client, config, mongo):
    ptr = make_pointer('validation-multiform.2018-05-22.xml', 'start_node')
    juan = make_user('juan', 'Juan')

    juan.assigned_tasks.append(ptr)

    execution = ptr.execution

    state = Xml.load(config, execution.process_name).get_state()
    node = state['items']['start_node']

    prev_work = [Form.state_json('set', [
        {'_type': 'field', 'name': 'A', 'value': 'a1', 'state': 'valid'},
        {'_type': 'field', 'name': 'B', 'value': 'b1', 'state': 'valid'},
        {'_type': 'field', 'name': 'C', 'value': 'c1', 'state': 'invalid'},
        {'_type': 'field', 'name': 'D', 'value': 'd1', 'state': 'valid'},
    ]), Form.state_json('set', [
        {'_type': 'field', 'name': 'A', 'value': 'a2', 'state': 'valid'},
        {'_type': 'field', 'name': 'B', 'value': 'b2', 'state': 'valid'},
        {'_type': 'field', 'name': 'C', 'value': 'c2', 'state': 'valid'},
        {'_type': 'field', 'name': 'D', 'value': 'd2', 'state': 'valid'},
    ])]

    node['state'] = 'valid'
    node['actors']['items']['juan'] = {
        '_type': 'actor',
        'state': 'valid',
        'user': {
            '_type': 'user',
            'identifier': 'juan',
            'fullname': None,
        },
        'forms': prev_work,
    }

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': state,
    })

    res = client.get(
        '/v1/task/{}'.format(ptr.id),
        headers=make_auth_header(juan),
    )
    body = json.loads(res.data)['data']

    assert res.status_code == 200
    assert body == {
        **ptr.as_json(),
        'execution': execution.as_json(),
        'node_type': 'action',
        'form_array': [{
            'inputs': [
                {'label': 'Value A', 'name': 'A', 'type': 'text'},
                {'label': 'Value B', 'name': 'B', 'type': 'text'},
                {'label': 'Value C', 'name': 'C', 'type': 'text'},
                {'label': 'Value D', 'name': 'D', 'type': 'text'},
            ],
            'multiple': '1-5',
            'ref': 'set',
        }],
        'prev_work': node['actors']['items']['juan']['forms'],
    }
