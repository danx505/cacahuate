from datetime import datetime

from cacahuate.database import db_session
from cacahuate.errors import (
    InconsistentState,
    MisconfiguredProvider,
)
from cacahuate.handler import Handler
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.xml import Xml
from cacahuate.xml.node import (
    Action,
    Form,
)

from pika.adapters.blocking_connection import BlockingChannel

import pytest

import simplejson as json

import tests.utils
from tests.utils import (
    assert_near_date,
    make_date,
    make_pointer,
    make_user,
    random_string,
)


def test_recover_step(config):
    handler = Handler(config)

    ptr = make_pointer('simple.2018-02-19.xml', 'mid_node')

    manager = make_user('juan_manager', 'Manager')

    pointer, user, inpt = handler.recover_step({
          'command': 'step',
          'pointer_id': ptr.id,
          'user_identifier': 'juan_manager',
          'input': [[
              'auth_form',
              [{
                  'auth': 'yes',
              }],
          ]],
      })

    assert pointer.id == pointer.id
    assert user.id == manager.id


def test_create_pointer(config):
    handler = Handler(config)

    xml = Xml.load(config, 'simple')
    xmliter = iter(xml)

    node = Action(next(xmliter), xmliter)

    exc = Execution(
        process_name='simple.2018-02-19.xml',
        name='nombre',
        name_template='nombre',
        description='description',
        description_template='description',
        started_at=make_date(2020, 8, 21, 4, 5, 6),
        status='ongoing',
    )
    pointer = handler._create_pointer(
        node_id=node.id,
        node_type='action',
        name='name',
        description='description',
        execution=exc,
    )
    execution = pointer.execution

    assert pointer.node_id == 'start_node'

    assert execution.process_name == 'simple.2018-02-19.xml'
    assert execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).count() == 1


def test_wakeup(config, mongo, mocker):
    ''' the first stage in a node's lifecycle '''
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.exchange_declare'
    ))
    # setup stuff
    handler = Handler(config)

    pointer = make_pointer('simple.2018-02-19.xml', 'start_node')
    execution = pointer.execution
    juan = tests.utils.make_user('juan', 'Juan')
    manager = tests.utils.make_user(
        'juan_manager',
        'Juan Manager',
        'hardcoded@mailinator.com',
    )

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
    })

    # will wakeup the second node
    handler.step({
        'command': 'step',
        'pointer_id': pointer.id,
        'user_identifier': juan.identifier,
        'input': [],
    })

    # test manager is notified
    BlockingChannel.basic_publish.assert_called_once()
    BlockingChannel.exchange_declare.assert_called_once()

    args = BlockingChannel.basic_publish.call_args[1]

    assert args['exchange'] == config['RABBIT_NOTIFY_EXCHANGE']
    assert args['routing_key'] == 'email'
    assert json.loads(args['body']) == {
        'recipient': 'hardcoded@mailinator.com',
        'subject': '[procesos] Tarea asignada',
        'template': 'assigned-task.html',
        'data': {
            'pointer': {
                **db_session.query(Pointer).filter(
                    Pointer.status == 'ongoing',
                ).first().as_json(),
                'execution': db_session.query(Pointer).filter(
                    Pointer.status == 'ongoing',
                ).first().execution.as_json(),
            },
            'cacahuate_url': config['GUI_URL'],
        },
    }

    # pointer collection updated
    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert_near_date(reg['started_at'])
    assert reg['finished_at'] is None
    assert reg['execution']['id'] == execution.id
    assert reg['node'] == {
        'id': 'mid_node',
        'type': 'action',
        'description': 'añadir información',
        'name': 'Segundo paso',
    }
    assert reg['actors'] == {
        '_type': ':map',
        'items': {},
    }
    assert reg['actor_list'] == []
    assert reg['notified_users'] == [manager.as_json()]
    assert reg['state'] == 'ongoing'

    # execution collection updated
    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['state']['items']['mid_node']['state'] == 'ongoing'

    # tasks where asigned
    assert manager.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 1

    task = manager.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).first()

    assert isinstance(task, Pointer)
    assert task.node_id == 'mid_node'
    assert task.execution.id == execution.id


def test_teardown(config, mongo):
    ''' second and last stage of a node's lifecycle '''
    # test setup
    handler = Handler(config)

    process_name = 'simple.2018-02-19.xml'
    xml = Xml.load(config, process_name, direct=True)

    p_0 = make_pointer('simple.2018-02-19.xml', 'mid_node')
    execution = p_0.execution

    tests.utils.make_user('juan', 'Juan')
    manager = tests.utils.make_user('manager', 'Manager')
    manager2 = tests.utils.make_user('manager2', 'Manager2')

    assert manager not in execution.actors
    assert execution not in manager.activities.filter(
        Execution.status == 'ongoing',
    )

    manager.assigned_tasks.extend([p_0])
    manager2.assigned_tasks.extend([p_0])

    db_session.commit()

    state = Xml.load(config, execution.process_name).get_state()
    state['items']['start_node']['state'] = 'valid'

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': state,
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'id': p_0.id,
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': execution.id,
        },
        'node': {
            'id': p_0.node_id,
        },
        'actors': {
            '_type': ':map',
            'items': {},
        },
        'actor_list': [],
    })

    # will teardown mid_node
    handler.step({
        'command': 'step',
        'pointer_id': p_0.id,
        'user_identifier': manager.identifier,
        'input': [Form.state_json('mid_form', [
            {
                '_type': 'field',
                'state': 'valid',
                'value': 'yes',
                'value_caption': 'yes',
                'name': 'data',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == p_0.id,
        Pointer.status == 'ongoing',
    ).first() is None

    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 1
    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first().node_id == 'final_node'

    # mongo has a registry
    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert reg['started_at'] == datetime(2018, 4, 1, 21, 45)
    assert_near_date(reg['finished_at'])
    assert reg['execution']['id'] == execution.id
    assert reg['node']['id'] == p_0.node_id
    assert reg['actors'] == {
        '_type': ':map',
        'items': {
            'manager': {
                '_type': 'actor',
                'state': 'valid',
                'user': manager.as_json(include=[
                    '_type',
                    'identifier',
                    'fullname',
                    'email',
                ]),
                'forms': [Form.state_json('mid_form', [
                    {
                        '_type': 'field',
                        'state': 'valid',
                        'value': 'yes',
                        'value_caption': 'yes',
                        'name': 'data',
                    },
                ])],
            },
        },
    }
    assert reg['actor_list'] == [
        {
            'form': 'mid_form',
            'actor': manager.as_json(include=[
                '_type',
                'identifier',
                'fullname',
                'email',
            ]),
        },
    ]

    # tasks where deleted from user
    assert manager.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0
    assert manager2.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # state
    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['state'] == {
        '_type': ':sorted_map',
        'items': {
            'start_node': {
                **xml.get_node('start_node').get_state(),
                'state': 'valid',
            },
            'mid_node': {
                **xml.get_node('mid_node').get_state(),
                'actors': {
                    '_type': ':map',
                    'items': {
                        'manager': {
                            '_type': 'actor',
                            'state': 'valid',
                            'user': manager.as_json(include=[
                                '_type',
                                'identifier',
                                'fullname',
                                'email',
                            ]),
                            'forms': [Form.state_json('mid_form', [
                                {
                                    '_type': 'field',
                                    'state': 'valid',
                                    'value': 'yes',
                                    'value_caption': 'yes',
                                    'name': 'data',
                                },
                            ])],
                        },
                    },
                },
                'state': 'valid',
            },
            'final_node': {
                **xml.get_node('final_node').get_state(),
                'state': 'ongoing',
            },
        },
        'item_order': [
            'start_node',
            'mid_node',
            'final_node',
        ],
    }

    assert reg['values'] == {
        '_execution': [{
            'name': '',
            'description': '',
        }],
        'mid_form': [{
            'data': 'yes',
        }],
    }

    assert manager in execution.actors
    assert execution in manager.activities.filter(
        Execution.status == 'ongoing',
    )


def test_finish_execution(config, mongo):
    handler = Handler(config)

    p_0 = make_pointer('simple.2018-02-19.xml', 'manager')
    execution = p_0.execution
    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'status': 'ongoing',
        'id': execution.id,
    })

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())
    assert execution.id == reg['id']

    handler.finish_execution(execution)

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['status'] == 'finished'
    assert_near_date(reg['finished_at'])


def test_call_handler_delete_process(config, mongo):
    handler = Handler(config)
    pointer = make_pointer('simple.2018-02-19.xml', 'requester')
    user = make_user('juan', 'Juan')
    execution_id = pointer.execution.id
    payload = {
        'command': 'cancel',
        'execution_id': execution_id,
        'pointer_id': pointer.id,
        'user_identifier': user.identifier,
    }

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'status': 'ongoing',
        'id': execution_id,
    })

    ptr_id_1 = 'RANDOMPOINTERNAME'
    ptr_id_2 = 'MORERANDOMNAME'
    ptr_id_3 = 'NOTSORANDOMNAME'

    mongo[config["POINTER_COLLECTION"]].insert_many([
        {
            'execution': {
                'id': execution_id,
            },
            'state': 'finished',
            'id': ptr_id_1,
        },
        {
            'execution': {
                'id': execution_id,
            },
            'state': 'ongoing',
            'id': ptr_id_2,
        },
        {
            'execution': {
                'id': execution_id[::-1],
            },
            'state': 'ongoing',
            'id': ptr_id_3,
        },
    ])

    handler(json.dumps(payload))

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find())

    assert reg['id'] == execution_id
    assert reg['status'] == "cancelled"
    assert_near_date(reg['finished_at'])

    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).count() == 0
    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    assert mongo[config["POINTER_COLLECTION"]].find_one({
        'id': ptr_id_1,
    })['state'] == 'finished'
    assert mongo[config["POINTER_COLLECTION"]].find_one({
        'id': ptr_id_2,
    })['state'] == 'cancelled'
    assert mongo[config["POINTER_COLLECTION"]].find_one({
        'id': ptr_id_3,
    })['state'] == 'ongoing'


def test_resistance_unexistent_hierarchy_backend(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name).get_state(),
    })

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_hierarchy_return(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name).get_state(),
    })

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_hierarchy_item(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name).get_state(),
    })

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_node_not_found(config, mongo):
    handler = Handler(config)

    ptr = make_pointer('wrong.2018-04-11.xml', 'start_node')
    exc = ptr.execution
    user = make_user('juan', 'Juan')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exc.id,
        'state': Xml.load(config, exc.process_name).get_state(),
    })

    with pytest.raises(MisconfiguredProvider):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': ptr.id,
            'user_identifier': user.identifier,
            'input': {},
        }))


def test_resistance_dead_pointer(config):
    handler = Handler(config)

    with pytest.raises(InconsistentState):
        handler(json.dumps({
            'command': 'step',
            'pointer_id': 'nones',
        }))


def test_compact_values(config):
    handler = Handler(config)
    names = [random_string() for _ in '123']

    values = handler.compact_values([
        Form.state_json('form1', [
            {
                'name': 'name',
                'type': 'text',
                'value': names[0],
            },
        ]),
        Form.state_json('form1', [
            {
                'name': 'name',
                'type': 'text',
                'value': names[1],
            },
        ]),
        Form.state_json('form2', [
            {
                'name': 'name',
                'type': 'text',
                'value': names[2],
            },
        ]),
    ])

    assert values == {
        'values.form1': [
            {
                'name': name,
            } for name in names[:2]
        ],
        'values.form2': [
            {
                'name': names[2],
            },
        ],
    }
