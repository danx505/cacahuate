import cacahuate.models
import cacahuate.xml
from cacahuate.database import db_session

import flask

import tests.utils


def test_list_activities_requires(client):
    res = client.get('/v1/activity')

    assert res.status_code == 401


def test_list_activities(client):
    '''Given 4 activities, two for the current user and two for
    another, list only the two belonging to him or her'''
    juan = tests.utils.make_user('juan', 'Juan')

    exc = cacahuate.models.Execution(
        process_name='simple.2018-02-19.xml',
        name=tests.utils.random_string(),
        description=tests.utils.random_string(),
        status='ongoing',
    )

    db_session.add(exc)
    db_session.commit()

    exc_2 = cacahuate.models.Execution(
        process_name='simple.2018-02-19.xml',
        name=tests.utils.random_string(),
        description=tests.utils.random_string(),
        status='ongoing',
    )
    exc_2.actors.append(juan)

    db_session.add(exc_2)
    db_session.commit()

    res = client.get(
        '/v1/activity',
        headers=tests.utils.make_auth_header(juan),
    )

    assert res.status_code == 200
    assert flask.json.loads(res.data) == {
        'data': [
            exc_2.as_json(),
        ],
    }


def test_name_with_if(client, mongo, config):
    xml = cacahuate.xml.Xml.load(config, 'pollo')
    assert xml.name == 'pollo.2018-05-20.xml'
