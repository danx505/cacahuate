from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
)


def test_exit_interaction(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    process_name = 'exit.2018-05-03.xml'
    xml = Xml.load(config, process_name, direct=True)

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('exit.2018-05-03.xml', 'start_node')

    execution = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # first node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id

    args = handle.delay.call_args[0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [],
    }

    # exit node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [],
    })

    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 0
    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).count() == 0

    # state is coherent
    state = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': execution.id,
    }))

    del state['_id']
    del state['finished_at']

    assert state == {
        '_type': 'execution',
        'id': execution.id,
        'name': '',
        'description': '',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'start_node': {
                    **xml.get_node('start_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                },

                'exit': {
                    **xml.get_node('exit').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            '__system__': {
                                '_type': 'actor',
                                'forms': [],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': '__system__',
                                    'fullname': 'System',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                },

                'final_node': {
                    **xml.get_node('final_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {},
                    },
                },
            },
            'item_order': ['start_node', 'exit', 'final_node'],
        },
        'status': 'finished',
        'actor_list': [
            {
                'node': 'start_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'exit',
                'actor': {
                    '_type': 'user',
                    'fullname': 'System',
                    'identifier': '__system__',
                    'email': '',
                },
            },
        ],
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    }
