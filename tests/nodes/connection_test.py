from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
    random_string,
)


def test_connection_node_execution_all(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer(
        'proc-connector.2023-03-01.xml',
        'first_node',
    )
    exe = ptr.execution

    value_01 = random_string()
    value_02 = random_string()
    value_03 = random_string()

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': exe.id,
        'state': Xml.load(config, exe.process_name).get_state(),
    })

    # node teardown
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('first_form', [
            {
                'name': 'data',
                'label': 'Info',
                'value': value_01,
                'value_caption': value_01,
            },
        ])],
    }
    handler.step(rabbit_call)

    # check db status
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
    ).first().status == 'finished'
    ptr = exe.pointers.filter(
        Pointer.status == 'ongoing',
        Pointer.execution_id == exe.id,
    ).first()
    assert ptr.node_id == 'exe_create'

    # test 01: execution create
    alt_exe = db_session.query(Execution).filter(
        Execution.status == 'ongoing',
        Execution.process_name == 'simple.2018-02-19.xml',
    ).first()

    alt_ptr = alt_exe.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()

    args = handle.delay.call_args_list[0][0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': alt_ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'name': 'data',
                'state': 'valid',
                'type': 'text',
                'value': value_01,
                'value_caption': value_01,
                'hidden': False,
            },
        ])],
    }
    assert json.loads(args) == rabbit_call
    handler.step(rabbit_call)

    reg = next(mongo[config["POINTER_COLLECTION"]].find({
        'id': alt_ptr.id,
    }))
    assert reg['node']['id'] == 'start_node'

    reg = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': alt_exe.id,
    }))
    assert reg['name'] == 'Simplest process ever started with: ' + value_01

    assert db_session.query(Execution).filter(
        Execution.status == 'ongoing',
        Execution.process_name == 'simple.2018-02-19.xml',
        Execution.id == alt_exe.id,
    ).count() == 1

    # node teardown
    args = handle.delay.call_args_list[1][0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('creation', [
            {
                'name': 'id',
                'label': 'Id',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }
    assert json.loads(args) == rabbit_call
    handler.step(rabbit_call)

    # check db status
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
    ).first().status == 'finished'
    ptr = exe.pointers.filter(
        Pointer.status == 'ongoing',
        Pointer.execution_id == exe.id,
    ).first()
    assert ptr.node_id == 'second_node'

    # node teardown
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('second_form', [
            {
                'name': 'data',
                'label': 'Info',
                'value': value_02,
                'value_caption': value_02,
            },
        ])],
    }
    handler.step(rabbit_call)

    # check db status
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
    ).first().status == 'finished'
    ptr = exe.pointers.filter(
        Pointer.status == 'ongoing',
        Pointer.execution_id == exe.id,
    ).first()
    assert ptr.node_id == 'exe_search'

    # node teardown
    # test 02: execution search
    args = handle.delay.call_args_list[2][0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('search', [
            {
                'name': 'id',
                'label': 'Id',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }
    assert json.loads(args) == rabbit_call
    handler.step(rabbit_call)

    # check db status
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
    ).first().status == 'finished'
    ptr = exe.pointers.filter(
        Pointer.status == 'ongoing',
        Pointer.execution_id == exe.id,
    ).first()
    assert ptr.node_id == 'third_node'

    # node teardown
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('third_form', [
            {
                'name': 'data',
                'label': 'Info',
                'value': value_03,
                'value_caption': value_03,
            },
        ])],
    }
    handler.step(rabbit_call)

    # check db status
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
    ).first().status == 'finished'
    ptr = exe.pointers.filter(
        Pointer.status == 'ongoing',
        Pointer.execution_id == exe.id,
    ).first()
    assert ptr.node_id == 'exe_update'

    # test 03: execution update
    args = handle.delay.call_args_list[3][0][0]
    rabbit_call = {
        'command': 'patch',
        'execution_id': alt_exe.id,
        'comment': 'System update',
        'user_identifier': '__system__',
        'inputs': [
            {
                'ref': 'start_node.__system__.0:start_form.data',
                'value': value_03,
                'value_caption': value_03,
            },
        ],
    }
    assert json.loads(args) == rabbit_call
    handler.patch(rabbit_call)

    # node teardown
    args = handle.delay.call_args_list[4][0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('update', [
            {
                'name': 'id',
                'label': 'Id',
                'state': 'valid',
                'type': 'text',
                'value': alt_exe.id,
                'value_caption': alt_exe.id,
            },
        ])],
    }
    assert json.loads(args) == rabbit_call
    handler.step(rabbit_call)
