from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import Pointer
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

from pika.adapters.blocking_connection import BlockingChannel

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
)


def test_true_condition_node(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')
    ''' conditional node will be executed if its condition is true '''
    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('condition.2018-05-17.xml', 'start_node')

    execution = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('mistery', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'abrete sésamo',
                'value_caption': 'abrete sésamo',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'condition1'

    # rabbit called
    handle.delay.assert_called_once()
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('condition1', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'mistical_node'


def test_false_condition_node(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    ''' conditional node won't be executed if its condition is false '''
    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('condition.2018-05-17.xml', 'start_node')

    execution = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('mistery', [
            {
                'name': 'password',
                'type': 'text',
                'value': '123456',
                'value_caption': '123456',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'condition1'

    # rabbit called
    handle.delay.assert_called_once()
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('condition1', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'condition2'


def test_anidated_conditions(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')
    ''' conditional node won't be executed if its condition is false '''
    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('anidated-conditions.2018-05-17.xml', 'a')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': ptr.execution.id,
        'state': Xml.load(config, 'anidated-conditions').get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('a', [
            {
                'name': 'a',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'outer'

    # rabbit called
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('outer', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'b'

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('b', [
            {
                'name': 'b',
                'value': '-1',
                'value_caption': '-1',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'inner1'

    # rabbit called
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('inner1', [
            {
                'name': 'condition',
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'f'

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('f', [
            {
                'name': 'f',
                'value': '-1',
                'value_caption': '-1',
            },
        ])],
    })

    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'g'


def test_ifelifelse_if(config, mongo, mocker):
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))
    mocker.patch('cacahuate.tasks.handle.delay')
    ''' else will be executed if preceding condition is false'''
    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('else.2018-07-10.xml', 'start_node')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': ptr.execution.id,
        'state': Xml.load(config, 'else').get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('secret01', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'incorrect!',
                'value_caption': 'incorrect!',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'condition01'

    # rabbit called
    handle.delay.assert_called_once()
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('condition01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'action01'

    # rabbit called to notify the user
    BlockingChannel.basic_publish.assert_called_once()
    args = BlockingChannel.basic_publish.call_args[1]
    assert args['exchange'] == 'charpe_notify'

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form01', [
            {
                'name': 'answer',
                'value': 'answer',
                'value_caption': 'answer',
            },
        ])],
    })

    # arrives to final_action
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'final_action'


def test_ifelifelse_elif(config, mongo, mocker):
    ''' else will be executed if preceding condition is false'''
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))

    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('else.2018-07-10.xml', 'start_node')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': ptr.execution.id,
        'state': Xml.load(config, 'else').get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('secret01', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'hocus pocus',
                'value_caption': 'hocus pocus',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'condition01'

    # rabbit called
    handle.delay.assert_called_once()
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('condition01', [
            {
                'name': 'condition',
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'elif01'

    # rabbit called
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('elif01', [
            {
                'name': 'condition',
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'action02'

    # rabbit called to notify the user
    BlockingChannel.basic_publish.assert_called_once()
    args = BlockingChannel.basic_publish.call_args[1]
    assert args['exchange'] == 'charpe_notify'

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form01', [
            {
                'name': 'answer',
                'value': 'answer',
                'value_caption': 'answer',
            },
        ])],
    })

    # arrives to final_action
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'final_action'


def test_ifelifelse_else(config, mongo, mocker):
    ''' else will be executed if preceding condition is false'''
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch((
        'pika.adapters.blocking_connection.'
        'BlockingChannel.basic_publish'
    ))

    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('else.2018-07-10.xml', 'start_node')

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': ptr.execution.id,
        'state': Xml.load(config, 'else').get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('secret01', [
            {
                'name': 'password',
                'type': 'text',
                'value': 'cuca',
                'value_caption': 'cuca',
            },
        ])],
    })

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'condition01'

    # rabbit called
    handle.delay.assert_called_once()
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('condition01', [
            {
                'name': 'condition',
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'elif01'

    # rabbit called
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('elif01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'else01'

    # rabbit called
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('else01', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    handler.step(rabbit_call)

    # pointer moved
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'action03'

    # rabbit called to notify the user
    BlockingChannel.basic_publish.assert_called_once()
    args = BlockingChannel.basic_publish.call_args[1]
    assert args['exchange'] == 'charpe_notify'

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form01', [
            {
                'name': 'answer',
                'value': 'answer',
                'value_caption': 'answer',
            },
        ])],
    })

    # arrives to final_action
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'final_action'


def test_invalidated_conditional(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')
    ''' a condiitonal depends on an invalidated field, if it changes during
    the second response it must take the second value '''
    # test setup
    handler = Handler(config)

    process_name = 'condition_invalidated.2019-10-08.xml'
    xml = Xml.load(config, process_name, direct=True)

    user = make_user('juan', 'Juan')
    process_filename = 'condition_invalidated.2019-10-08.xml'

    ptr = make_pointer(process_filename, 'start_node')

    execution = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(
            config, execution.process_name,
        ).get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # initial rabbit call
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'value',
                'type': 'int',
                'value': 3,
                'value_caption': '3',
            },
        ])],
    })
    handle.delay.assert_called_once()

    # arrives to if_node
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'if_node'

    # if_node's condition is True
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('if_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # if rabbit call
    handler.step(rabbit_call)
    handle.delay.assert_called_once()

    # arrives to if_validation
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'if_validation_node'

    # if's call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('if_validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I do not like it',
                'value_caption': 'I do not like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:form1.value',
                }],
                'value_caption': '',
            },
        ])],
    })
    handle.delay.assert_called_once()

    # returns to start_node
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'start_node'

    # second lap
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'value',
                'type': 'int',
                'value': -3,
                'value_caption': '-3',
            },
        ])],
    })

    # arrives to if_node again
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'if_node'

    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('if_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # if second rabbit call
    handler.step(rabbit_call)

    # arrives to elif_node
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'elif_node'

    # elif node's condition is true
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('elif_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # elif rabbit call
    handler.step(rabbit_call)

    # arrives to elif_validation
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'elif_validation_node'

    # elif's call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('elif_validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'Ugly... nope',
                'value_caption': 'Ugly... nope',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:form1.value',
                }],
                'value_caption': '',
            },
        ])],
    })

    # returns to start_node
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'start_node'

    # third lap
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'value',
                'type': 'int',
                'value': 0,
                'value_caption': '0',
            },
        ])],
    })

    # arrives to if_node again again
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'if_node'

    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('if_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # if third rabbit call
    handler.step(rabbit_call)

    # arrives to elif_node again
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'elif_node'

    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('elif_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': False,
                'value_caption': 'False',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # elif second rabbit call
    handler.step(rabbit_call)

    # arrives to else_node
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'else_node'

    # else node's condition is true
    args = handle.delay.call_args[0][0]
    rabbit_call = {
        'command': 'step',
        'pointer_id': ptr.id,
        'input': [Form.state_json('else_node', [
            {
                'name': 'condition',
                'state': 'valid',
                'type': 'bool',
                'value': True,
                'value_caption': 'True',
            },
        ])],
        'user_identifier': '__system__',
    }
    assert json.loads(args) == rabbit_call

    # else rabbit call
    handler.step(rabbit_call)

    # arrives to if_validation
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'else_validation_node'

    # else's call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('else_validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'What? No!',
                'value_caption': 'What? No!',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:form1.value',
                }],
                'value_caption': '',
            },
        ])],
    })

    # returns to start_node
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'start_node'

    # state is coherent
    state = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': execution.id,
    }))

    del state['_id']

    assert state == {
        '_type': 'execution',
        'id': execution.id,
        'name': '',
        'description': '',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'start_node': {
                    **xml.get_node('start_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json(
                                    'form1',
                                    [
                                        {
                                            'name': 'value',
                                            'type': 'int',
                                            'value': 0,
                                            'value_caption': '0',
                                            'state': 'invalid',
                                        },
                                    ],
                                    state='invalid',
                                )],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'ongoing',
                    'comment': 'What? No!',
                },

                'if_node': {
                    **xml.get_node('if_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            '__system__': {
                                '_type': 'actor',
                                'forms': [Form.state_json(
                                    'if_node',
                                    [
                                        {
                                            'name': 'condition',
                                            'value': False,
                                            'value_caption': 'False',
                                            'type': 'bool',
                                            'state': 'invalid',
                                        },
                                    ],
                                    state='invalid',
                                )],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': '__system__',
                                    'fullname': 'System',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'What? No!',
                },

                'if_validation_node': {
                    **xml.get_node('if_validation_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json(
                                    'if_validation_node',
                                    [
                                        {
                                            'name': 'response',
                                            'value': 'reject',
                                            'value_caption': 'reject',
                                            'state': 'invalid',
                                        },
                                        {
                                            'name': 'comment',
                                            'value': 'I do not like it',
                                            'value_caption': (
                                                'I do not like it'
                                            ),
                                        },
                                        {
                                            'name': 'inputs',
                                            'value': [{
                                                'ref': (
                                                    'start_node.juan.0:form1'
                                                    '.value'
                                                ),
                                            }],
                                            'value_caption': '',
                                        },
                                    ],
                                    state='invalid',
                                )],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'fullname': 'Juan',
                                    'identifier': 'juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'What? No!',
                },

                'elif_node': {
                    **xml.get_node('elif_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            '__system__': {
                                '_type': 'actor',
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'fullname': 'System',
                                    'identifier': '__system__',
                                    'email': '',
                                },
                                'forms': [Form.state_json(
                                    'elif_node',
                                    [
                                        {
                                            'name': 'condition',
                                            'value': False,
                                            'value_caption': 'False',
                                            'type': 'bool',
                                            'state': 'invalid',
                                        },
                                    ],
                                    state='invalid',
                                )],
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'What? No!',
                },

                'elif_validation_node': {
                    **xml.get_node('elif_validation_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'fullname': 'Juan',
                                    'identifier': 'juan',
                                    'email': '',
                                },
                                'forms': [Form.state_json(
                                    'elif_validation_node',
                                    [
                                        {
                                            'name': 'response',
                                            'value': 'reject',
                                            'value_caption': 'reject',
                                            'state': 'invalid',
                                        },
                                        {
                                            'name': 'comment',
                                            'value': 'Ugly... nope',
                                            'value_caption': (
                                                'Ugly... nope'
                                            ),
                                        },
                                        {
                                            'name': 'inputs',
                                            'value': [{
                                                'ref': (
                                                    'start_node.juan.0:form1'
                                                    '.value'
                                                ),
                                            }],
                                            'value_caption': '',
                                        },
                                    ],
                                    state='invalid',
                                )],
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'What? No!',
                },

                'else_node': {
                    **xml.get_node('else_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            '__system__': {
                                '_type': 'actor',
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'fullname': 'System',
                                    'identifier': '__system__',
                                    'email': '',
                                },
                                'forms': [Form.state_json(
                                    'else_node',
                                    [
                                        {
                                            'name': 'condition',
                                            'value': True,
                                            'value_caption': 'True',
                                            'type': 'bool',
                                            'state': 'invalid',
                                        },
                                    ],
                                    state='invalid',
                                )],
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'What? No!',
                },

                'else_validation_node': {
                    **xml.get_node('else_validation_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'fullname': 'Juan',
                                    'identifier': 'juan',
                                    'email': '',
                                },
                                'forms': [Form.state_json(
                                    'else_validation_node',
                                    [
                                        {
                                            'name': 'response',
                                            'value': 'reject',
                                            'value_caption': 'reject',
                                            'state': 'invalid',
                                        },
                                        {
                                            'name': 'comment',
                                            'value': 'What? No!',
                                            'value_caption': 'What? No!',
                                        },
                                        {
                                            'name': 'inputs',
                                            'value': [{
                                                'ref': (
                                                    'start_node.juan.0:form1'
                                                    '.value'
                                                ),
                                            }],
                                            'value_caption': '',
                                        },
                                    ],
                                    state='invalid'),
                                ],
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'What? No!',
                },
            },
            'item_order': [
                'start_node',
                'if_node',
                'if_validation_node',
                'elif_node',
                'elif_validation_node',
                'else_node',
                'else_validation_node',
            ],
        },

        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
            'form1': [
                {
                    'value': 0,
                },
            ],
            'if_node': [
                {
                    'condition': False,
                },
            ],
            'if_validation_node': [
                {
                    'response': 'reject',
                    'comment': 'I do not like it',
                    'inputs': [
                        {
                            'ref': 'start_node.juan.0:form1.value',
                        },
                    ],
                },
            ],
            'elif_node': [
                {
                    'condition': False,
                },
            ],
            'elif_validation_node': [
                {
                    'response': 'reject',
                    'comment': 'Ugly... nope',
                    'inputs': [
                        {
                            'ref': 'start_node.juan.0:form1.value',
                        },
                    ],
                },
            ],
            'else_node': [
                {
                    'condition': True,
                },
            ],
            'else_validation_node': [
                {
                    'response': 'reject',
                    'comment': 'What? No!',
                    'inputs': [
                        {
                            'ref': 'start_node.juan.0:form1.value',
                        },
                    ],
                },
            ],
        },

        'actor_list': [
            {
                'node': 'start_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'if_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'System',
                    'identifier': '__system__',
                    'email': '',
                },
            },
            {
                'node': 'if_validation_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'elif_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'System',
                    'identifier': '__system__',
                    'email': '',
                },
            },
            {
                'node': 'elif_validation_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'else_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'System',
                    'identifier': '__system__',
                    'email': '',
                },
            },
            {
                'node': 'else_validation_node',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
        ],
    }
