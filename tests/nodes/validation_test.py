from datetime import datetime

from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import Pointer
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import simplejson as json

from tests.utils import (
    assert_near_date,
    make_pointer,
    make_user,
)


def test_approve(config, mongo):
    ''' tests that a validation node can go forward on approval '''
    # test setup
    handler = Handler(config)
    user = make_user('juan', 'Juan')
    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'id': ptr.id,
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': ptr.execution.id,
        },
        'node': {
            'id': 'approval_node',
        },
        'actors': {
            '_type': ':map',
            'items': {},
        },
        'actor_list': [],
    })

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': ptr.execution.id,
        'state': Xml.load(config, 'validation.2018-05-09').get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # thing to test
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'accept',
                'value_caption': 'accept',
            },
            {
                'name': 'comment',
                'value': 'I like it',
                'value_caption': 'I like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0.task',
                }],
                'value_caption': '',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None

    new_ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert new_ptr.node_id == 'final_node'

    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert reg['started_at'] == datetime(2018, 4, 1, 21, 45)
    assert_near_date(reg['finished_at'])
    assert reg['execution']['id'] == new_ptr.execution.id
    assert reg['node']['id'] == 'approval_node'
    assert reg['actor_list'] == [
        {
            'form': 'approval_node',
            'actor': {
                '_type': 'user',
                'fullname': 'Juan',
                'identifier': 'juan',
                'email': '',
            },
        },
    ]
    assert reg['actors'] == {
        '_type': ':map',
        'items': {
            'juan': {
                '_type': 'actor',
                'state': 'valid',
                'user': {
                    '_type': 'user',
                    'identifier': 'juan',
                    'fullname': 'Juan',
                    'email': '',
                },
                'forms': [Form.state_json('approval_node', [
                    {
                        'name': 'response',
                        'name': 'response',
                        'value': 'accept',
                        'value_caption': 'accept',
                    },
                    {
                        'name': 'comment',
                        'name': 'comment',
                        'value': 'I like it',
                        'value_caption': 'I like it',
                    },
                    {
                        'name': 'inputs',
                        'name': 'inputs',
                        'value': [{
                            'ref': 'start_node.juan.0.task',
                        }],
                        'value_caption': '',
                    },
                ])],
            },
        },
    }


def test_reject(config, mongo):
    ''' tests that a rejection moves the pointer to a backward position '''
    # test setup
    process_name = 'validation.2018-05-09.xml'
    xml = Xml.load(config, process_name, direct=True)

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('validation.2018-05-09.xml', 'approval_node')

    execution = ptr.execution

    mongo[config["POINTER_COLLECTION"]].insert_one({
        'id': ptr.id,
        'started_at': datetime(2018, 4, 1, 21, 45),
        'finished_at': None,
        'execution': {
            'id': execution.id,
        },
        'node': {
            'id': 'approval_node',
        },
        'actors': {
            '_type': ':map',
            'items': {},
        },
        'actor_list': [],
    })

    state = Xml.load(config, 'validation.2018-05-09').get_state()

    state['items']['start_node']['state'] = 'valid'
    state['items']['start_node']['actors']['items']['juan'] = {
        '_type': 'actor',
        'state': 'valid',
        'user': {
            '_type': 'user',
            'identifier': 'juan',
            'fullname': 'Juan',
            'email': '',
        },
        'forms': [Form.state_json('work', [
            {
                'name': 'task',
                '_type': 'field',
                'state': 'valid',
                'value': '2',
            },
        ])],
    }

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': state,
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # will teardown the approval node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('approval_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I do not like it',
                'value_caption': 'I do not like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:work.task',
                }],
                'value_caption': '',
            },
        ])],
    })

    # assertions
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None

    new_ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert new_ptr.node_id == 'start_node'

    assert new_ptr in user.assigned_tasks.filter(
        Pointer.status == 'ongoing',
    )

    # data is invalidated
    state = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': execution.id,
    }))

    del state['_id']

    assert state == {
        '_type': 'execution',
        'id': execution.id,
        'name': '',
        'description': '',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'start_node': {
                    **xml.get_node('start_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('work', [
                                    {
                                        'name': 'task',
                                        '_type': 'field',
                                        'state': 'invalid',
                                        'value': '2',
                                    },
                                ], state='invalid')],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'ongoing',
                    'comment': 'I do not like it',
                },

                'approval_node': {
                    **xml.get_node('approval_node').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('approval_node', [
                                    {
                                        'name': 'response',
                                        'state': 'invalid',
                                        'value': 'reject',
                                        'value_caption': 'reject',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'I do not like it',
                                        'value_caption': 'I do not like it',
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': [{
                                            'ref': 'start_node.'
                                                   'juan.0:work.task',
                                        }],
                                        'value_caption': '',
                                    },
                                ], state='invalid')],
                                'state': 'invalid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'invalid',
                    'comment': 'I do not like it',
                },

                'final_node': {
                    **xml.get_node('final_node').get_state(),
                },
            },
            'item_order': ['start_node', 'approval_node', 'final_node'],
        },
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
            'approval_node': [{
                'comment': 'I do not like it',
                'response': 'reject',
                'inputs': [{'ref': 'start_node.juan.0:work.task'}],
            }],
        },
        'actor_list': [{
            'node': 'approval_node',
            'actor': {
                '_type': 'user',
                'fullname': 'Juan',
                'identifier': 'juan',
                'email': '',
            },
        }],
    }

    # mongo has the data
    reg = next(mongo[config["POINTER_COLLECTION"]].find())

    assert reg['started_at'] == datetime(2018, 4, 1, 21, 45)
    assert (reg['finished_at'] - datetime.now()).total_seconds() < 2
    assert reg['execution']['id'] == new_ptr.execution.id
    assert reg['node']['id'] == 'approval_node'
    assert reg['actor_list'] == [
        {
            'form': 'approval_node',
            'actor': {
                '_type': 'user',
                'fullname': 'Juan',
                'identifier': 'juan',
                'email': '',
            },
        },
    ]
    assert reg['actors'] == {
        '_type': ':map',
        'items': {
            'juan': {
                '_type': 'actor',
                'forms': [Form.state_json('approval_node', [
                    {
                        'name': 'response',
                        'value': 'reject',
                        'value_caption': 'reject',
                    },
                    {
                        'name': 'comment',
                        'value': 'I do not like it',
                        'value_caption': 'I do not like it',
                    },
                    {
                        'name': 'inputs',
                        'value': [{
                            'ref': 'start_node.juan.0:work.task',
                        }],
                        'value_caption': '',
                    },
                ])],
                'state': 'valid',
                'user': {
                    '_type': 'user',
                    'identifier': 'juan',
                    'fullname': 'Juan',
                    'email': '',
                },
            },
        },
    }


def test_reject_with_dependencies(config, mongo):
    handler = Handler(config)

    process_name = 'validation-reloaded.2018-05-17.xml'
    xml = Xml.load(config, process_name, direct=True)

    user = make_user('juan', 'Juan')
    ptr = make_pointer('validation-reloaded.2018-05-17.xml', 'node1')
    execution = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, 'validation-reloaded').get_state(),
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
        },
    })

    # first call to node1
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node2'

    # first call to node2
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form2', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node3'

    # first call to node3
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form3', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node4'

    # first call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('node4', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'comment',
                'value': 'I do not like it',
                'value_caption': 'I do not like it',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'node1.juan.0:form1.task',
                }],
                'value_caption': '',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node1'

    # second call to node1
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form1', [
            {
                'name': 'task',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node2'

    # second call to node2
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form2', [
            {
                'name': 'task',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node4'

    # second call to validation
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('node4', [
            {
                'name': 'response',
                'value': 'accept',
                'value_caption': 'accept',
            },
            {
                'name': 'comment',
                'value': 'I like it',
                'value_caption': 'I like it',
            },
            {
                'name': 'inputs',
                'value': None,
                'value_caption': 'None',
            },
        ])],
    })
    ptr = db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'node5'

    # first call to last node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('form5', [
            {
                'name': 'task',
                'value': '1',
                'value_caption': '1',
            },
        ])],
    })
    assert db_session.query(Pointer).filter(
        Pointer.status == 'ongoing',
    ).count() == 0

    # state is coherent
    state = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': execution.id,
    }))

    del state['_id']
    del state['finished_at']

    assert state == {
        '_type': 'execution',
        'id': execution.id,
        'name': '',
        'description': '',
        'state': {
            '_type': ':sorted_map',
            'items': {
                'node1': {
                    **xml.get_node('node1').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form1', [
                                    {
                                        'name': 'task',
                                        'value': '2',
                                        'value_caption': '2',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                    'comment': 'I do not like it',
                },

                'node2': {
                    **xml.get_node('node2').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form2', [
                                    {
                                        'name': 'task',
                                        'value': '2',
                                        'value_caption': '2',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                    'comment': 'I do not like it',
                },

                'node3': {
                    **xml.get_node('node3').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form3', [
                                    {
                                        'name': 'task',
                                        'value': '1',
                                        'value_caption': '1',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                },

                'node4': {
                    **xml.get_node('node4').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('node4', [
                                    {
                                        'name': 'response',
                                        'value': 'accept',
                                        'value_caption': 'accept',
                                    },
                                    {
                                        'name': 'comment',
                                        'value': 'I like it',
                                        'value_caption': 'I like it',
                                    },
                                    {
                                        'name': 'inputs',
                                        'value': None,
                                        'value_caption': 'None',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                    'comment': 'I do not like it',
                },

                'node5': {
                    **xml.get_node('node5').get_state(),
                    'actors': {
                        '_type': ':map',
                        'items': {
                            'juan': {
                                '_type': 'actor',
                                'forms': [Form.state_json('form5', [
                                    {
                                        'name': 'task',
                                        'value': '1',
                                        'value_caption': '1',
                                    },
                                ])],
                                'state': 'valid',
                                'user': {
                                    '_type': 'user',
                                    'identifier': 'juan',
                                    'fullname': 'Juan',
                                    'email': '',
                                },
                            },
                        },
                    },
                    'state': 'valid',
                },
            },
            'item_order': ['node1', 'node2', 'node3', 'node4', 'node5'],
        },
        'status': 'finished',
        'values': {
            '_execution': [{
                'name': '',
                'description': '',
            }],
            'node4': [{
                'comment': 'I like it',
                'inputs': None,
                'response': 'accept',
            }],
            'form1': [{'task': '2'}],
            'form2': [{'task': '2'}],
            'form3': [{'task': '1'}],
            'form5': [{'task': '1'}],
        },
        'actor_list': [
            {
                'node': 'node1',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'node2',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'node3',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'node4',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
            {
                'node': 'node5',
                'actor': {
                    '_type': 'user',
                    'fullname': 'Juan',
                    'identifier': 'juan',
                    'email': '',
                },
            },
        ],
    }


def test_invalidate_all_nodes(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')
    mocker.patch('cacahuate.xml.node.Request.make_request')

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('all-nodes-invalidated.2018-05-24.xml', 'start_node')

    execution = ptr.execution

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, 'all-nodes-invalidated').get_state(),
    })

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('work', [
            {
                'name': 'task',
                'value': '2',
                'value_caption': '2',
            },
        ])],
    })
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'request_node'
    args = handle.delay.call_args[0][0]

    handler.step(json.loads(args))
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'call_node'
    args = handle.delay.call_args[0][0]

    handler.step(json.loads(args))
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'if_node'
    args = handle.delay.call_args[0][0]

    handler.step(json.loads(args))
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'validation_node'

    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('validation_node', [
            {
                'name': 'response',
                'value': 'reject',
                'value_caption': 'reject',
            },
            {
                'name': 'inputs',
                'value': [{
                    'ref': 'start_node.juan.0:work.task',
                }],
                'value_caption': '',
            },
            {
                'name': 'comment',
                'value': '',
                'value_caption': '',
            },
        ])],
    })
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'start_node'
