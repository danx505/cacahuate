from cacahuate.database import db_session
from cacahuate.handler import Handler
from cacahuate.models import (
    Execution,
    Pointer,
)
from cacahuate.tasks import handle
from cacahuate.xml import Xml
from cacahuate.xml.node import Form

import simplejson as json

from tests.utils import (
    make_pointer,
    make_user,
    random_string,
)


def test_call_node(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('call.2018-05-18.xml', 'start_node')

    execution = ptr.execution

    value = random_string()

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
    })

    # teardown of first node and wakeup of call node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('start_form', [
            {
                'name': 'data',
                'name': 'data',
                'value': value,
                'value_caption': value,
            },
        ])],
    })
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'call'

    new_ptr = db_session.query(Pointer).filter(
        Pointer.node_id == 'start_node',
        Pointer.status == 'ongoing',
    ).first()

    # aditional rabbit call for new process
    args = handle.delay.call_args_list[0][0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': new_ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'name': 'data',
                'state': 'valid',
                'type': 'text',
                'value': value,
                'value_caption': value,
                'hidden': False,
            },
        ])],
    }

    # normal rabbit call
    args = handle.delay.call_args_list[1][0][0]

    json_res = json.loads(args)
    # check execution_id exists
    json_execution = json_res['input'][0]['inputs']['items']['execution']
    assert json_execution.pop('value')
    assert json_execution.pop('value_caption')

    assert json_res == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('call', [
            {
                'name': 'execution',
                'state': 'valid',
                'type': 'text',
            },
        ])],
    }

    # mongo log registry created for new process
    reg = next(mongo[config["POINTER_COLLECTION"]].find({
        'id': new_ptr.id,
    }))
    assert reg['node']['id'] == 'start_node'

    # mongo execution registry created for new process
    reg = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': new_ptr.execution.id,
    }))
    assert reg['name'] == 'Simplest process ever started with: ' + value

    # teardown of the call node and end of first execution
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [],
    })

    # old execution is gone, new is here
    assert db_session.query(Execution).filter(
        Execution.id == execution.id,
        Execution.status == 'ongoing',
    ).first() is None
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    execution = db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).first()
    assert execution.process_name == 'simple.2018-02-19.xml'


def test_call_node_render(config, mongo, mocker):
    mocker.patch('cacahuate.tasks.handle.delay')

    handler = Handler(config)
    user = make_user('juan', 'Juan')

    ptr = make_pointer('call-render.2020-04-24.xml', 'start_node')

    execution = ptr.execution

    value = random_string()

    mongo[config["EXECUTION_COLLECTION"]].insert_one({
        '_type': 'execution',
        'id': execution.id,
        'state': Xml.load(config, execution.process_name).get_state(),
    })

    # teardown of first node and wakeup of call node
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': user.identifier,
        'input': [Form.state_json('start_form', [
            {
                'name': 'data',
                'name': 'data',
                'value': value,
                'value_caption': value,
            },
        ])],
    })
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    ptr = execution.pointers.filter(
        Pointer.status == 'ongoing',
    ).first()
    assert ptr.node_id == 'call'

    new_ptr = db_session.query(Pointer).filter(
        Pointer.node_id == 'start_node',
        Pointer.status == 'ongoing',
    ).first()

    # aditional rabbit call for new process
    args = handle.delay.call_args_list[0][0][0]

    assert json.loads(args) == {
        'command': 'step',
        'pointer_id': new_ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('start_form', [
            {
                'label': 'Info',
                'name': 'data',
                'state': 'valid',
                'type': 'text',
                'value': value,
                'value_caption': value,
                'hidden': False,
            },
        ])],
    }

    # normal rabbit call
    args = handle.delay.call_args_list[1][0][0]

    json_res = json.loads(args)
    # check execution_id exists
    json_execution = json_res['input'][0]['inputs']['items']['execution']
    assert json_execution.pop('value')
    assert json_execution.pop('value_caption')

    assert json_res == {
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [Form.state_json('call', [
            {
                'name': 'execution',
                'state': 'valid',
                'type': 'text',
            },
        ])],
    }

    # mongo log registry created for new process
    reg = next(mongo[config["POINTER_COLLECTION"]].find({
        'id': new_ptr.id,
    }))
    assert reg['node']['id'] == 'start_node'

    # mongo execution registry created for new process
    reg = next(mongo[config["EXECUTION_COLLECTION"]].find({
        'id': new_ptr.execution.id,
    }))
    assert reg['name'] == 'Simplest process ever started with: ' + value

    # teardown of the call node and end of first execution
    handler.step({
        'command': 'step',
        'pointer_id': ptr.id,
        'user_identifier': '__system__',
        'input': [],
    })

    # old execution is gone, new is here
    assert db_session.query(Execution).filter(
        Execution.id == execution.id,
        Execution.status == 'ongoing',
    ).first() is None
    assert db_session.query(Pointer).filter(
        Pointer.id == ptr.id,
        Pointer.status == 'ongoing',
    ).first() is None
    execution = db_session.query(Execution).filter(
        Execution.status == 'ongoing',
    ).first()
    assert execution.process_name == 'simple.2018-02-19.xml'
