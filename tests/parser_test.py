import os
import xml

from cacahuate.xml.parser import (
    parse_xml,
    translate_parsed_process,
)

import yaml


def load_yaml(path):
    with open(path, 'r') as stream:
        src = yaml.safe_load(stream)
    return src


def test_parse_xml(config):
    proc_names = [
        'call-render.2020-04-24',
        'complex.2023-03-01',
        'gift-request.2020-04-05',
        'proc-connector.2023-03-01',
        'simple.2018-02-19',
        'validation-multiform.2018-05-22',
    ]

    for proc_name in proc_names:
        xml_path = os.path.join(config['XML_PATH'], f'{proc_name}.xml')
        with open(xml_path) as f:
            parsed = parse_xml(xml.dom.pulldom.parse(f))

        expected = load_yaml(f'tests/fixtures/parsed/{proc_name}.yml')
        assert expected == parsed


def test_translate_process(config):
    proc_names = [
        'complex.2023-03-01',
        'gift-request.2020-04-05',
        'simple.2018-02-19',
        'validation-multiform.2018-05-22',
    ]

    path = os.path.join(config['XML_PATH'], 'complex.2023-03-01.xml')
    with open(path) as f:
        parsed = parse_xml(xml.dom.pulldom.parse(f))

    for proc_name in proc_names:
        xml_path = os.path.join(config['XML_PATH'], f'{proc_name}.xml')
        with open(xml_path) as f:
            parsed = parse_xml(xml.dom.pulldom.parse(f))

        expected = load_yaml(f'tests/fixtures/translated/{proc_name}.yml')
        assert expected == translate_parsed_process(parsed)
