FROM python:3.9.17-alpine

ENV GROUP_ID=1000 \
    USER_ID=1000

WORKDIR /var/www/
ENV CACAHUATE_SETTINGS=/var/www/cacahuate/settings.py

RUN apk update && apk add \
    musl-dev \
    libffi-dev \
    build-base \
    libxml2-utils \
    postgresql-dev \
    git
RUN python -m pip install --upgrade pip

COPY . .
RUN pip install --upgrade pip
RUN pip install --verbose -r requirements.txt

RUN pip install gunicorn

RUN chmod +x wait-for-mongo.sh

RUN addgroup -g $GROUP_ID www
RUN adduser -D -u $USER_ID -G www www -s /bin/sh

USER www

EXPOSE 5000

CMD ["gunicorn", "-w", "4", "--bind", "0.0.0.0:5000", "cacahuate.http.wsgi:app"]
